<?php
    error_reporting(0);
    date_default_timezone_set("Mexico/General");
    session_start();
    $usuario_actual= $_SESSION['usuario'];
    include ("../includes/conexion.php");
    $link = ConectarsePostgreSQL();
    $linkMySQL = ConectarseMySQLMegaBD();

    #VERIFICAMOS SI EXISTE ALGUN USUARIO LOGEADO
    if($usuario_actual)
    {
        #VERIFICAMOS SI EL USUARIO DE LA SESIÓN ESTA AUTORIZADO PARA ENTRAR A ESTA PLATAFORMA
        $sqlUsuarioValido = mysqli_query($linkMySQL,"SELECT COUNT(*)
        FROM sis_control_acceso
        LEFT JOIN plataforma ON sis_control_acceso.id_plataforma=plataforma.id_plataforma
        LEFT JOIN usuario ON sis_control_acceso.id_usuario=usuario.id_usuario
        WHERE sis_control_acceso.id_plataforma='5' AND usuario.nombre_usuario='$usuario_actual'");
        while($row = mysqli_fetch_row($sqlUsuarioValido))
        {
            $cuantos = $row[0];
        }
        if($cuantos == 0) #USUARIO NO AUTORIZADO
        {
            session_destroy();
            echo "<script>parent.window.location.reload();</script>";
            exit;
        }
        else if($cuantos > 0)
        {
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/myIcons.css">
    <link rel="stylesheet" type="text/css" href="css/myForm.css">
	<link rel="stylesheet" type="text/css" href="includes/jquery-easyui-1.5.2/themes/material/easyui.css">
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/jquery.min.js"></script>
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/jquery.easyui.min.js"></script>
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/locale/easyui-lang-es.js"></script>
    <script type="text/javascript" src="includes/datagrid-filter/datagrid-filter.js"></script>
    <script type="text/javascript" src="includes/datagrid-view/datagrid-scrollview.js"></script>
    <!--OpenLayers-->
    <link rel="stylesheet" href="includes/Openlayers_3.19.1/ol.css" type="text/css">
    <script src="includes/Openlayers_3.19.1/ol-debug.js"></script>
    <!--Proj4-->
    <script src="includes/proj4js-2.3.15/dist/proj4.js"></script>
    <!--jQuery imageLens-->
    <script src="js/jquery.imageLens.js" type="text/javascript"></script>
    <script src="js/jquery.imageLens2.js" type="text/javascript"></script>
    <!--jQuery FancyBox-->
    <script type="text/javascript" src="includes/fancybox-2.1.7/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" href="includes/fancybox-2.1.7/jquery.fancybox.css" type="text/css" media="screen" />
    <!--jQuery PanZoom-->
    <script src="js/jquery.panzoom.js"></script>
    <script src="js/jquery.mousewheel.js"></script>


	<style>
        body
        {
            font-family:Roboto;
            font-size:12px;
            padding: 20px 20px 0 20px;
            margin:0;
        }
        /*SI SE DEFINIERON POR HTML*/
        input.easyui-textbox{text-align:center;}
        input.easyui-combobox{text-align:center;}
        input.easyui-numberbox{text-align:center;}
        input.easyui-datebox{text-align:center;}
        select.easyui-combobox{text-align:center;}
        /*SI SE DEFINIERON POR JS (APLICAMOS PARA TODOS: TEXT, COMBO, NUMBER,ETC)*/
        .textbox-text{text-align:center;color:crimson;font-weight:bold;}
        .textbox-readonly .textbox-text{background:#EEEEEE;text-align:center;color:dimgray;font-weight:bold;}
        /*PROPIEDADES PARA LOS TOOLTIPS*/
        .panelttUbicacionOK{padding-left:10px;background-color: #486647;color:#FFFFFF;}
        .panelttUbicacionNoEncontada{padding-left:10px;background-color: #8A1616;color:#FFFFFF;}

        .panelttFoto{padding-left:10px;background-color: #86A527;color:#FFFFFF;}
        .panelttVideo{padding-left:10px;background-color: #2B485D;color:#FFFFFF;}
    </style>

  	<script>
        /*OVERRIDES*/
        //FUNCION PARA LIMITAR UNICAMENTE LA SELECCION A LOS ELEMENTOS DE LA LISTA (COMBOBOX)
		$.extend($.fn.validatebox.defaults.rules,{
			inList:{
				validator:function(value,param){
					var c = $(param[0]);
					var opts = c.combobox('options');
					var data = c.combobox('getData');
					var exists = false;
					for(var i=0; i<data.length; i++){
						if (value == data[i][opts.textField]){
							exists = true;
							break;
						}
					}
					return exists;
				},
				message:'Debe seleccionar un elemento de la lista.'
			}
		});
        //ONLY NUMBERS IN TEXTBOX
        $.extend($.fn.validatebox.defaults.rules,{
            onlyNumbers:
            {
                validator: function(value,param)
                {
                    if(  /^\d+$/.test(value)  )
                        return value;
                },
                message: 'Solo se aceptan numeros'
            }
        });

        //FUNCION PARA FORMATEAR LOS CAMPOS DATEBOX (GUIONES, DIAGONALES, ETC.)
        $.fn.datebox.defaults.formatter = function(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            return y + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
        };
        $.fn.datebox.defaults.parser = function(s)
        {
            if (s)
            {
                var a = s.split('-');
                var d = new Number(a[2]);
                var m = new Number(a[1]);
                var y = new Number(a[0]);
                var dd = new Date(y, m-1, d);
                return dd;
            }
            else
            {
                return new Date();
            }
        };

        //FUNCION PARA MOSTRAR U OCULTAR LOS TEXBOX
        $.extend($.fn.textbox.methods, {
            show: function(jq){
                return jq.each(function(){
                    $(this).next().show();
                })
            },
            hide: function(jq){
                return jq.each(function(){
                    $(this).next().hide();
                })
            }
        });

        //FUNCION PARA PODER OBLIGAR A INTRODUCIR X NUMERO DE CARACTERES (USADO PARA COORDENADAS)
        $.extend($.fn.validatebox.defaults.rules, {
    		minLength: {
        		validator: function(value, param) {
            		return value.length >= param[0];
        		},
        		message: 'Ingrese al menos {0} caracteres'
    		},
    		decimalValue: {
        		validator: function(value, param) {
            		if (!/^([0-9])*[.]?[0-9]*$/.test(value)) {
                		//Invalido
            		} else {
                		//Valido
                		return value;
            		}
        		},
        		message: 'Por favor ingrese un numero decimal valido'
    		},
    		decimalLengthX: {
        		validator: function(value, param) {
            		var coordenada = value.split(".");
            		if (coordenada.length > 1) {
                		var enteros = coordenada[0];
                		var decimales = coordenada[1];
                		if (enteros.length == 6 && decimales.length == 2) {
                    		return value;
                		}
            		} else {
                		//console.warn('Es un entero');
            		}
        		},
        		message: 'El formato correcto de la coordenada "X" son 6 enteros y 2 decimales'
    		},
    		decimalLengthY: {
        		validator: function(value, param) {
            		var coordenada = value.split(".");
            		if (coordenada.length > 1) {
                		var enteros = coordenada[0];
                		var decimales = coordenada[1];
                		if (enteros.length == 7 && decimales.length == 2) {
                    		return value;
                		}
            		} else {
                		//console.warn('Es un entero');
            		}
        		},
        		message: 'El formato correcto de la coordenada "Y" son 7 enteros y 2 decimales'
    		}
		});
  	</script>
    <script>
    	/*FUNCIONES*/
        function formatTipo(val,row)
        {
            var ext = val.substr(-3).toLowerCase();
            if(ext == 'jpg')
                return '<span class="ttpFoto"><img src="images/picture.png"></span>';
            else if(ext == 'mp4')
                return '<span class="ttpVideo"><img src="images/film.png"></span>';
        }

        function formatZona(val, row) 
        {
            if (val == 'Jalacingo') return '<span style="background-color:#548235; color:white; padding:2px;">' + val + '</span>';
            else if (val == 'NORTE') return '<span style="background-color:#C00000; color:white; padding:2px;">' + 'Norte' + '</span>';
            else if (val == 'SMO') return '<span style="background-color:#305496; color:white; padding:2px;">' + val + '</span>';
            else if (val == 'TLACO') return '<span style="background-color:#BF8F00; color:white; padding:2px;">' + 'Tlacotalpan' + '</span>';
            else if (val == 'CPALMA') return '<span style="background-color:#7030A0; color:white; padding:2px;">' + 'Costa de la palma' + '</span>';
            else return val;
        }

        function showUbicacionWindow(idubicacion)
        {
            console.warn("Show windowUbicacion info with id: " + idubicacion);
            $('#windowUbicacion').window('open').window('center');
        }

        function destacar(opcion)
        {
            if(opcion=='SI')
                $('#dlgRecolectadasParaDestacar').dialog({ iconCls: 'icon-destacada', title: 'Definir chequeadas como: Destacada' }).dialog('open');
            else if(opcion=='NO')
                $('#dlgRecolectadasParaDestacar').dialog({ iconCls: 'icon-nodestacada', title: 'Definir chequeadas como: No destacada' }).dialog('open');


            var idsFotosParaDestacar = '';
            var idsVideosParaDestacar = '';
            var concatIdsArchivo = '';
            var cuantasFotos  = 0;
            var cuantosVideos = 0;

            var rows = $('#dgClasificacion').datagrid('getChecked');
            var longitud=rows.length;   
            
            for(var i=0; i<longitud; i++)
            {
                var row = rows[i];
                var thisArchivo = row.archivo;
                var ext_archivo = thisArchivo.substr(-3).toLowerCase();

                if(ext_archivo == 'jpg')
                {                       
                    if(cuantasFotos == 0)
                        idsFotosParaDestacar += row.id;
                    else
                        idsFotosParaDestacar += ", " + row.id;   
                    cuantasFotos++;                
                }
                else if(ext_archivo == 'mp4')
                {
                    
                    if(cuantosVideos == 0)
                        idsVideosParaDestacar += row.id;
                    else
                        idsVideosParaDestacar += ", " + row.id;
                    cuantosVideos++;
                }

                //RESULTADO PARA PHP
                if(i==0)
                    concatIdsArchivo+= row.id + "|" + ext_archivo;
                else
                    concatIdsArchivo+= "," + row.id + "|" + ext_archivo;

            }

            $('#textboxIDsFotos').textbox('setValue', idsFotosParaDestacar);
            $('#textboxIDsVideos').textbox('setValue', idsVideosParaDestacar);
            $('#paraDestacarValues').textbox('setValue', concatIdsArchivo);
            $('#textboxOperacion').textbox('setValue',opcion);    

        }

        function destacarenBD()
        {
            var paraDestacarValues = $('#paraDestacarValues').textbox('getValue');
            var opcion = $('#textboxOperacion').textbox('getValue');

            $('#formRecolectadasParaDestacar').form('submit',{
                url: 'destacar_foto_video.php',
                onSubmit: function(param)
                {
                    //Extra params
                    param.paraDestacarValues = paraDestacarValues;
                    param.opcion = opcion;

                    return $(this).form('enableValidation').form('validate');
                },
                success: function(result)
                {
                    var result = eval('('+result+')');
                    if (result.errorMsg)
                    {
                        $.messager.show({
                            title: 'Error',
                            msg: result.errorMsg,
                            height:'auto'
                        });
                    } 
                    else 
                    {
                        if(result.okMsg)
                        {
                            $.messager.show({
                                title: 'Aviso',
                                msg: result.okMsg,
                                height:'auto'
                            });
                            $('#dlgRecolectadasParaDestacar').dialog('close');
                            $('#dgClasificacion').datagrid('clearSelections');
                            $('#dgClasificacion').datagrid('clearChecked');
                            $('#dgClasificacion').datagrid('reload');
   
                        }
                    }
                }
            });
        }


        function limpiar_selecciones()
        {
            $('#dgClasificacion').datagrid('clearChecked');
        }    
    	/**/
    </script>
</head>
<body>
    <table style="margin:0 auto 0 auto;">
      <tr>
        <td>
            <table id="dgClasificacion">
                <thead>
                    <tr>
                        <th data-options="field:'ck', align:'center', checkbox:true"></th>
                        <th data-options="field:'archivo', align:'center', width:90" formatter="formatTipo">Tipo</th>
                         <th data-options="field:'id', align:'left', width:70">ID</th>
                         <!--<th data-options="field:'archivo', align:'center', width:60">Archivo</th>-->
                         <th data-options="field:'id_ct',align:'left', width:60">Camara</th>
                         <th data-options="field:'fecha',align:'left', width:70">Fecha</th>
                         <th data-options="field:'hora',align:'center', width:70">Hora</th>
                         <th data-options="field:'zona',align:'center', width:120" formatter="formatZona">Zona</th>
                    </tr>
                </thead>
            </table>
        </td>
        <td>
            <div id="panelFoto" style='display:flex; align-items:center; justify-content:center; width:100%; height:100%; text-align:center;'>                    
                    <header style="line-height: 15px; font-family: Roboto; font-weight: bold; font-size: 12px;">
                            <img align= "absmiddle" src="images/picture_insert.png">
                            <label>Vista previa</label>                           
                            <a style="float:right;" id="btntest"></a>                     
                    </header>

                    <img id="img_01" src="images/empty.png" width="600" height="450" />
                    <video controls controlsList="nodownload" id="vid_01" style="display:none; width:600px; height:450px; min-width: 100%; min-height: 100%;"></video>
                    <a style="display: none;" id="hiddenImage" href="#"><img id="img_02" src="images/empty.png" width="600" height="450"></a>
            </div>
        </td>
      </tr>
    </table>
    <div id="myFooter" style="border:0px solid coral; width:610px; height:20px; background:#f0f0f0; margin-top:-3px; padding-top:5px; margin-left:7px; text-align:right; font-family: Roboto; font-size: 12px; color:dimgray;"></div>

    <?php
        if($usuario_actual != 'invitado' && $usuario_actual != 'comunicacion' && $usuario_actual != 'jcobos')
        {
    ?>
            <div id="dgClasificacionToolbar">
                <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-limpiar',plain:'true'" onclick="limpiar_selecciones()">Deshacer selección</a>
                <a style="padding-left:217px;">Definir chequeadas como: </a>
                <a href="#" class="easyui-linkbutton easyui-tooltip" title="Destacada" data-options="iconCls:'icon-destacada',plain:'true'" onclick="destacar('SI')"></a>
                <a href="#" class="easyui-linkbutton easyui-tooltip" title="No destacada" data-options="iconCls:'icon-nodestacada',plain:'true'" onclick="destacar('NO')"></a>
            </div>
    <?php
        }
    ?>

    <div id="dlgRecolectadasParaDestacar">
        <form id="formRecolectadasParaDestacar" method="post">
            <div style="width:100% height:100%; text-align: center; padding-top:10px;">
                <label style="width:450px;">¿Esta seguro de aplicar los cambios a los siguientes IDs?</label>

                <table style="width:70%; margin:0 auto;">
                    <tr>
                        <td><label style="width: 50px;">Fotos:&nbsp;</label></td>
                        <td><input id="textboxIDsFotos"></td>                        
                    </tr>

                    <tr>
                        <td><label style="width: 50px;">Videos:&nbsp;</label></td>
                        <td><input id="textboxIDsVideos"></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <input name="textboxOperacion" id="textboxOperacion">
                            <input name="paraDestacarValues" id="paraDestacarValues">
                        </td>
                    </tr>
                </table>     

            </div>
        </form>
    </div>   






  <script>
   $(function()
   {
        //PanZoom Constructor///////////////////////////////////////////////////////////
        var $panzoom = $('#img_01').panzoom({
            contain: 'automatic',
            panOnlyWhenZoomed: true,
            increment: 0.1,
            minScale: 1
        });
        ////////////////////////////////////////////////////////////////////////////////


        $('#dgClasificacion').datagrid({
          iconCls: 'icon-clasificacion',
          title:'Clasificación de fotos y videos',
          width: 610,
          height:550,
          url:'get_clasificacion.php',
          toolbar: '#dgClasificacionToolbar',
          idField:'id',
          autoRowHeight:false,
          pageSize: 19,
          view: scrollview,
          remoteFilter:true,
          remoteSort:true,
          rownumbers:true,            
          nowrap:true,
          fitColumns:true,
          striped:true,
          singleSelect: true,
          selectOnCheck: false,
          checkOnSelect: false,
          onLoadSuccess:function()
          {
            //DISABLE CHECKBOX [HEADER]
            $(this).datagrid('getPanel').find('div.datagrid-header input[type=checkbox]').attr('disabled','disabled');

            $('#myFooter').fadeOut(300, function() {
                $(this).html('<span style="margin:0 0 0 0;">Numero de elementos: <b style="color:crimson;">' + $("#dgClasificacion").datagrid('getData').total + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span>').fadeIn(300);
            });

            //FOTO
            $('.ttpFoto').tooltip({
                content: function(){return $('<div></div>')},
                position: 'right',
                onShow: function()
                {
                    var t = $(this);
                    t.tooltip('tip').css({
                        backgroundColor: '#86A527',
                        borderColor: '#86A527'  
                    });
                },
                onUpdate:function(content)
                {
                    content.panel({
                        width: 65,
                        height: 'auto',
                        content: '<span>Foto</span>',
                        border: false,
                        bodyCls: 'panelttFoto'
                    });
                }
            });
            //VIDEO
            $('.ttpVideo').tooltip({
                content: function(){return $('<div></div>')},
                position: 'right',
                onShow: function()
                {
                    var t = $(this);
                    t.tooltip('tip').css({
                        backgroundColor: '#2B485D',
                        borderColor: '#2B485D'  
                    });
                },
                onUpdate:function(content)
                {
                    content.panel({
                        width: 65,
                        height: 'auto',
                        content: '<span>Video</span>',
                        border: false,
                        bodyCls: 'panelttVideo'
                    });
                }
            });


          },
          onClickRow:function(index,row)
          {
            
          		$.post('get_foto_video.php', {id: row.id, archivo: row.archivo}, function(result)
                {
          			var ext_archivo = result.substr(-3).toLowerCase();
                    //console.log(ext_archivo);


                    if(ext_archivo == 'jpg')
                    {
                        //panelFoto source and PanZoom
                        document.getElementById("img_01").src= result;
                        $panzoom.panzoom('reset').panzoom('resetZoom').panzoom('resetPan');
                    
                        $panzoom.parent().on('mousewheel.focal', function( e ) 
                        {
                            e.preventDefault();
                            var delta = e.delta || e.originalEvent.wheelDelta;
                            var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
                            $panzoom.panzoom('zoom', zoomOut, {
                                animate: false,
                                focal: e
                            });
                        });
                        /////////////////////////////////////////////////////////////////////
                        //FANCYBOX////////////////////////////////////////////////////////////////////                    
                        document.getElementById("hiddenImage").href  = result;
                        document.getElementById("hiddenImage").title = "ID: " + row.id;
                        $("a#hiddenImage").fancybox({
                            openEffect  : 'elastic',
                            closeEffect : 'elastic' ,
                            padding: 0,   
                            fitToView: true,
                            aspectRatio: true
                        });
                        /////////////////////////////////////////////////////////////////////////////
                        document.getElementById('vid_01').style.display = "none";
                        document.getElementById('img_01').style.display = "inline";
                        $('#btntest').linkbutton('enable');
                    }
                    else if(ext_archivo == 'mp4')
                    {
                        $('#vid_01')[0].src = result;
                        document.getElementById('vid_01').style.display = "inline";
                        document.getElementById('img_01').style.display = "none";
                        $('#btntest').linkbutton('disable');
                    }                  
          		});

              $('#dgUbicacion').propertygrid({url:'get_tabla_ubicacion.php?idubicacion=' + row.idubicacion_ct});
          }
        }).datagrid('enableFilter',[
            {
                field:'archivo',
                type: 'combobox',
                options:
                {
                    editable:false,
                    panelWidth:'auto',
                    panelHeight:'auto',
                    panelMinWidth:100,
                    valueField:'value',
                    textField:'text',
                    data:[{'value':'jpg', 'text':'Foto'},{'value':'mp4', 'text':'Video'}],
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).combobox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgClasificacion').datagrid('removeFilterRule','archivo').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).combobox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgClasificacion').datagrid('addFilterRule', {
                                field: 'archivo',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }           
            },
            {
                field:'id',
                type:'textbox',
                options:
                {
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).textbox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgClasificacion').datagrid('removeFilterRule','id').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).textbox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgClasificacion').datagrid('addFilterRule', {
                                field: 'id',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }
            },
            {
                field:'id_ct',
                type:'label'
            },
            {
                field:'fecha',
                type:'label'
            },
            {
                field:'hora',
                type:'label'
            },
            {
                field:'zona',
                type:'label'
            }
        ]);


        $('#panelFoto').panel({          
          width: 610,
          height: 550
        });

        $('#btntest').linkbutton({
            height:16,
            iconCls: 'icon-zoom',
            plain:true,
            onClick:function()
            {
                $("a#hiddenImage").trigger("click");
            }
        }); 


        $('#dlgRecolectadasParaDestacar').dialog({
            width:510,
            //height:160,
            height:200,
            closed:true,
            modal:true,
            draggable:false, 
            collapsible:false, 
            resizable:false,
            buttons:
            [
                {
                    text:'Aceptar',
                    iconCls:'icon-save',
                    handler:function(e)
                    {
                        destacarenBD();
                    }
                },
                {
                    text:'Cancelar',
                    iconCls:'icon-cancel',
                    handler:function(e) 
                    {
                        $('#dlgRecolectadasParaDestacar').dialog('close');
                    }     
                }
            ],
            onOpen: function()
            {
                $('#formRecolectadasParaDestacar').form('clear').form('disableValidation').form('validate'); 
                $('#textboxOperacion').textbox('hide');
                //Hidden
                $('#paraDestacarValues').textbox('hide');
            }
        });

        $('#formRecolectadasParaDestacar').form({
            novalidate:true
        });

        $('#textboxRecolectadasParaDestacar').textbox({
            width:450,
            height:50,
            editable:false,
            required:true,
            multiline:true
        });

        $('#textboxOperacion').textbox({
            width:250,
            editable:false,
            required:true
        });       

        ////////////////////////////

        $('#textboxIDsFotos').textbox({
            width:250,
            height:30,
            //required:true,
            editable:false,
            readonly:true,
            multiline:true 
        });

        $('#textboxIDsVideos').textbox({
            width:250,
            height:30,
            //required:true,
            editable:false,
            readonly:true,
            multiline:true 
        });

        $('#paraDestacarValues').textbox({
            width:250,
            required:true,
            editable:false,
            readonly:true                    
        });





    });
  </script>
  <?php
        }
    }
    else
    {
        echo "
        <html>
            <head><style>@font-face {font-family: 'Roboto';src: url('fonts/Roboto-Medium.ttf') format('truetype');}</style></head>
            <body><div style='width:100%; text-align:center; margin-top:250px;'><p style='color:crimson; font-family:Roboto;'>¡ERROR!</p><p style='color:dimgray; font-family:Roboto;'>Necesita iniciar sesión para utilizar este sistema</p></div></body>
        </html>";
    }
  ?>
</body>
</html>
