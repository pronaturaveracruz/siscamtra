<?php
    error_reporting(0);
    date_default_timezone_set("Mexico/General");
    session_start();
    $usuario_actual= $_SESSION['usuario'];
    include ("../includes/conexion.php");
    $link = ConectarsePostgreSQL();
    $linkMySQL = ConectarseMySQLMegaBD();

    #VERIFICAMOS SI EXISTE ALGUN USUARIO LOGEADO
    if($usuario_actual)
    {
        #VERIFICAMOS SI EL USUARIO DE LA SESIÓN ESTA AUTORIZADO PARA ENTRAR A ESTA PLATAFORMA
        $sqlUsuarioValido = mysqli_query($linkMySQL,"SELECT COUNT(*)
        FROM sis_control_acceso
        LEFT JOIN plataforma ON sis_control_acceso.id_plataforma=plataforma.id_plataforma
        LEFT JOIN usuario ON sis_control_acceso.id_usuario=usuario.id_usuario
        WHERE sis_control_acceso.id_plataforma='5' AND usuario.nombre_usuario='$usuario_actual'");
        while($row = mysqli_fetch_row($sqlUsuarioValido))
        {
            $cuantos = $row[0];
        }
        if($cuantos == 0) #USUARIO NO AUTORIZADO
        {
            session_destroy();
            echo "<script>parent.window.location.reload();</script>";
            exit;
        }
        else if($cuantos > 0)
        {
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/myIcons.css">
    <link rel="stylesheet" type="text/css" href="css/myForm.css">
	<link rel="stylesheet" type="text/css" href="includes/jquery-easyui-1.5.2/themes/material/easyui.css">
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/jquery.min.js"></script>
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/jquery.easyui.min.js"></script>
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/locale/easyui-lang-es.js"></script>
    <script type="text/javascript" src="includes/datagrid-filter/datagrid-filter.js"></script>
    <script type="text/javascript" src="includes/datagrid-view/datagrid-scrollview.js"></script>
    <!--OpenLayers-->
    <link rel="stylesheet" href="includes/Openlayers_3.19.1/ol.css" type="text/css">
    <script src="includes/Openlayers_3.19.1/ol-debug.js"></script>
    <!--Proj4-->
    <script src="includes/proj4js-2.3.15/dist/proj4.js"></script>
    <!--jQuery imageLens-->
    <script src="js/jquery.imageLens.js" type="text/javascript"></script>
    <script src="js/jquery.imageLens2.js" type="text/javascript"></script>
    <!--jQuery FancyBox-->
    <script type="text/javascript" src="includes/fancybox-2.1.7/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" href="includes/fancybox-2.1.7/jquery.fancybox.css" type="text/css" media="screen" />
    <!--jQuery PanZoom-->
    <script src="js/jquery.panzoom.js"></script>
    <script src="js/jquery.mousewheel.js"></script>


	<style>
        body
        {
            font-family:Roboto;
            font-size:12px;
            padding: 20px 20px 0 20px;
            margin:0;
        }
        /*SI SE DEFINIERON POR HTML*/
        input.easyui-textbox{text-align:center;}
        input.easyui-combobox{text-align:center;}
        input.easyui-numberbox{text-align:center;}
        input.easyui-datebox{text-align:center;}
        select.easyui-combobox{text-align:center;}
        /*SI SE DEFINIERON POR JS (APLICAMOS PARA TODOS: TEXT, COMBO, NUMBER,ETC)*/
        .textbox-text{text-align:center;color:crimson;font-weight:bold;}
        .textbox-readonly .textbox-text{background:#EEEEEE;text-align:center;color:dimgray;font-weight:bold;}
        /*PROPIEDADES PARA LOS TOOLTIPS*/
        .panelttUbicacionOK{padding-left:10px;background-color: #486647;color:#FFFFFF;}
        .panelttUbicacionNoEncontada{padding-left:10px;background-color: #8A1616;color:#FFFFFF;}

        .panelttMamiferos{padding-left:10px;background-color: #DDD4C2;color:#5D4E37;}
        .panelttAves{padding-left:10px;background-color: #FBBFC6;color:#C14049;}

        .panelttFoto{padding-left:10px;background-color: #86A527;color:#FFFFFF;}
        .panelttVideo{padding-left:10px;background-color: #2B485D;color:#FFFFFF;}
    </style>

  	<script>
        /*OVERRIDES*/
        //FUNCION PARA LIMITAR UNICAMENTE LA SELECCION A LOS ELEMENTOS DE LA LISTA (COMBOBOX)
		$.extend($.fn.validatebox.defaults.rules,{
			inList:{
				validator:function(value,param){
					var c = $(param[0]);
					var opts = c.combobox('options');
					var data = c.combobox('getData');
					var exists = false;
					for(var i=0; i<data.length; i++){
						if (value == data[i][opts.textField]){
							exists = true;
							break;
						}
					}
					return exists;
				},
				message:'Debe seleccionar un elemento de la lista.'
			}
		});
        //ONLY NUMBERS IN TEXTBOX
        $.extend($.fn.validatebox.defaults.rules,{
            onlyNumbers:
            {
                validator: function(value,param)
                {
                    if(  /^\d+$/.test(value)  )
                        return value;
                },
                message: 'Solo se aceptan numeros'
            }
        });

        //FUNCION PARA FORMATEAR LOS CAMPOS DATEBOX (GUIONES, DIAGONALES, ETC.)
        $.fn.datebox.defaults.formatter = function(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            return y + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
        };
        $.fn.datebox.defaults.parser = function(s)
        {
            if (s)
            {
                var a = s.split('-');
                var d = new Number(a[2]);
                var m = new Number(a[1]);
                var y = new Number(a[0]);
                var dd = new Date(y, m-1, d);
                return dd;
            }
            else
            {
                return new Date();
            }
        };

        //FUNCION PARA MOSTRAR U OCULTAR LOS TEXBOX
        $.extend($.fn.textbox.methods, {
            show: function(jq){
                return jq.each(function(){
                    $(this).next().show();
                })
            },
            hide: function(jq){
                return jq.each(function(){
                    $(this).next().hide();
                })
            }
        });

        //FUNCION PARA PODER OBLIGAR A INTRODUCIR X NUMERO DE CARACTERES (USADO PARA COORDENADAS)
        $.extend($.fn.validatebox.defaults.rules, {
    		minLength: {
        		validator: function(value, param) {
            		return value.length >= param[0];
        		},
        		message: 'Ingrese al menos {0} caracteres'
    		},
    		decimalValue: {
        		validator: function(value, param) {
            		if (!/^([0-9])*[.]?[0-9]*$/.test(value)) {
                		//Invalido
            		} else {
                		//Valido
                		return value;
            		}
        		},
        		message: 'Por favor ingrese un numero decimal valido'
    		},
    		decimalLengthX: {
        		validator: function(value, param) {
            		var coordenada = value.split(".");
            		if (coordenada.length > 1) {
                		var enteros = coordenada[0];
                		var decimales = coordenada[1];
                		if (enteros.length == 6 && decimales.length == 2) {
                    		return value;
                		}
            		} else {
                		//console.warn('Es un entero');
            		}
        		},
        		message: 'El formato correcto de la coordenada "X" son 6 enteros y 2 decimales'
    		},
    		decimalLengthY: {
        		validator: function(value, param) {
            		var coordenada = value.split(".");
            		if (coordenada.length > 1) {
                		var enteros = coordenada[0];
                		var decimales = coordenada[1];
                		if (enteros.length == 7 && decimales.length == 2) {
                    		return value;
                		}
            		} else {
                		//console.warn('Es un entero');
            		}
        		},
        		message: 'El formato correcto de la coordenada "Y" son 7 enteros y 2 decimales'
    		}
		});

        //FUNCION PARA ADMITIR UN RANGO DE FECHAS EN LOS FILTROS DEL DATAGRID
        $.extend($.fn.datagrid.defaults.filters, {
            dateRange: {
                init: function(container, options){
                    var c = $('<div style="display:inline-block; width:233px;"><input class="d1"><input class="d2"><a class="btn1"></a><a class="btn2"></a></div>').appendTo(container);

                    //c.find('.d1,.d2').datebox({ 

                    //Comportamiento para el primer datebox                    
                    c.find('.d1').datebox({ 
                        editable: false,
                        width:90,
                        onChange: function(value)
                        {
                            //console.log("Value changed: " + value);
                        }
                    });

                    //Comportamiento para el segundo datebox
                    c.find('.d2').datebox({ 
                        editable: false,
                        disabled:true,
                        width:90,
                        onChange: function(value)
                        {
                            if(value != '')
                            {
                                var d1Value = c.find('.d1').datebox('getValue');
                                var d2Value = value;
                                var valueConcat = d1Value + ':' + d2Value;

                                //console.log(valueConcat);

                                //Apply filter
                                c.find('.d1').datebox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                                c.find('.d2').datebox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                                $('#dgConsulta').datagrid('addFilterRule', {
                                    field: 'fecha',
                                    op: 'range',
                                    value: valueConcat
                                }).datagrid('doFilter');
                            }
                        }
                    });

                    //Comportamiento del boton filtro
                    c.find('.btn1').linkbutton({ 
                        iconCls:'icon-filter',
                        plain:true,
                        onClick:function()
                        {
                            $('#menuFilterOptionsFecha')
                                .menu({panelWidth:'auto'})
                                .menu('show',{left:400, top:130})
                                .menu({onClick:function(item)
                                    { 
                                        var date1Value = c.find('.d1').datebox('getValue');                                        

                                        if(date1Value != '')
                                        {
                                            switch(item.text)
                                            {
                                                case 'Igual':
                                                    $('#m-equal_f').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                                                    $('#m-greater_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    $('#m-less_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    $('#m-range_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    c.find('.d2').datebox('clear');
                                                    c.find('.d2').datebox('disable');
                                                    //Apply filter
                                                    c.find('.d1').datebox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                                                    c.find('.d2').datebox('textbox').css({backgroundColor: '#F1F1ED'});
                                                    $('#dgConsulta').datagrid('addFilterRule', {
                                                        field: 'fecha',
                                                        op: 'equal',
                                                        value: date1Value
                                                    }).datagrid('doFilter');
                                                break;
                                                case 'Mayor':
                                                    $('#m-equal_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    $('#m-greater_f').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                                                    $('#m-less_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    $('#m-range_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    c.find('.d2').datebox('clear');
                                                    c.find('.d2').datebox('disable');
                                                    //Apply filter
                                                    c.find('.d1').datebox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                                                    c.find('.d2').datebox('textbox').css({backgroundColor: '#F1F1ED'});
                                                    $('#dgConsulta').datagrid('addFilterRule', {
                                                        field: 'fecha',
                                                        op: 'greater',
                                                        value: date1Value
                                                    }).datagrid('doFilter');
                                                break;
                                                case 'Menor':
                                                    $('#m-equal_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    $('#m-greater_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    $('#m-less_f').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                                                    $('#m-range_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    c.find('.d2').datebox('clear');
                                                    c.find('.d2').datebox('disable');
                                                    //Apply filter
                                                    c.find('.d1').datebox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                                                    c.find('.d2').datebox('textbox').css({backgroundColor: '#F1F1ED'});
                                                    $('#dgConsulta').datagrid('addFilterRule', {
                                                        field: 'fecha',
                                                        op: 'less',
                                                        value: date1Value
                                                    }).datagrid('doFilter');
                                                break;
                                                case 'Rango':
                                                    $('#m-equal_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    $('#m-greater_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    $('#m-less_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                                                    $('#m-range_f').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                                                    c.find('.d2').datebox('enable');
                                                    c.find('.d2').datebox('textbox').css({backgroundColor: '#FFFFFF'});
                                                break;
                                            }
                                        }

                                    }
                                });
                        }
                    });

                    //Comportamiento del boton limpiar
                    c.find('.btn2').linkbutton({ 
                        iconCls:'icon-clear',
                        plain:true,
                        onClick:function()
                        {
                            c.find('.d1').datebox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                            c.find('.d2').datebox('textbox').css({backgroundColor: '#F1F1ED'});
                            c.find('.d1').datebox('clear');
                            c.find('.d2').datebox('clear');
                            c.find('.d2').datebox('disable');
                            $('#dgConsulta').datagrid('removeFilterRule','fecha').datagrid('doFilter');
                            $('#m-equal_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                            $('#m-greater_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                            $('#m-less_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                            $('#m-range_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                        }
                    });


                    return c;
                },
                destroy: function(target){
                    $(target).find('.d1,.d2').datebox('destroy');
                },
                getValue: function(target){
                    /*var d1 = $(target).find('.d1');
                    var d2 = $(target).find('.d2');
                    return d1.datebox('getValue') + ':'+d2.datebox('getValue');*/
                },
                setValue: function(target, value){
                    /*var d1 = $(target).find('.d1');
                    var d2 = $(target).find('.d2');
                    var vv = value.split(':');
                    d1.datebox('setValue', vv[0]);
                    d2.datebox('setValue', vv[1]);*/
                },
                resize: function(target, width){
                    /*$(target)._outerWidth(width)._outerHeight(22);
                    $(target).find('.d1,.d2').datebox('resize', width/2);*/
                }
            }
        });
  	</script>
    <script>
    	/*FUNCIONES*/
        function formatTipo(val,row)
        {
            var ext = val.substr(-3).toLowerCase();
            if(ext == 'jpg')
                return '<span class="ttpFoto"><img src="images/picture.png"></span>';
            else if(ext == 'mp4')
                return '<span class="ttpVideo"><img src="images/film.png"></span>';
        }

        function formatZona(val, row) 
        {
            if (val == 'Jalacingo') return '<span style="background-color:#548235; color:white; padding:2px;">' + val + '</span>';
            else if (val == 'NORTE') return '<span style="background-color:#C00000; color:white; padding:2px;">' + 'Norte' + '</span>';
            else if (val == 'SMO') return '<span style="background-color:#305496; color:white; padding:2px;">' + val + '</span>';
            else if (val == 'TLACO') return '<span style="background-color:#BF8F00; color:white; padding:2px;">' + 'Tlacotalpan' + '</span>';
            else if (val == 'CPALMA') return '<span style="background-color:#7030A0; color:white; padding:2px;">' + 'Costa de la palma' + '</span>';
            else return val;
        }

        function formatPredio(val, row)
        {
            switch(val)
            {
                case '1':
                    return '<span>El Pájaro</span>'; 
                    break;
                case '2':
                    return '<span>Puente Fierro</span>'; 
                    break;
                case '3':
                    return '<span>Reserva de Los Canates</span>'; 
                    break;
                case '4':
                    return '<span>Cala Larga o Estero Grande</span>'; 
                    break;
                case '5':
                    return '<span>Isleta de Pataratas</span>'; 
                    break;
                case '6':
                    return '<span>La Flota</span>'; 
                    break;
                case '7':
                    return '<span>Nacaxtle</span>'; 
                    break;                    
                case '8':
                    return '<span>Otro</span>';
                    break;
            }
        }

        function formatClase(val, row)
        {
            if(val == '6')
                return '<span class="ttpAves"><img src="images/flamingo.png"></span>';
            else if(val == '9')
                return '<span class="ttpMamiferos"><img src="images/emotion_face_monkey.png"></span>';
            else return val;
        }

        function formatUbicacion(val, row)
        {
            if(val == '')
                return '<img src="images/exclamation.png" title="No se ha capturado la ubicación">';
            else
                return '<img src="images/tick.png" title="De clic sobre el ícono para mas información" onclick="javascript:showUbicacionWindow(' + val +')">';
        }   

        function formatDestacada(val, row)
        {
            if(val == '' || val == null)
                return "<img src='images/clock_15.png' title='Pendiente por destacar'>";
            else if(val == 'Si')
                return "<img src='images/award_star_gold_red.png' title='Fotografía destacada'>";
            else if(val == 'No')
                return "<img src='images/award_star_silver_blue.png' title='No es una fotografía destacada'>";
            else return val;
        }

        function showUbicacionWindow(idubicacion)
        {
            console.warn("Show windowUbicacion info with id: " + idubicacion);
            $('#windowUbicacion').window('open').window('center');
        }

        function exportToExcel()
        {
            var idFilterValue        = $('#dgConsulta').datagrid('getFilterRule','id');
            var id_ctFilterValue     = $('#dgConsulta').datagrid('getFilterRule','id_ct');
            var fechaFilterValue     = $('#dgConsulta').datagrid('getFilterRule','fecha');
            var horaFilterValue      = $('#dgConsulta').datagrid('getFilterRule','hora');
            var zonaFilterValue      = $('#dgConsulta').datagrid('getFilterRule','zona');
            var predioFilterValue    = $('#dgConsulta').datagrid('getFilterRule','predio');
            var id_claseFilterValue  = $('#dgConsulta').datagrid('getFilterRule','id_clase');
            var nombre_cientificoFilterValue = $('#dgConsulta').datagrid('getFilterRule','nombre_cientifico');
            var destacadaFilterValue = $('#dgConsulta').datagrid('getFilterRule','destacada');

            if(idFilterValue)
                idFilterValue = idFilterValue.field + "|" + idFilterValue.op + "|" + idFilterValue.value;
            else
                idFilterValue = "";
            if(id_ctFilterValue)
                id_ctFilterValue = id_ctFilterValue.field + "|" + id_ctFilterValue.op + "|" + id_ctFilterValue.value;
            else
                id_ctFilterValue = "";
            if(fechaFilterValue)
                fechaFilterValue = fechaFilterValue.field + "|" + fechaFilterValue.op + "|" + fechaFilterValue.value;
            else
                fechaFilterValue = "";
            if(horaFilterValue)
                horaFilterValue = horaFilterValue.field + "|" + horaFilterValue.op + "|" + horaFilterValue.value;
            else
                horaFilterValue = "";
            if(zonaFilterValue)
                zonaFilterValue = zonaFilterValue.field + "|" + zonaFilterValue.op + "|" + zonaFilterValue.value;
            else
                zonaFilterValue = "";
            if(predioFilterValue)
                predioFilterValue = predioFilterValue.field + "|" + predioFilterValue.op + "|" + predioFilterValue.value;
            else
                predioFilterValue = "";
            if(id_claseFilterValue)
                id_claseFilterValue = id_claseFilterValue.field + "|" + id_claseFilterValue.op + "|" + id_claseFilterValue.value;
            else
                id_claseFilterValue = "";
            if(nombre_cientificoFilterValue)
                nombre_cientificoFilterValue = nombre_cientificoFilterValue.field + "|" + nombre_cientificoFilterValue.op + "|" + nombre_cientificoFilterValue.value;
            else
                nombre_cientificoFilterValue = "";
            if(destacadaFilterValue)
                destacadaFilterValue = destacadaFilterValue.field + "|" + destacadaFilterValue.op + "|" + destacadaFilterValue.value;
            else
                destacadaFilterValue = "";

            $('#windowExportToExcel').window({
                title: '&nbsp;&nbsp;Generando el archivo',
                iconCls: 'icon-time',
                width: 500,
                height: 80,
                modal: true,
                closed:true,
                closable: false,
                minimizable:false,
                maximizable: false,
                collapsible: false,
                resizable: false,
                draggable:false,
                border: 'thin',
                inline:true,                  
                href: 'exportToExcel.php',
                method:'post',
                queryParams: 
                {
                    id: idFilterValue,
                    id_ct: id_ctFilterValue,
                    fecha: fechaFilterValue,
                    hora:horaFilterValue,
                    zona:zonaFilterValue,
                    predio:predioFilterValue,
                    id_clase:id_claseFilterValue,
                    nombre_cientifico:nombre_cientificoFilterValue,
                    destacada:destacadaFilterValue
                  },
                  loadingMessage: 'Estamos procesando su solicitud, esto puede tardar unos minutos...',
                  extractor: function(data) 
                  {
                     //console.log(data);
                     var htmlText = "";
                     var data = data.split(",");                     
                     var estadoPHP = data[0];
                     //console.log(estadoPHP);
                     if(data.length > 1)
                     {
                        var rutaCSV   = data[1];
                        var nombreCSV = data[1].substring(14);
                     }                     

                     if (estadoPHP === 'filterCSV' || estadoPHP === 'allCSV') 
                     {
                        htmlText = '<div style="text-align:center; padding-top:5px; font-family:Roboto; font-weight:bold; color:dimgray; font-size:13px;">'+ nombreCSV +'<br><a target="_blank" href="'+ rutaCSV +'">Descargar</a></div>';
                     } 
                     else 
                     {
                        htmlText = '<div style="text-align:center; padding-top:0px; font-family:Roboto; font-weight:bold; color:crimson; font-size:13px;"><br>Ocurrió un error!</div>';
                     }
                     return htmlText;
                  },
                  onLoad: function() 
                  {                   
                     $(this).window({ title:'&nbsp;&nbsp;Resultado de su solicitud', iconCls: 'icon-acerca', href:'', closed:false, closable:true });
                  }
               });
               $('#windowExportToExcel').window('open');
               $('#windowExportToExcel').window('center');
        }  

        function exportToShapefile(proyeccion) 
        {
            console.warn(proyeccion);
            var idFilterValue        = $('#dgConsulta').datagrid('getFilterRule','id');
            var id_ctFilterValue     = $('#dgConsulta').datagrid('getFilterRule','id_ct');
            var fechaFilterValue     = $('#dgConsulta').datagrid('getFilterRule','fecha');
            var horaFilterValue      = $('#dgConsulta').datagrid('getFilterRule','hora');
            var zonaFilterValue      = $('#dgConsulta').datagrid('getFilterRule','zona');
            var predioFilterValue    = $('#dgConsulta').datagrid('getFilterRule','predio');
            var id_claseFilterValue  = $('#dgConsulta').datagrid('getFilterRule','id_clase');
            var nombre_cientificoFilterValue = $('#dgConsulta').datagrid('getFilterRule','nombre_cientifico');
            var destacadaFilterValue = $('#dgConsulta').datagrid('getFilterRule','destacada');

            if(idFilterValue)
                idFilterValue = idFilterValue.field + "|" + idFilterValue.op + "|" + idFilterValue.value;
            else
                idFilterValue = "";
            if(id_ctFilterValue)
                id_ctFilterValue = id_ctFilterValue.field + "|" + id_ctFilterValue.op + "|" + id_ctFilterValue.value;
            else
                id_ctFilterValue = "";
            if(fechaFilterValue)
                fechaFilterValue = fechaFilterValue.field + "|" + fechaFilterValue.op + "|" + fechaFilterValue.value;
            else
                fechaFilterValue = "";
            if(horaFilterValue)
                horaFilterValue = horaFilterValue.field + "|" + horaFilterValue.op + "|" + horaFilterValue.value;
            else
                horaFilterValue = "";
            if(zonaFilterValue)
                zonaFilterValue = zonaFilterValue.field + "|" + zonaFilterValue.op + "|" + zonaFilterValue.value;
            else
                zonaFilterValue = "";
            if(predioFilterValue)
                predioFilterValue = predioFilterValue.field + "|" + predioFilterValue.op + "|" + predioFilterValue.value;
            else
                predioFilterValue = "";
            if(id_claseFilterValue)
                id_claseFilterValue = id_claseFilterValue.field + "|" + id_claseFilterValue.op + "|" + id_claseFilterValue.value;
            else
                id_claseFilterValue = "";
            if(nombre_cientificoFilterValue)
                nombre_cientificoFilterValue = nombre_cientificoFilterValue.field + "|" + nombre_cientificoFilterValue.op + "|" + nombre_cientificoFilterValue.value;
            else
                nombre_cientificoFilterValue = "";
            if(destacadaFilterValue)
                destacadaFilterValue = destacadaFilterValue.field + "|" + destacadaFilterValue.op + "|" + destacadaFilterValue.value;
            else
                destacadaFilterValue = "";

            $('#windowExportToShapefile').window({
                title: '&nbsp;&nbsp;Generando el archivo',
                iconCls: 'icon-time',
                width: 500,
                height: 120,
                modal: true,
                closed:true,
                closable: false,
                minimizable:false,
                maximizable: false,
                collapsible: false,
                resizable: false,
                draggable:false,
                border: 'thin',
                inline:true,                  
                href: 'exportToShapefile.php',
                method:'post',
                queryParams: 
                {
                    id: idFilterValue,
                    id_ct: id_ctFilterValue,
                    fecha: fechaFilterValue,
                    hora:horaFilterValue,
                    zona:zonaFilterValue,
                    predio:predioFilterValue,
                    id_clase:id_claseFilterValue,
                    nombre_cientifico:nombre_cientificoFilterValue,
                    destacada:destacadaFilterValue,
                    proyeccion: proyeccion
                  },
                  loadingMessage: 'Estamos procesando su solicitud, esto puede tardar unos minutos...',
                  extractor: function(data) 
                  {
                    //console.log(data);
                    //ok,elementos,9618,exportToShapefile/shapeSISCAMTRA_20170815_091332_ccl.zip

                     var htmlText = "";
                     var data = data.split(",");                     
                     var estadoPHP = data[0];
                     //console.log(estadoPHP);

                     if (estadoPHP === 'ok') 
                     {
                        htmlText = '<div style="text-align:center; padding-top:5px; font-weight:bold; color:dimgray;"><br>Se generó correctamente el archivo.';
                        if(data[2] == 1)
                            htmlText += '<br>Contenido: 1 punto.';
                        else
                            htmlText += '<br>Contenido: ' + data[2] + ' puntos.';

                        htmlText += '<br><br><a target="_blank" href="' + data[3] + '">Descargar</a></div>';                        
                    } 
                    else 
                    {
                        htmlText = '<div style="text-align:center; padding-top:0px; font-family:Roboto; font-weight:bold; color:crimson; font-size:13px;"><br>Ocurrió un error!</div>';
                    }

                     return htmlText;
                  },
                  onLoad: function() 
                  {                   
                     $(this).window({ title:'&nbsp;&nbsp;Resultado de su solicitud', iconCls: 'icon-acerca', href:'', closed:false, closable:true });
                  }
               });
               $('#windowExportToShapefile').window('open');
               $('#windowExportToShapefile').window('center');



        }
    	/**/
    </script>
</head>
<body>
    <table style="margin:0 auto 0 auto;">
      <tr>
        <td>
            <table id="dgConsulta" style="width:100%; height:550px; min-width:610px;">
                <thead frozen="true">
                    <tr>
                        <!--<th data-options="field:'ck', align:'center', checkbox:true"></th>-->
                        <th data-options="field:'archivo', align:'center', width:30" formatter="formatTipo">Tipo</th>
                        <th data-options="field:'id', align:'left', width:70">ID</th>
                        <th data-options="field:'id_ct',align:'center', width:60">Camara</th>
                    </tr>
                </thead>

                <thead>
                    <tr>                        
                         <th data-options="field:'fecha',align:'center', width:235">Fecha</th>
                         <th data-options="field:'hora',align:'center', width:80">Hora</th>
                         <th data-options="field:'zona',align:'center', width:120" formatter="formatZona">Zona</th>

                        <?php if ($usuario_actual != 'invitado') {?>
                         <th data-options="field:'predio',align:'center', width:180" formatter="formatPredio">Predio</th>
                         <?php }?>

                         <th data-options="field:'id_clase',align:'center', width:120" formatter="formatClase">Clase</th>
                         <th data-options="field:'nombre_cientifico',align:'left', width:170">Especie</th>
                         <th data-options="field:'total_individuos',align:'center', width:70">Individuos</th>
                         <th data-options="field:'nom059',align:'center', width:130">NOM059-2010</th>
                         <th data-options="field:'cites',align:'center', width:120">CITES</th>
                         <th data-options="field:'redlist2013',align:'center', width:120">RedList2013</th>

                         <?php if ($usuario_actual != 'invitado') {?>
                         <th data-options="field:'ubicacion',align:'center', width:75" formatter="formatUbicacion">Ubicación</th>
                         <?php }?>

                         <th data-options="field:'destacada',align:'center', width:90" formatter="formatDestacada">Destacada</th>

                         <!--Otros campos-->
                    </tr>
                </thead>
            </table>
        </td>
        <td>
            <div id="panelFoto" style='display:flex; align-items:center; justify-content:center; width:100%; height:100%; text-align:center;'>                    
                    <header style="line-height: 15px; font-family: Roboto; font-weight: bold; font-size: 12px;">
                            <img align= "absmiddle" src="images/picture_insert.png">
                            <label>Vista previa</label>                           
                            <a style="float:right;" id="btntest"></a>                     
                    </header>

                    <img id="img_01" src="images/empty.png" width="600" height="450" />
                    <img id="imgCargando" src="images/spinner2.gif" width="600" height="450" style="display:none;" /> <!-- Cache the GIF image -->

                    <video controls controlsList="nodownload" id="vid_01" style="display:none; width:600px; height:450px; min-width: 100%; min-height: 100%;"></video>
                    <a style="display: none;" id="hiddenImage" href="#"><img id="img_02" src="images/empty.png" width="600" height="450"></a>
            </div>
        </td>
      </tr>
    </table>
    <div id="myFooter" style="border:0px solid coral; width:610px; height:20px; background:#f0f0f0; margin-top:-3px; padding-top:5px; margin-left:7px; text-align:right; font-family: Roboto; font-size: 12px; color:dimgray;"></div>

    <?php
        if($usuario_actual != 'invitado' && $usuario_actual != 'comunicacion' && $usuario_actual != 'jcobos')
        {
    ?>
            <div id="dgConsultaToolbar">
                <a id="btnExport">Exportar</a>
                <a id="btnRemoveFilter" title="Quitar todos los filtros" class="easyui-tooltip" data-options="position:'top', onShow: function(){$(this).tooltip('tip').css({backgroundColor: '#d89e00', borderColor: '#ffecba', color: '#FFFFFF', margin: 'auto'});}"></a>
                <div id="menuExport">
                <div data-options="name:'exportToExcel', iconCls:'icon-exportToExcel'">Hoja de calculo</div>
                <div data-options="iconCls:'icon-exportShapefile'">
                    <span>Shapefile</span>
                    <div>
                        <div data-options="name:'exportShapefileCCL', iconCls:'icon-exportZip'">Cónica conforme de Lambert (WGS84)</div>
                        <div data-options="name:'exportShapefileUTM14', iconCls:'icon-exportZip'">UTM Zona 14N</div>
                        <div data-options="name:'exportShapefileUTM15', iconCls:'icon-exportZip'">UTM Zona 15N</div>
                    </div>
                </div>                
            </div> 
    </div>
    <?php
        }
    ?>

    <div id="windowUbicacion">
        <table id="pgUbicacion">
        <thead>
              <th data-options="field:'id_ct', align:'center'">Camara</th>
              <th data-options="field:'clave',align:'center'">Clave</th>
              <th data-options="field:'nombre_predio',align:'center'">Predio</th>
              <th data-options="field:'zona',align:'center'">Zona</th>
              <th data-options="field:'utmx',align:'center'">X</th>
              <th data-options="field:'utmy',align:'center'">Y</th>             
              <th data-options="field:'municipio',align:'center'">Estado</th>
              <th data-options="field:'estado',align:'center'">Municipio</th>
              <th data-options="field:'fecha_inicio',align:'center'">Fecha de instalación</th>
              <th data-options="field:'fecha_fin',align:'center'">Fecha de finalización</th>
              <th data-options="field:'observador',align:'center'">Observador</th>
            </thead>
          </table>
    </div>






    <div id="dlgRecolectadasParaDestacar">
        <form id="formRecolectadasParaDestacar" method="post">
            <div style="width:100% height:100%; text-align: center; padding-top:10px;">
                <label style="width:450px;">¿Esta seguro de aplicar los cambios a los siguientes IDs?</label>

                <table style="width:70%; margin:0 auto;">
                    <tr>
                        <td><label style="width: 50px;">Fotos:&nbsp;</label></td>
                        <td><input id="textboxIDsFotos"></td>                        
                    </tr>

                    <tr>
                        <td><label style="width: 50px;">Videos:&nbsp;</label></td>
                        <td><input id="textboxIDsVideos"></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <input name="textboxOperacion" id="textboxOperacion">
                            <input name="paraDestacarValues" id="paraDestacarValues">
                        </td>
                    </tr>
                </table>     

            </div>
        </form>
    </div>   

    <div id="menuFilterOptionsFecha" class="easyui-menu" data-options="minWidth:10" style="width:75px;">
      <div id="m-equal_f" data-options="iconCls:'icon-equal'">Igual</div>
      <div id="m-greater_f" data-options="iconCls:'icon-greater'">Mayor</div>
      <div id="m-less_f" data-options="iconCls:'icon-less'">Menor</div>
      <div id="m-range_f" data-options="iconCls:'icon-daterange'">Rango</div>
    </div>

    <div id="windowExportToExcel"></div>
    <div id="windowExportToShapefile"></div>





  <script>
   $(function()
   {
        var fechaoOptionSelected ="";

        //PanZoom Constructor///////////////////////////////////////////////////////////
        var $panzoom = $('#img_01').panzoom({
            contain: 'automatic',
            panOnlyWhenZoomed: true,
            increment: 0.1,
            minScale: 1
        });
        ////////////////////////////////////////////////////////////////////////////////


        $('#dgConsulta').datagrid({
          iconCls: 'icon-consulta',
          title:'Consulta',
          url:'get_consulta.php',          
          idField:'id',
          autoRowHeight:false,
          pageSize: 19,
          view: scrollview,
          remoteFilter:true,
          remoteSort:true,
          rownumbers:true,            
          nowrap:true,
          striped:true,
          singleSelect: true,
          selectOnCheck: false,
          checkOnSelect: false,
          onLoadSuccess:function()
          {
            $('#myFooter').fadeOut(300, function() {
                $(this).html('<span style="margin:0 0 0 0;">Numero de elementos: <b style="color:crimson;">' + $("#dgConsulta").datagrid('getData').total + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span>').fadeIn(300);
            });

            //TOOLTIPS
            //MAMIFEROS
            $('.ttpMamiferos').tooltip({
                content: function(){return $('<div></div>')},
                position: 'right',
                onShow: function()
                {
                    var t = $(this);
                    t.tooltip('tip').css({
                        backgroundColor: '#DDD4C2',
                        borderColor: '#5D4E37'  
                    });
                },
                onUpdate:function(content)
                {
                    content.panel({
                        width: 65,
                        height: 'auto',
                        content: '<span>Mamífero</span>',
                        border: false,
                        bodyCls: 'panelttMamiferos'
                    });
                }
            });
            //AVES
            $('.ttpAves').tooltip({
                content: function(){return $('<div></div>')},
                position: 'right',
                onShow: function()
                {
                    var t = $(this);
                    t.tooltip('tip').css({
                        backgroundColor: '#FBBFC6',
                        borderColor: '#C14049'  
                    });
                },
                onUpdate:function(content)
                {
                    content.panel({
                        width: 65,
                        height: 'auto',
                        content: '<span>Ave</span>',
                        border: false,
                        bodyCls: 'panelttAves'
                    });
                 }
            });
            //FOTO
            $('.ttpFoto').tooltip({
                content: function(){return $('<div></div>')},
                position: 'right',
                onShow: function()
                {
                    var t = $(this);
                    t.tooltip('tip').css({
                        backgroundColor: '#86A527',
                        borderColor: '#86A527'  
                    });
                },
                onUpdate:function(content)
                {
                    content.panel({
                        width: 65,
                        height: 'auto',
                        content: '<span>Foto</span>',
                        border: false,
                        bodyCls: 'panelttFoto'
                    });
                }
            });
            //VIDEO
            $('.ttpVideo').tooltip({
                content: function(){return $('<div></div>')},
                position: 'right',
                onShow: function()
                {
                    var t = $(this);
                    t.tooltip('tip').css({
                        backgroundColor: '#2B485D',
                        borderColor: '#2B485D'  
                    });
                },
                onUpdate:function(content)
                {
                    content.panel({
                        width: 65,
                        height: 'auto',
                        content: '<span>Video</span>',
                        border: false,
                        bodyCls: 'panelttVideo'
                    });
                }
            });
          },
          onClickRow:function(index,row)
          {
                $('#img_01')[0].src = "images/spinner2.gif";
          		$.post('get_foto_video.php', {id: row.id, archivo: row.archivo}, function(result)
                {
          			var ext_archivo = result.substr(-3).toLowerCase();
                    if(ext_archivo == 'jpg')
                    {
                        //panelFoto source and PanZoom
                        document.getElementById("img_01").src= result;
                        $panzoom.panzoom('reset').panzoom('resetZoom').panzoom('resetPan');
                    
                        $panzoom.parent().on('mousewheel.focal', function( e ) 
                        {
                            e.preventDefault();
                            var delta = e.delta || e.originalEvent.wheelDelta;
                            var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
                            $panzoom.panzoom('zoom', zoomOut, {
                                animate: false,
                                focal: e
                            });
                        });
                        /////////////////////////////////////////////////////////////////////
                        //FANCYBOX////////////////////////////////////////////////////////////////////                    
                        document.getElementById("hiddenImage").href  = result;
                        document.getElementById("hiddenImage").title = "ID: " + row.id;
                        $("a#hiddenImage").fancybox({
                            openEffect  : 'elastic',
                            closeEffect : 'elastic' ,
                            padding: 0,   
                            fitToView: true,
                            aspectRatio: true
                        });
                        /////////////////////////////////////////////////////////////////////////////
                        document.getElementById('vid_01').style.display = "none";
                        document.getElementById('img_01').style.display = "inline";
                        $('#btntest').linkbutton('enable');
                    }
                    else if(ext_archivo == 'mp4')
                    {
                        $('#vid_01')[0].src = result;
                        document.getElementById('vid_01').style.display = "inline";
                        document.getElementById('img_01').style.display = "none";
                        $('#btntest').linkbutton('disable');
                    }                  
          		});

              $('#pgUbicacion').propertygrid({url:'get_tabla_ubicacion.php?idubicacion=' + row.ubicacion});
          },
          toolbar: '#dgConsultaToolbar'
        }).datagrid('enableFilter',[
            {
                field:'archivo',
                type: 'label'
            },
            {
                field:'id',
                type:'textbox',
                options:
                {
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).textbox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','id').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).textbox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'id',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }   
                }
            },
            {
                field:'id_ct',
                type:'textbox',
                options:
                {
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).textbox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','id_ct').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).textbox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'id_ct',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }     
                }
            },
            {
                field:'fecha',
                type:'dateRange'                
            },
            {
                field:'hora',
                type:'textbox',
                options:
                {
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).textbox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','hora').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).textbox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'hora',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    } 
                }
            },
            {
                field:'zona',
                type:'combobox',
                options:
                {
                    editable:false,
                    multiple:true,
                    panelWidth:'auto',
                    panelHeight:'auto',
                    panelMinWidth:120,
                    url:'getZonasFilter.php',
                    valueField:'zona',
                    textField:'zona',
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).combobox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','zona').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {
                        if(value != '')
                        {
                            console.log(value);
                            var valueConcat = "";
                            for(var i=0;i<value.length;i++)
                            {
                                if(i===0)
                                    valueConcat+= value[i];
                                else
                                    valueConcat+= "," + value[i];   
                            }
                            //console.log(valueConcat);
                            $(this).combobox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'zona',
                                op: 'equal',
                                value: valueConcat
                            }).datagrid('doFilter');
                        }
                        else
                        {
                            console.log('empty');
                            $(this).combobox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                            $('#dgConsulta').datagrid('removeFilterRule','zona').datagrid('doFilter');
                        }                        
                    }
                }
            },
            {
                field:'predio',
                type:'combobox',
                options:
                {
                    editable:false,
                    multiple:true,
                    panelWidth:'auto',
                    panelHeight:'auto',
                    url:'getPredioFilter.php',
                    valueField:'idpredio',
                    textField:'nombre_predio',
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).combobox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','predio').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {
                        if(value != '')
                        {
                            //console.log(value);
                            var valueConcat = "";
                            for(var i=0;i<value.length;i++)
                            {
                                if(i===0)
                                    valueConcat+= value[i];
                                else
                                    valueConcat+= "," + value[i];   
                            }
                            //console.log(valueConcat);
                            $(this).combobox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'predio',
                                op: 'equal',
                                value: valueConcat
                            }).datagrid('doFilter');
                        }
                        else
                        {
                            //console.log('empty');
                            $(this).combobox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                            $('#dgConsulta').datagrid('removeFilterRule','predio').datagrid('doFilter');
                        }                        
                    }
                }

            },
            {
                field:'id_clase',
                type:'combobox',
                options:
                {
                    editable:false,
                    panelWidth:'auto',
                    panelHeight:'auto',
                    panelMinWidth:120,
                    url:'getClaseFilter.php',
                    valueField:'idclasificacion',
                    textField:'notas',
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).combobox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','id_clase').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).combobox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'id_clase',
                                op: 'equal',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }    
                }
            },
            {
                field:'nombre_cientifico',
                type:'textbox',
                options:
                {
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).textbox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','nombre_cientifico').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).textbox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'nombre_cientifico',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }   
                } 
            },
            {
                field:'total_individuos',
                type:'textbox',
                options:
                {
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).textbox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','total_individuos').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).textbox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'total_individuos',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }
            },
            {
                field:'nom059',
                type:'textbox',
                options:
                {
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).textbox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','nom059').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).textbox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'nom059',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }
            },
            {
                field:'cites',
                type:'textbox',
                options:
                {
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).textbox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','cites').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).textbox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'cites',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }
            },
            {
                field:'redlist2013',
                type:'textbox',
                options:
                {
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).textbox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','redlist2013').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).textbox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'redlist2013',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }
            },
            {
                field:'ubicacion',
                type:'label'  
            },
            {
                field:'destacada',
                type:'combobox',
                options:
                {
                    editable:false,
                    panelWidth:'auto',
                    panelHeight:'auto',
                    panelMinWidth:90,
                    valueField:'text',
                    textField:'text',
                    data:[{'text':'Si'},{'text':'No'}],
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).combobox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgConsulta').datagrid('removeFilterRule','destacada').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).combobox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgConsulta').datagrid('addFilterRule', {
                                field: 'destacada',
                                op: 'equal',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }
            }
        ]);




        $('#panelFoto').panel({          
          width: 610,
          height: 550
        });

        $('#btntest').linkbutton({
            height:16,
            iconCls: 'icon-zoom',
            plain:true,
            onClick:function()
            {
                $("a#hiddenImage").trigger("click");
            }
        }); 

        $('#windowUbicacion').window({
            title:'Ubicación',
            iconCls:'icon-ubicaciones',
            width:300,
            height:338,
            closed:true,
            modal:true,
            collapsible:false,
            minimizable:false,
            maximizable:false,
            draggable:false,
            resizable:false
        });

        $('#pgUbicacion').propertygrid({
           scrollbarSize: 0,
           striped:true,
           width:'auto',
           columns:
           [[
                {field:'name',title:'Columna',width:100, resizable:false},
                {field:'value',title:'Valor', width:150,  resizable:false}
           ]]
        });


        $('#dlgRecolectadasParaDestacar').dialog({
            width:510,
            //height:160,
            height:200,
            closed:true,
            modal:true,
            draggable:false, 
            collapsible:false, 
            resizable:false,
            buttons:
            [
                {
                    text:'Aceptar',
                    iconCls:'icon-save',
                    handler:function(e)
                    {
                        destacarenBD();
                    }
                },
                {
                    text:'Cancelar',
                    iconCls:'icon-cancel',
                    handler:function(e) 
                    {
                        $('#dlgRecolectadasParaDestacar').dialog('close');
                    }     
                }
            ],
            onOpen: function()
            {
                $('#formRecolectadasParaDestacar').form('clear').form('disableValidation').form('validate'); 
                $('#textboxOperacion').textbox('hide');
            }
        });

        $('#formRecolectadasParaDestacar').form({
            novalidate:true
        });

        $('#textboxRecolectadasParaDestacar').textbox({
            width:450,
            height:50,
            editable:false,
            required:true,
            multiline:true
        });

        $('#textboxOperacion').textbox({
            width:250,
            editable:false,
            required:true
        });       

        ////////////////////////////

        $('#textboxIDsFotos').textbox({
            width:250,
            height:30,
            required:true,
            editable:false,
            readonly:true,
            multiline:true 
        });

        $('#textboxIDsVideos').textbox({
            width:250,
            height:30,
            required:true,
            editable:false,
            readonly:true,
            multiline:true 
        });

        $('#paraDestacarValues').textbox({
            width:250,
            required:true,
            editable:false,
            readonly:true                    
        });


        ///////////////////////////////////////////////////

        $('#btnExport').menubutton({
            iconCls: 'icon-export',
            plain:true,
            menu: '#menuExport'
        });

        $('#menuExport').menu({
            onClick:function(item)
            {
                switch(item.name)
                {
                    case 'exportToExcel':
                        exportToExcel();
                    break;
                    case 'exportShapefileCCL':
                        exportToShapefile('ccl');
                    break;
                    case 'exportShapefileUTM14':
                        exportToShapefile('utm14');
                    break;
                    case 'exportShapefileUTM15':
                        exportToShapefile('utm15');
                    break;
                }
            }
        });

        $('#btnRemoveFilter').linkbutton({
            iconCls: 'icon-clearFilter',
            plain:true,
            onClick: function()
            {
                //MODO AUTOMATICO
                $('.textbox').find("input").each(function(index, element) {
                    element.style.backgroundColor = "#FFFFFF";
                    element.style.color = "crimson";
                });

                //LIMPIAR MANUALMENTE LOS DATEBOX DEL RANGO (YA QUE PERTENECEN A OTRO CONTAINER Y EL METODO DE ABAJO NO LOS CONTEMPLA).
                $('.d1').datebox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                $('.d2').datebox('textbox').css({backgroundColor: '#F1F1ED'});

                $('.d1').datebox('clear');
                $('.d2').datebox('clear');
                $('.d2').datebox('disable');                           

                $('#m-equal_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                $('#m-greater_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                $('#m-less_f').css({backgroundColor: '#FFFFFF', color: '#000000'});
                $('#m-range_f').css({backgroundColor: '#FFFFFF', color: '#000000'});

                $('#dgConsulta').datagrid('removeFilterRule').datagrid('doFilter');
            }
        });





    });
  </script>
  <?php
        }
    }
    else
    {
        echo "
        <html>
            <head><style>@font-face {font-family: 'Roboto';src: url('fonts/Roboto-Medium.ttf') format('truetype');}</style></head>
            <body><div style='width:100%; text-align:center; margin-top:250px;'><p style='color:crimson; font-family:Roboto;'>¡ERROR!</p><p style='color:dimgray; font-family:Roboto;'>Necesita iniciar sesión para utilizar este sistema</p></div></body>
        </html>";
    }
  ?>
</body>
</html>
