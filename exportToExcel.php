<?php
	//error_reporting(0);
	date_default_timezone_set("Mexico/General");
	$fecha_actual = date("Ymd_His");
	include ("../includes/conexion.php");
	$linkMySQL = ConectarseMySQLMegaBD();
	##FUNCIÓN PARA CONVERTIR UN ARRAY TIPO OBJETO A UN ARRAY CONVENCIONAL
	function objectToArray($d) 
	{
		if (is_object($d)) 
		{
			$d = get_object_vars($d); #Gets the properties of the given object with get_object_vars function
		} 
		if (is_array($d)) 
		{
            return array_map(__FUNCTION__, $d); #Return array converted to object, Using __FUNCTION__ (Magic constant), for recursive call
        }
        else 
        {
        	return $d; #Return array
        }
    }

	#VARIABLES POST
	$id = $_POST['id'];
	$id_ct = $_POST['id_ct'];
	$fecha = $_POST['fecha'];
	$hora = $_POST['hora'];
	$zona = $_POST['zona'];
	$predio = $_POST['predio'];
	$id_clase = $_POST['id_clase'];
	$nombre_cientifico = $_POST['nombre_cientifico'];
	$destacada = $_POST['destacada'];

	#INICIALIZAMOS VARIABLES LOCALES
	$filterArray = array();
	$filtroActivo = 0;
	$where = "";
	$cadenaFinal = "";

	#VALIDAR SI LAS VARIABLES POST TIENEN ALGO PARA FILTRAR
	if($id !== '')
	{
		$id = explode("|", $id);
		array_push($filterArray, $id);
		$filtroActivo++;
	}
	if($id_ct !== '')
	{
		$id_ct = explode("|", $id_ct);
		array_push($filterArray, $id_ct);
		$filtroActivo++;
	}
	if($fecha !== '')
	{
		$fecha = explode("|", $fecha);
		array_push($filterArray, $fecha);
		$filtroActivo++;
	}
	if($hora !== '')
	{
		$hora = explode("|", $hora);
		array_push($filterArray, $hora);
		$filtroActivo++;
	}
	if($zona !== '')
	{
		$zona = explode("|", $zona);
		array_push($filterArray, $zona);
		$filtroActivo++;
	}
	if($predio !== '')
	{
		$predio = explode("|", $predio);
		array_push($filterArray, $predio);
		$filtroActivo++;
	}
	if($id_clase !== '')
	{
		$id_clase = explode("|", $id_clase);
		array_push($filterArray, $id_clase);
		$filtroActivo++;
	}
	if($nombre_cientifico !== '')
	{
		$nombre_cientifico = explode("|", $nombre_cientifico);
		array_push($filterArray, $nombre_cientifico);
		$filtroActivo++;
	}
	if($destacada !== '')
	{
		$destacada = explode("|", $destacada);
		array_push($filterArray, $destacada);
		$filtroActivo++;
	}


	#FILTRO
	if($filtroActivo > 0)
	{
		$cadenaFinal =	"filterCSV,";
		#CAMBIAMOS LOS VALORES DE LAS LLAVES DE NUMERICOS A TEXTO (PARA QUE QUEDEN IGUAL QUE EN 'get_apc.php')
		$filterArray = array_map(function($tag) {
	    	return array(
	        	'field' => $tag[0],
        		'op' 	=> $tag[1],
        		'value' => $tag[2]
    		);
		}, $filterArray);

		$num_filter  = count($filterArray);
		for($i=0; $i<$num_filter; $i++)
		{
			$filterField = $filterArray[$i]['field'];
			$filterOperator = $filterArray[$i]['op'];
			$filterValue = $filterArray[$i]['value'];

			#EXCEPCION PARA LAS ABREVIATURAS DE ZONA
			if($filterField == 'zona')
			{
				if($filterValue == 'Tlacotalpan')
					$filterValue = 'TLACO';
				else if($filterValue == 'Costa de la palma')
					$filterValue = 'CPALMA';
			}
			########################################

			$mValues = explode(",",$filterValue);
			$countmValues = count($mValues);		
			for($j=0; $j<$countmValues; $j++)
			{
				$filterValue = $mValues[$j];
				if ($countmValues > 1)
				{
					if($j === 0)
						$where .= " AND( lower(q2.$filterField) LIKE lower('%$filterValue%')";
					else if($j !==0 && $j!==($countmValues-1))
						$where .= " OR lower(q2.$filterField) LIKE lower('%$filterValue%')";
					else if($j===($countmValues-1))
						$where .= " OR lower(q2.$filterField) LIKE lower('%$filterValue%')  )";
				}
				else
				{
					switch($filterOperator)
					{
						case 'contains':
							$where .= " AND ( lower(q2.$filterField) LIKE lower('%$filterValue%')  )";
							break;
						case 'equal';
							$where .= " AND q2.".$filterField." = '".$filterValue."'";
							break;
						case 'notequal';
							$where .= " AND q2.".$filterField . " <> '" . $filterValue ."'";
							break;
						case 'beginwith';
							$where .= " AND q2.".$filterField . " LIKE '" . $filterValue ."%'";
							break;
						case 'endwith';
							$where .= " AND q2.".$filterField . " LIKE '%" . $filterValue ."'";
							break;
						case 'less';
							$where .= " AND q2.".$filterField . " < '". $filterValue."'";
							break;
						case 'lessorequal';
							$where .= " AND q2.".$filterField . " <= '". $filterValue."'";
							break;
						case 'greater';
							$where .= " AND q2.".$filterField . " > '". $filterValue."'";
							break;
						case 'greaterorequal';
							$where .= " AND q2.".$filterField . " >= '". $filterValue."'";
							break;
						case 'range':
							#TODO: Separar el filterValue en 2 variables
							$arrayFilterValue = explode(":", $filterValue);
							$f1 = $arrayFilterValue[0];
							$f2 = $arrayFilterValue[1];
							$where .= " AND (q2.fecha BETWEEN '$f1' AND '$f2')";
							break;
					}
				}
			}
		}

	}
	else
	{
		$where = "";
		$cadenaFinal =	"allCSV,";
	}

	#CONSULTA BASE -> 9,819 filas en servidor
	$select = "SELECT id, id_ct, fecha, hora, zona, nombre_predio, notas, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador "; 
	$consultaBase = "FROM
	(
		SELECT id, archivo, id_ct, fecha, hora, q1.zona, predio, nombre_predio, ubicacion, id_clase, notas, id_especie, nombre_cientifico, destacada, ruta, status, clave, registro_ubicaciones.zona AS zona_utm, utmx, utmy, registro_ubicaciones.municipio, estado, fecha_inicio, fecha_fin, observador
		FROM
		(
			SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status
			FROM concentrado_fotos, registro_fototrampas, aves_ecoforestal
			WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6')
				UNION ALL
			SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status
			FROM concentrado_fotos, registro_fototrampas, mamiferos
			WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9')

			UNION ALL

			SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status
			FROM concentrado_videos, registro_videotrampas, aves_ecoforestal
			WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6')
				UNION ALL
			SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status
			FROM concentrado_videos, registro_videotrampas, mamiferos
			WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9')
		) AS q1, registro_ubicaciones, clasificacion, predio
		WHERE (q1.ubicacion = registro_ubicaciones.idubicacion_ct AND q1.id_clase = clasificacion.idclasificacion AND registro_ubicaciones.predio = predio.idpredio)
	) AS q2 WHERE id> 0".$where." ORDER BY ruta DESC";

	$result = array();
	$rs = mysqli_query($linkMySQL,$select.$consultaBase);

	$items = array (
		array('id','camara','fecha','hora', 'zona', 'predio', 'clase','especie', 'destacada', 'clave', 'zona_utm', 'x', 'y', 'municipio', 'estado', 'fecha_inicio', 'fecha_fin', 'observador')
	);
	while($row = mysqli_fetch_object($rs))
	{

		##CAMBIAR EL NOMBRE DE LAS ZONAS EN EL ARCHIVO
		$nombreZonaMySQL = $row -> zona;
		switch ($nombreZonaMySQL) 
		{
			case 'TLACO':
				$row -> zona = 'Tlacotalpan';
				break;
			case 'NORTE':
				$row -> zona = 'Norte';
				break;
			case 'CPALMA':
				$row -> zona = 'Costa de la palma';
				break;
			default:
				break;
		}
		############################################
		array_push($items, $row);
	}
	$result= $items;

	#CONVERTIMOS OBJETO A ARREGLO
	$array = objectToArray($result);

	#ELIMINAR TODOS LOS ARCHIVOS DEL DIRECTORIO 'exportToExcel/'
	$files = glob('exportToExcel/*');
	foreach ($files as $file) {
		if (is_file($file))
        	unlink($file);
	}

	##DAR SOPORTE A LOS ACENTOS
	$nombre_archivo = 'exportToExcel/SISCAMTRA_'.$fecha_actual.'.csv';
	$fp = fopen($nombre_archivo, 'w');
	foreach ($array as $key => $value) 
	{
		fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
		fputcsv($fp, $value);
	}
	fclose($fp);

	mysqli_free_result($rs);
	mysqli_close($linkMySQL);

	echo $cadenaFinal.$nombre_archivo; #NECESARIO PARA VALIDACIONES EN EL EXTRACTOR DE 'consulta.php'