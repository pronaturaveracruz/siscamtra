<?php
    error_reporting(0);
    date_default_timezone_set("Mexico/General");
    session_start();
    $usuario_actual= $_SESSION['usuario'];
    include ("../includes/conexion.php");
    //$link = ConectarsePostgreSQL();
    //$linkMySQL = ConectarseMySQLMegaBD();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>SISCAMTRA | Pronatura Veracruz A.C.</title>
    <link rel="shortcut icon" type="image/png" href="images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="css/myIcons.css">
    <!-- <link rel="stylesheet" type="text/css" href="includes/jquery-easyui-1.5.2/themes/material/easyui.css"> -->
    <!-- EasyUI -->
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/material/easyui.css">
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/color.css">

    <!-- <script type="text/javascript" src="includes/jquery-easyui-1.5.2/jquery.min.js"></script>
    <script type="text/javascript" src="includes/jquery-easyui-1.5.2/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="includes/jquery-easyui-1.5.2/locale/easyui-lang-es.js"></script> -->



    <style>
        body {
            width: 100%;
            margin: auto;
            min-width: 600px;
            max-width: 1920px;
            font-family: Roboto;
            font-size: 12px;
            padding: 20px 0 0 0;
            background-color: #eee;
            background-image: url(data:image/gif;base64,R0lGODlhCAAIAJEAAMzMzP///////wAAACH5BAEHAAIALAAAAAAIAAgAAAINhG4nudroGJBRsYcxKAA7);
        }

        .noscroll {
            overflow: hidden;
        }
    </style>

</head>

<body>
    <div style="width:1280px; margin:0 auto 0 auto; border:0px solid coral; text-align:center;">
        <img src="images/header.png" style="position:absolute; z-index:-2; margin-left:-625px; margin-top:-13px;">
        <div style="position:absolute; z-index:1; margin-left:120px; margin-top:8px; text-align:left;">
            <a style="color:dimgray;">Sistema de Camaras Trampa</a>
            <br>
            <a style="color:#26b9ed">Pronatura Veracruz A.C.</a>
        </div>
    </div>

    <table style="margin:0 auto 0 auto;">
        <tr>
            <td>
                <div class="easyui-panel" style="padding:5px; width:1280px; margin:40px auto 0 auto; ">
                    <a href="javascript:void(0)" class="easyui-linkbutton"
                        onclick="addTab('Ubicaciones','ubicaciones.php')"
                        data-options="plain:true, iconCls:'icon-ubicaciones'"><span
                            style="font-size:1.2em">Ubicaciones</span></a>
                    <a href="javascript:void(0)" class="easyui-menubutton"
                        data-options="menu:'#mm1',iconCls:'icon-identificacion'"><span
                            style="font-size:1.2em">Identificación</span></a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="addTab('Consulta','consulta.php')"
                        data-options="plain:true, iconCls:'icon-consulta'"><span
                            style="font-size:1.2em">Consulta</span></a>
                    <a href="javascript:void(0)" class="easyui-linkbutton"
                        onclick="addTab('Clasificación','clasificacion.php')"
                        data-options="plain:true, iconCls:'icon-clasificacion'"><span
                            style="font-size:1.2em">Clasificación</span></a>
                    <a href="javascript:void(0)" class="easyui-menubutton"
                        data-options="menu:'#mm3',iconCls:'icon-acerca'"><span style="font-size:1.2em">Acerca
                            de</span></a>
                    <?php 
                        if (!(isset($_SESSION['usuario']) && $_SESSION['usuario'] != '')) 
                        {
                    ?>
                    <p class="easyui-linkbutton" onclick="$('#ff').form('reset'); $('#dlg_login').window('open')"
                        data-options="plain:true,iconCls:'icon-offline'"><span style="font-size:1.2em">Iniciar
                            sesión</span></p>
                    <?php 
                        } 
                        else
                        {
                            $nombre_completo= mysqli_query($linkMySQL,"SELECT nombre_completo FROM usuario WHERE nombre_usuario='$usuario_actual'");
                            $mfa_nombre_completo = mysqli_fetch_assoc($nombre_completo);
                            $usuario_nombre=$mfa_nombre_completo['nombre_completo'];
                    ?>
                    <a href="#" class="easyui-menubutton"
                        data-options="menu:'#menu_usuario_logout',iconCls:'icon-online'"><span
                            style="font-size:1.2em"><?php echo $usuario_nombre; ?></span></a>
                    <?php            
                        }
                    ?>
                </div>
            </td>
        </tr>
    </table>

    <div id="mm1" style="width:150px;">
        <div onclick="addTab('Identificación de fotos','identificacion_fotos.php')" data-options="iconCls:'icon-fotos'">
            Fotos</div>
        <div onclick="addTab('Identificación de videos','identificacion_videos.php')"
            data-options="iconCls:'icon-videos'">Videos</div>
    </div>

    <div id="mm3" class="menu-content"
        style="font-family:Roboto; background:#f0f0f0; padding:10px; text-align:justify: word-wrap:break-word; width:300px;">
        <p style="font-size:11px;color:#444; text-align:justify;"><b>ACERCA DE SISCAMTRA.</b></p>
        <p style="text-align:justify;">Este sistema ha sido desarrollado por Pronatura Veracruz A.C. para la
            identificación de fotografías y videos capturados por camaras trampa.</p>
    </div>

    <div style="display:none;" id="menu_usuario_logout" style="width:100px;">
        <div onclick="location.href = 'logout.php';" data-options="iconCls:'icon-disconnect'">Cerrar sesión</div>
    </div>

    <div class="easyui-dialog" id='dlg_login'
        data-options="buttons:'#btns', title:'Iniciar sesión', closed:true, modal:true, iconCls: 'icon-online', minimizable:false, maximizable:false, draggable:false, collapsible:false, resizable:false">
        <form id="ff" class="easyui-form" method="post" data-options="novalidate:true" enctype="multipart/form-data">
            <div class="fitem">
                <input id="user" name="user" class="easyui-textbox" data-options="required:true, prompt:'Usuario'"
                    style="text-align:center;" onkeypress="validateUserEnter(event)">
            </div>
            <div class="fitem">
                <input id="pass" name="pass" class="easyui-passwordbox"
                    data-options="required:true, prompt:'Contraseña', showEye:true" style="text-align:center;"
                    onkeypress="validateUserEnter(event)">
            </div>
        </form>
        <div id='btns'>
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="validateUser()">Acceder</a>
        </div>
    </div>

    <div id="tt" class="easyui-tabs" data-options="plain:true"
        style="width:1280px; /*height:auto;*/ /*height:850px;*/ height:660px; margin:0 auto 0 auto;">
        <div title="Bienvenido">
            <?php include("includes/tab_inicio.php"); ?>
        </div>
    </div>

    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- EasyUI -->
    <script type="text/javascript" src="https://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="https://www.jeasyui.com/easyui/locale/easyui-lang-es.js"></script>

    <script>
        $(document).ready(function () {
            //ENVIAMOS EL FORMULARIO CUANDO EL USUARIO DA 'ENTER'
            $('#user').textbox('textbox').bind('keydown', function (e) { if (e.keyCode == 13) validateUser(); });
            $('#pass').passwordbox('textbox').bind('keydown', function (e) { if (e.keyCode == 13) validateUser(); });
        });

        $(window).bind('resize', function () {
            $('#body').css('min-height', $(window).height() - $('#title').height());
        });

        function addTab(title, url) {
            if ($('#tt').tabs('exists', title)) {
                $('#tt').tabs('select', title);
            }
            else {
                var content = '<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%; height:100%; margin:auto;"></iframe>';
                $('#tt').tabs('add', {
                    title: title,
                    content: content,
                    closable: true,
                    bodyCls: 'noscroll'
                });
            }
        }

        function validateUser() {
            $('#ff').form('submit', {
                url: 'valida_usuario.php',
                onSubmit: function () {
                    return $(this).form('enableValidation').form('validate');
                },
                success: function (result) {
                    var result = eval('(' + result + ')');
                    if (result.errorMsg) {
                        $.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    }
                    else {
                        location.reload();
                    }
                }
            });
        }
    </script>


</body>

</html>