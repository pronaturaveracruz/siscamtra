<?php
	error_reporting(0);
	date_default_timezone_set("Mexico/General");
	session_start();
	$usuario_actual= $_SESSION['usuario'];
	$fecha_actual = date('Y-m-d');
	include ("../includes/conexion.php");
	$linkMySQL = ConectarseMySQLMegaBD();
	$aux=0;
	$contador=0;
	
	//RECUPERAMOS VARIABLES
	$recolectadosParaDestacar = $_POST['recolectadosParaDestacar'];
	$opcion = $_POST['opcion'];

	if($recolectadosParaDestacar && $opcion)
	{
		//AJUSTAMOS LOS ARRAY
		$arreglo_recolectados_a_destacar = explode(",", $recolectadosParaDestacar);
		$cuantas = count($arreglo_recolectados_a_destacar);

		for($i=0;$i<$cuantas;$i++)
		{
			if($opcion=='SI')
				$sql_actualizar_concentrado_videos= mysqli_query($linkMySQL, "UPDATE concentrado_videos SET destacado = 'Si', quien_destaco= '$usuario_actual' WHERE concentrado_videos.id_video = $arreglo_recolectados_a_destacar[$i]");
			else if($opcion=='NO')
				$sql_actualizar_concentrado_videos= mysqli_query($linkMySQL, "UPDATE concentrado_videos SET destacado = 'No', quien_destaco= '$usuario_actual' WHERE concentrado_videos.id_video = $arreglo_recolectados_a_destacar[$i]");

			$mar_actualiza= mysqli_affected_rows($linkMySQL);
			if($mar_actualiza>0)
				$aux++;
		}



		if($aux>0)
		{
			if($opcion == 'SI')
			{
				$status = "Destacado";
				if($aux == 1)					
					$okMsg = "Se definio ".$aux." video como 'Destacado'";
				else
					$okMsg = "Se definieron ".$aux." videos como 'Destacados'";
			}
			else if($opcion == 'NO')
			{
				$status = "No destacado";
				if($aux == 1)					
					$okMsg = "Se definio ".$aux." video como 'No destacado'";
				else
					$okMsg = "Se definieron ".$aux." videos como 'No destacados'";
			}

			echo json_encode(array('okMsg' => $okMsg ));
		}
		else if($aux == 0)
		{
			$errorMsg = "No se actualizo nada";
			echo json_encode(array('errorMsg'=> $errorMsg ));
		}
	}
	else
	{
		$errorMsg = "Error en las variables";
		echo json_encode(array('errorMsg'=> $errorMsg));
	}