<?php
	error_reporting(0);
	date_default_timezone_set("Mexico/General");
	session_start();
	$usuario_actual= $_SESSION['usuario'];
	$fecha_actual = date('Y-m-d');
	include ("../includes/conexion.php");
	$linkMySQL = ConectarseMySQLMegaBD();

	$auxEspecies = 0;
	$auxIDs = 0;
	$contadorUbicaciones=0;
	
	//RECUPERAMOS VARIABLES
	$recolectadosParaIdentificar = $_POST['recolectadosParaIdentificar'];
	$ubicacionesRecolectadas = $_POST['ubicacionesRecolectadas'];
	$especieValues = $_POST['especieValues']; //Obtenida del form

	//AJUSTAMOS LOS ARRAY
	$arregloRecolectadosParaIdentificar = explode(",", $recolectadosParaIdentificar);
	$cuantosIDs = count($arregloRecolectadosParaIdentificar);	
	$arregloUbicacionesRecolectadas = explode(",", $ubicacionesRecolectadas);
	$cuantasUbicacionesRecolectadas = count($arregloUbicacionesRecolectadas);
	
	for($x=0;$x<$cuantasUbicacionesRecolectadas;$x++)
	{
		if($arregloUbicacionesRecolectadas[$x]!='')
			$contadorUbicaciones++;
	}
	
	//echo "<script>console.log('Cuantos IDs: ".$cuantosIDs."');</script>";
	//echo "<script>console.log('Cuantas Ubicaciones: ".$contadorUbicaciones."');</script>";
	
	if($cuantosIDs==$contadorUbicaciones)
	{
		//echo "<script>console.log('Coinciden en numero');</script>";

		#Recorrido de idFotos recolectados
		for($i=0;$i<$cuantosIDs;$i++)
		{
			$thisIDFoto = $arregloRecolectadosParaIdentificar[$i];
			$thisUbicacion = $arregloUbicacionesRecolectadas[$i];

			//echo "\nRecorrido #".$i.": idFoto=".$thisIDFoto." - idUbicacion=".$thisUbicacion;			

			#FRAGMENT
			$arrayEspecies   = explode(",",$especieValues);
			$cuantasEspecies = count($arrayEspecies);

			//echo "\nCuantas especies: ".$cuantasEspecies;

			for($j=0; $j<$cuantasEspecies; $j++)
			{
				$thisEspecie = $arrayEspecies[$j];
				$arrayThisEspecie = explode("|", $thisEspecie);

				$idEspecie         = $arrayThisEspecie[0];
				$claseEspecie      = $arrayThisEspecie[1];
				$individuosEspecie = $arrayThisEspecie[2];
				$machosEspecie     = $arrayThisEspecie[3];
				$hembrasEspecie    = $arrayThisEspecie[4];
				$juvenilEspecie    = $arrayThisEspecie[5];

				//echo "\n".$idEspecie." - ".$claseEspecie." - ".$individuosEspecie." - ".$machosEspecie." - ".$hembrasEspecie." - ".$juvenilEspecie;

				if($claseEspecie == 'Ave')
				{
					$claseNumero = '6';

					if($idEspecie == 9999)
						$aou = 'N/A';
					else
					{
						$sql_aou = mysqli_query($linkMySQL, "SELECT AOU_2012 FROM aves_ecoforestal WHERE id_aves_ecoforestal = '$idEspecie'");
						$mfa_aou = mysqli_fetch_row($sql_aou);
						$aou = $mfa_aou[0];
					}			
				}
				else if($claseEspecie == 'Mamifero')
				{
					$claseNumero = '9';
					$aou = 'N/A';
				}
				//AGREGAR PARA FER
				else if($claseEspecie == 'Indeterminado')
				{
					$claseNumero = 'N/A';
					$aou = 'N/A';
				}

				//echo "\nINSERT INTO registro_fototrampas (id_foto,id_clase,id_especie,aou2012,total_individuos,machos,hembras,juvenil) VALUES ('$thisIDFoto', '$claseNumero','$idEspecie', '$aou', '$individuosEspecie', '$machosEspecie', '$hembrasEspecie', '$juvenilEspecie')";
				$sqlInsertarEspecie = mysqli_query($linkMySQL, "INSERT INTO registro_fototrampas (id_foto,id_clase,id_especie,aou2012,total_individuos,machos,hembras,juvenil) VALUES ('$thisIDFoto', '$claseNumero','$idEspecie', '$aou', '$individuosEspecie', '$machosEspecie', '$hembrasEspecie', '$juvenilEspecie')");
				$affectedRowsInsertarEspecie = mysqli_affected_rows($linkMySQL);
				if($affectedRowsInsertarEspecie >0)
					$auxEspecies++;	
				
			}
			#########################
			if($auxEspecies == $cuantasEspecies)
			{
				//echo "\nSe insertaron correctamente todas las especies del id #".$thisIDFoto;
				$auxEspecies = 0;

				$sqlActualizarConcentrado = mysqli_query($linkMySQL, "UPDATE concentrado_fotos SET status = 'OK', ubicacion = '$thisUbicacion', quien_identifico= '$usuario_actual' WHERE concentrado_fotos.id_foto = $thisIDFoto ");
				$affectedRowsActualizarConcentrado = mysqli_affected_rows($linkMySQL);

				if($affectedRowsActualizarConcentrado > 0)
					$auxIDs++;
			}
						
		}	



		if($auxIDs == $cuantosIDs)
		{
			$okMsg = "Se identificaron correctamente todos los elementos seleccionados";
			//echo "\n".$okMsg;
			echo json_encode( array('okMsg' => $okMsg) );
		}	
		else
		{
			$errorMsg = 'No todos los elementos se guardaron en la base de datos';
			//echo "\n".$errorMsg;
			echo json_encode(array('errorMsg' => $errorMsg ));
		}
	}
	else if($cuantosIDs>$contadorUbicaciones)
	{
		$errorMsg = 'No coinciden los ID con las ubicaciones';
		//echo "\n".$errorMsg;
		echo json_encode(array('errorMsg' => $errorMsg ));
	}