<?php
	include("../includes/conexion.php");
	$linkMySQL = ConectarseMySQLMegaBD();

	$cargadosDatagrid = isset($_POST['cargadosDatagrid']) ? $_POST['cargadosDatagrid'] : '';

	if($cargadosDatagrid == '')
	{
		$myQuery = "SELECT * 
		FROM
		(
			SELECT CONCAT(id_aves_ecoforestal, ',', nombre_cientifico, ',Ave') AS id, id_aves_ecoforestal AS id_real, CONCAT('6','') AS clase, nombre_cientifico
			FROM aves_ecoforestal WHERE nombre_cientifico != 'Especie no identificada'
				UNION ALL
			SELECT CONCAT(idMamiferos, ',', nombre_cientifico, ',Mamifero') AS id, idMamiferos AS id_real, CONCAT('9','') AS clase, nombre_cientifico
			FROM mamiferos
		) AS q1
		WHERE q1.id_real > 0
		ORDER BY q1.nombre_cientifico ASC";		
	}
	else
	{
		$where ="";
		$arrayCargadosDatagrid = explode(",",$cargadosDatagrid);
		for($i=0;$i<count($arrayCargadosDatagrid);$i++)
		{
			$thisCargadosDatagrid = explode("|", $arrayCargadosDatagrid[$i]);
			$thisID = $thisCargadosDatagrid[0];
			$thisTipo = $thisCargadosDatagrid[1];

			if($thisTipo == 'Ave')
				$clase = 6;
			else if($thisTipo == 'Mamifero')
				$clase = 9;

			$where.= " AND NOT ( q1.id_real = '".$thisID."' AND q1.clase = '".$clase."' )";
		}

		$myQuery ="SELECT * 
		FROM
		(
			SELECT CONCAT(id_aves_ecoforestal, ',', nombre_cientifico, ',Ave') AS id, id_aves_ecoforestal AS id_real, CONCAT('6','') AS clase, nombre_cientifico
			FROM aves_ecoforestal WHERE nombre_cientifico != 'Especie no identificada'
				UNION ALL
			SELECT CONCAT(idMamiferos, ',', nombre_cientifico, ',Mamifero') AS id, idMamiferos AS id_real, CONCAT('9','') AS clase, nombre_cientifico
			FROM mamiferos
		) AS q1
		WHERE q1.id_real > 0".$where."ORDER BY q1.nombre_cientifico ASC";
	}

	$sql = mysqli_query($linkMySQL,$myQuery);
	$items = array();	
	while($row = mysqli_fetch_object($sql))
	{
		array_push($items, $row);
	}
	echo json_encode($items);