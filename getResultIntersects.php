<?php
    include ("../includes/conexion.php");
    $link = ConectarsePostgreSQL();

	#RECUPERAMOS VARIABLES
	$coordsPoint = $_POST['coordsPoint_WKTccl'];
	$coordsIntersects = $_POST['coordsIntersects_WKTccl'];


	#DETERMINAMOS SI EL PUNTO ESTA DENTRO DEL MUNICIPIO/PREDIO (ST_INTERSECTS)
	$sqlIntersects = "WITH buffer AS 
    (
        SELECT ST_GeomFromText('$coordsIntersects') AS geom
    )
    SELECT COUNT(*)
    FROM buffer
    WHERE ST_Intersects (ST_GeomFromText('$coordsPoint'),buffer.geom)";
	$sqlIntersectsQuery = pg_query($link, $sqlIntersects);
    while($row = pg_fetch_row($sqlIntersectsQuery))
    {
        $countResult = $row[0];
    }

    if($countResult > 0)
    {
    	echo "OK";
    }
    else
    {
    	echo "EL PUNTO NO ESTA DENTRO DEL MUNICIPIO_PREDIO SELECCIONADO";
    }
