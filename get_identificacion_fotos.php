<?php
error_reporting(0);
date_default_timezone_set("Mexico/General");
session_start();
$usuario_actual = $_SESSION['usuario'];
include "../includes/conexion.php";
$linkMySQL = ConectarseMySQLMegaBD();

$page   = isset($_POST['page']) ? intval($_POST['page']) : 1;
$rows   = isset($_POST['rows']) ? intval($_POST['rows']) : 19;
$sort   = isset($_POST['sort']) ? strval($_POST['sort']) : 'ruta';
$order  = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
$offset = ($page - 1) * $rows;

#REQUEST VALUES
$filterArray = isset($_REQUEST['filterRules']) ? json_decode($_REQUEST['filterRules'], true) : '';
$num_filter  = count($filterArray);
//print_r($filterArray);
//print_r($num_filter);

if ($num_filter === 0) {
    $whereFilter = "";
} else if ($filterArray != '' && $num_filter > 0) {
    //echo "filtro activo\n";
    $whereFilter = "";
    for ($i = 0; $i < $num_filter; $i++) {
        $filterField    = $filterArray[$i]['field'];
        $filterOperator = $filterArray[$i]['op'];
        $filterValue    = $filterArray[$i]['value'];

        $mValues      = explode(",", $filterValue);
        $countmValues = count($mValues);
        for ($j = 0; $j < $countmValues; $j++) {
            $filterValue = $mValues[$j];

            #EXCEPCION PARA LAS ABREVIATURAS DE ZONA
            if ($filterField == 'zona_municipio') {
                if ($filterValue == 'Tlacotalpan') {
                    $filterValue = 'TLACO';
                } else if ($filterValue == 'Costa de la palma') {
                    $filterValue = 'CPALMA';
                }
            }
            ########################################

            #EXCEPCION PARA LAS FECHAS (MESES)
            if ($filterField == 'fecha') {
                $explodedValue = explode(" ", $filterValue);
                $mes           = $explodedValue[0];
                $anyo          = $explodedValue[1];
                switch ($mes) {
                    case 'Enero':
                        $mes = '01';
                        break;
                    case 'Febrero':
                        $mes = '02';
                        break;
                    case 'Marzo':
                        $mes = '03';
                        break;
                    case 'Abril':
                        $mes = '04';
                        break;
                    case 'Mayo':
                        $mes = '05';
                        break;
                    case 'Junio':
                        $mes = '06';
                        break;
                    case 'Julio':
                        $mes = '07';
                        break;
                    case 'Agosto':
                        $mes = '08';
                        break;
                    case 'Septiembre':
                        $mes = '09';
                        break;
                    case 'Octubre':
                        $mes = '10';
                        break;
                    case 'Noviembre':
                        $mes = '11';
                        break;
                    case 'Diciembre':
                        $mes = '12';
                        break;
                    default:
                        break;
                }
                $filterValue = $anyo . "-" . $mes;
            }
            ########################################

            if ($countmValues > 1) {
                if ($j === 0) {
                    $whereFilter .= " AND( lower(q1.$filterField) LIKE lower('%$filterValue%')";
                } else if ($j !== 0 && $j !== ($countmValues - 1)) {
                    $whereFilter .= " OR lower(q1.$filterField) LIKE lower('%$filterValue%')";
                } else if ($j === ($countmValues - 1)) {
                    $whereFilter .= " OR lower(q1.$filterField) LIKE lower('%$filterValue%')  )";
                }

            } else {
                switch ($filterOperator) {
                    case 'contains':
                        $whereFilter .= " AND ( lower(q1.$filterField) LIKE lower('%$filterValue%')  )";
                        break;
                    case 'equal';
                        $whereFilter .= " AND q1." . $filterField . " = '" . $filterValue . "'";
                        break;
                    case 'notequal';
                        $whereFilter .= " AND q1." . $filterField . " <> '" . $filterValue . "'";
                        break;
                    case 'beginwith';
                        $whereFilter .= " AND q1." . $filterField . " LIKE '" . $filterValue . "%'";
                        break;
                    case 'endwith';
                        $whereFilter .= " AND q1." . $filterField . " LIKE '%" . $filterValue . "'";
                        break;
                    case 'less';
                        $whereFilter .= " AND q1." . $filterField . " < '" . $filterValue . "'";
                        break;
                    case 'lessorequal';
                        $whereFilter .= " AND q1." . $filterField . " <= '" . $filterValue . "'";
                        break;
                    case 'greater';
                        $whereFilter .= " AND q1." . $filterField . " > '" . $filterValue . "'";
                        break;
                    case 'greaterorequal';
                        $whereFilter .= " AND q1." . $filterField . " >= '" . $filterValue . "'";
                        break;
                    case 'range':
                        #TODO: Separar el filterValue en 2 variables
                        $arrayFilterValue = explode(":", $filterValue);
                        $f1               = $arrayFilterValue[0];
                        $f2               = $arrayFilterValue[1];
                        $whereFilter .= " AND (q1.fecha BETWEEN '$f1' AND '$f2')";
                        break;
                }
            }
        }
    }
}

//$where = "WHERE status = 'PENDIENTE'"; //Original
$where = "WHERE status = 'PENDIENTE' AND(zona='SMO' OR zona='Jalacingo') OR (zona ='TLACO' AND fecha < '2018-05-01' AND status = 'PENDIENTE')"; //Para Fer

$select       = "SELECT id_foto,id_ct,foto, marca,modelo,fecha,hora,ruta,status, zona_municipio, idubicacion_ct, idubicacion_ct AS icono_ubicacion, registro_ubicaciones.zona, registro_ubicaciones.utmx, registro_ubicaciones.utmy, destacada ";
$selectCount  = "SELECT count(*) ";
$consultaBase = "FROM
	(
		SELECT id_foto,id_ct,foto, marca,modelo,fecha,hora,ruta,status, concentrado_fotos.zona AS zona_municipio, destacada
		FROM concentrado_fotos
		" . $where . "
		ORDER BY ruta DESC
	) q1
	LEFT JOIN registro_ubicaciones ON registro_ubicaciones.idubicacion_ct=
	(
		SELECT MIN(idubicacion_ct)
		FROM registro_ubicaciones
		WHERE ( fecha BETWEEN fecha_inicio AND fecha_fin ) AND ( id_ct=CONCAT('C',id_camara) )
	)
	WHERE id_foto>0" . $whereFilter . " ORDER BY $sort $order LIMIT $offset,$rows";

//echo $consultaBase;

$rs              = mysqli_query($linkMySQL, $selectCount . $consultaBase);
$row             = mysqli_fetch_row($rs);
$result["total"] = $row[0];

$rs    = mysqli_query($linkMySQL, $select . $consultaBase);
$items = array();
while ($row = mysqli_fetch_object($rs)) {
    $icono_ubicacion = $row->icono_ubicacion;
    if ($icono_ubicacion == '') {
        $row->icono_ubicacion = '<img src="images/exclamation.png" title="No se ha capturado la ubicación">';
    } else {
        $row->icono_ubicacion = '<img src="images/tick.png" title="De clic sobre el ícono para mas información" onclick="javascript:showUbicacionWindow(' . $icono_ubicacion . ')">';
    }

    $icono_destacada = $row->destacada;

    if ($icono_destacada == '') {
        $row->destacada = "<img src='images/clock_15.png' title='Pendiente por destacar'>";
    } else if ($icono_destacada == 'Si') {
        $row->destacada = "<img src='images/award_star_gold_red.png' title='Fotografía destacada'>";
    } else if ($icono_destacada == 'No') {
        $row->destacada = "<img src='images/award_star_silver_blue.png' title='No es una fotografía destacada'>";
    }

    array_push($items, $row);
}

$result["rows"] = $items;

mysqli_free_result($rs);
mysqli_close($linkMySQL);
echo json_encode($result);
