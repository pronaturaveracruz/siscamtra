<?php
    error_reporting(0);
    date_default_timezone_set("Mexico/General");
    session_start();
    $usuario_actual= $_SESSION['usuario'];
    include ("../includes/conexion.php");
    $link = Conectarse();
    $linkMySQL = ConectarseMySQLMegaBD();

    #VERIFICAMOS SI EXISTE ALGUN USUARIO LOGEADO
    if($usuario_actual)
    {
        #VERIFICAMOS SI EL USUARIO DE LA SESIÓN ESTA AUTORIZADO PARA ENTRAR A ESTA PLATAFORMA
        $sqlUsuarioValido = mysqli_query($linkMySQL,"SELECT COUNT(*)
        FROM sis_control_acceso
        LEFT JOIN plataforma ON sis_control_acceso.id_plataforma=plataforma.id_plataforma
        LEFT JOIN usuario ON sis_control_acceso.id_usuario=usuario.id_usuario
        WHERE sis_control_acceso.id_plataforma='5' AND usuario.nombre_usuario='$usuario_actual'");
        while($row = mysqli_fetch_row($sqlUsuarioValido))
        {
            $cuantos = $row[0];
        }
        if($cuantos == 0) #USUARIO NO AUTORIZADO
        {
            session_destroy();
            echo "<script>parent.window.location.reload();</script>";
            exit;
        }
        else if($cuantos > 0)
        {
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/myIcons.css">
    <link rel="stylesheet" type="text/css" href="css/myForm.css">
	<link rel="stylesheet" type="text/css" href="includes/jquery-easyui-1.5.2/themes/material/easyui.css">
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/jquery.min.js"></script>
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/jquery.easyui.min.js"></script>
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/locale/easyui-lang-es.js"></script>
    <script type="text/javascript" src="includes/datagrid-filter/datagrid-filter.js"></script>
    <script type="text/javascript" src="includes/datagrid-view/datagrid-scrollview.js"></script>
    <!--OpenLayers-->
    <link rel="stylesheet" href="includes/Openlayers_3.19.1/ol.css" type="text/css">
    <script src="includes/Openlayers_3.19.1/ol-debug.js"></script>
    <!--Proj4-->
    <script src="includes/proj4js-2.3.15/dist/proj4.js"></script>
    <!--jQuery imageLens-->
    <script src="js/jquery.imageLens.js" type="text/javascript"></script>
    <script src="js/jquery.imageLens2.js" type="text/javascript"></script>
    <!--jQuery FancyBox-->
    <script type="text/javascript" src="includes/fancybox-2.1.7/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" href="includes/fancybox-2.1.7/jquery.fancybox.css" type="text/css" media="screen" />
    <!--jQuery PanZoom-->
    <script src="js/jquery.panzoom.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
    <!--BigScreen-->
    <script src="js/bigscreen.min.js"></script>


	<style>
        body
        {
            font-family:Roboto;
            font-size:12px;
            padding: 20px 20px 0 20px;
            margin:0;
        }
        /*SI SE DEFINIERON POR HTML*/
        input.easyui-textbox{text-align:center;}
        input.easyui-combobox{text-align:center;}
        input.easyui-numberbox{text-align:center;}
        input.easyui-datebox{text-align:center;}
        select.easyui-combobox{text-align:center;}
        /*SI SE DEFINIERON POR JS (APLICAMOS PARA TODOS: TEXT, COMBO, NUMBER,ETC)*/
        .textbox-text{text-align:center;color:crimson;font-weight:bold;}
        .textbox-readonly .textbox-text{background:#EEEEEE;text-align:center;color:dimgray;font-weight:bold;}
        /*PROPIEDADES PARA LOS TOOLTIPS*/
        .panelttUbicacionOK{padding-left:10px;background-color: #486647;color:#FFFFFF;}
        .panelttUbicacionNoEncontada{padding-left:10px;background-color: #8A1616;color:#FFFFFF;}

        video::-webkit-media-controls-fullscreen-button {}

    </style>

  	<script>
        /*OVERRIDES*/
        //FUNCION PARA LIMITAR UNICAMENTE LA SELECCION A LOS ELEMENTOS DE LA LISTA (COMBOBOX)
		$.extend($.fn.validatebox.defaults.rules,{
			inList:{
				validator:function(value,param){
					var c = $(param[0]);
					var opts = c.combobox('options');
					var data = c.combobox('getData');
					var exists = false;
					for(var i=0; i<data.length; i++){
						if (value == data[i][opts.textField]){
							exists = true;
							break;
						}
					}
					return exists;
				},
				message:'Debe seleccionar un elemento de la lista.'
			}
		});
        //ONLY NUMBERS IN TEXTBOX
        $.extend($.fn.validatebox.defaults.rules,{
            onlyNumbers:
            {
                validator: function(value,param)
                {
                    if(  /^\d+$/.test(value)  )
                        return value;
                },
                message: 'Solo se aceptan numeros'
            }
        });

        //FUNCION PARA FORMATEAR LOS CAMPOS DATEBOX (GUIONES, DIAGONALES, ETC.)
        $.fn.datebox.defaults.formatter = function(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            return y + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
        };
        $.fn.datebox.defaults.parser = function(s)
        {
            if (s)
            {
                var a = s.split('-');
                var d = new Number(a[2]);
                var m = new Number(a[1]);
                var y = new Number(a[0]);
                var dd = new Date(y, m-1, d);
                return dd;
            }
            else
            {
                return new Date();
            }
        };

        //FUNCION PARA MOSTRAR U OCULTAR LOS TEXBOX
        $.extend($.fn.textbox.methods, {
            show: function(jq){
                return jq.each(function(){
                    $(this).next().show();
                })
            },
            hide: function(jq){
                return jq.each(function(){
                    $(this).next().hide();
                })
            }
        });        
  	</script>
    <script>
    	/*FUNCIONES*/
        function formatZona(val, row) 
        {
            if (val == 'Jalacingo') return '<span style="background-color:#548235; color:white; padding:2px;">' + val + '</span>';
            else if (val == 'NORTE') return '<span style="background-color:#C00000; color:white; padding:2px;">' + 'Norte' + '</span>';
            else if (val == 'SMO') return '<span style="background-color:#305496; color:white; padding:2px;">' + val + '</span>';
            else if (val == 'TLACO') return '<span style="background-color:#BF8F00; color:white; padding:2px;">' + 'Tlacotalpan' + '</span>';
            else if (val == 'CPALMA') return '<span style="background-color:#7030A0; color:white; padding:2px;">' + 'Costa de la palma' + '</span>';
            else return val;
        }

        function showUbicacionWindow(idubicacion)
        {
            console.warn("Show windowUbicacion info with id: " + idubicacion);
            $('#windowUbicacion').window('open').window('center');
        }

        function destacar(opcion)
        {
            var recolectados_a_destacar='';
            var rows = $('#dgVSI').datagrid('getChecked');
            var longitud=rows.length; 

            if(opcion=='SI')
                $('#dlgRecolectadasParaDestacar').dialog({ iconCls: 'icon-destacada', title: 'Definir chequeadas como: Destacado' }).dialog('open');
            else if(opcion=='NO')
                $('#dlgRecolectadasParaDestacar').dialog({ iconCls: 'icon-nodestacada', title: 'Definir chequeadas como: No destacado' }).dialog('open');
            

            for(var i=0; i<longitud; i++)
            {
                var row = rows[i];
                if(i==0)
                    recolectados_a_destacar+= row.id_video;
                else
                    recolectados_a_destacar+= ","+ row.id_video;
            }
            $('#textboxRecolectadasParaDestacar').textbox('setValue',recolectados_a_destacar);
            $('#textboxOperacion').textbox('setValue',opcion);
        }

        function destacarenBD()
        {
            var recolectadosParaDestacar = $('#textboxRecolectadasParaDestacar').textbox('getValue');
            var opcion = $('#textboxOperacion').textbox('getValue');

            $('#formRecolectadasParaDestacar').form('submit',{
                url: 'destacar_video.php',
                onSubmit: function(param)
                {
                    //Extra params
                    param.recolectadosParaDestacar = recolectadosParaDestacar;
                    param.opcion = opcion;

                    return $(this).form('enableValidation').form('validate');
                },
                success: function(result)
                {
                    var result = eval('('+result+')');
                    if (result.errorMsg)
                    {
                        $.messager.show({
                            title: 'Error',
                            msg: result.errorMsg,
                            height:'auto'
                        });
                    } 
                    else 
                    {
                        if(result.okMsg)
                        {
                            $.messager.show({
                                title: 'Aviso',
                                msg: result.okMsg,
                                height:'auto'
                            });
                            $('#dlgRecolectadasParaDestacar').dialog('close');
                            $('#dgVSI').datagrid('clearSelections');
                            $('#dgVSI').datagrid('clearChecked');
                            $('#dgVSI').datagrid('reload');
   
                        }
                    }
                }
            });
        }

        function limpiar_selecciones()
        {
            $('#dgVSI').datagrid('clearChecked');
        }

        function getCheckedRows()
        {
            var idsRecolectados = '';
            var ubicacionesRecolectadas = '';
            var contadorUbicaciones = 0;

            var rows = $('#dgVSI').datagrid('getChecked');
            var longitud = rows.length;

            $('#dlgRecolectadasParaIdentificar').dialog('open');

            for(var i=0; i<rows.length; i++)
            {
                var row = rows[i]; 
                var thisLocation = row.idubicacion_ct;
                if(thisLocation!=null)
                {
                    contadorUbicaciones++;
                    //(UBICACIONES) SI ES EL PRIMER REGISTRO OMITIMOS PONER LA COMA
                    if(contadorUbicaciones==1)
                         ubicacionesRecolectadas+= thisLocation;                               
                    else
                        ubicacionesRecolectadas+= "," + thisLocation;
                }

                //(ID VIDEOS) SI ES EL PRIMER REGISTRO OMITIMOS PONER LA COMA
                if(i==0)
                    idsRecolectados+= row.id_video;
                else
                    idsRecolectados+= ","+ row.id_video;
            }

            $('#idsRecolectados').textbox('setValue',idsRecolectados);
            $('#ubicacionesRecolectadas').textbox('setValue',ubicacionesRecolectadas);
            $('#cuantosVideos').textbox('setValue',longitud);
            $('#cuantasUbicaciones').textbox('setValue',contadorUbicaciones);

            if(  (longitud==contadorUbicaciones) && (longitud != 0)  )
            {
                $('#resultado').textbox('setValue','OK');
                $('#resultado').textbox('textbox').css('background-color','#3ADF00');
                $('#resultado').textbox('textbox').css('color','#0B6121');
                $('#complementoResultado').textbox('setValue','OK');
            }
            else
            {
                $('#resultado').textbox('setValue','ERROR');
                $('#resultado').textbox('textbox').css('background-color','#FA5858');
                $('#resultado').textbox('textbox').css('color','#8A0808');
                $('#complementoResultado').textbox('setValue','');
            }

        }

        function identificar()
        {
            var recolectadosParaIdentificar = $('#idsRecolectados').textbox('getValue');
            var ubicacionesRecolectadas = $('#ubicacionesRecolectadas').textbox('getValue');

            $('#formRecolectadasParaIdentificar').form('submit',{
                url: 'identificar_video.php',
                onSubmit: function(param)
                {
                    //Extra params
                    param.recolectadosParaIdentificar = recolectadosParaIdentificar;
                    param.ubicacionesRecolectadas = ubicacionesRecolectadas;             

                    return $(this).form('enableValidation').form('validate');
                },
                success: function(result)
                {
                    var result = eval('('+result+')');
                    if (result.errorMsg)
                    {
                        $.messager.show({
                            title: 'Error',
                            msg: result.errorMsg,
                            height:'auto'
                        });
                    } 
                    else 
                    {
                        if(result.okMsg)
                        {
                            $.messager.show({
                                title: 'Aviso',
                                msg: result.okMsg,
                                height:'auto'
                            });
                            $('#dlgRecolectadasParaIdentificar').dialog('close');
                            $('#dgVSI').datagrid('clearSelections');
                            $('#dgVSI').datagrid('clearChecked');
                            $('#dgVSI').datagrid('reload');
   
                        }
                    }
                }
            });
        }

        function getDatagridValues(myDatagrid)
        {
            var coleccionID = "";
            var dg = eval(   "$('#"+myDatagrid+"')"    );
            var total = dg.datagrid('getData').total;
            var rows  = dg.datagrid('getData').rows;

            //console.warn(rows);


            for(var i=0;i<total;i++)
            {
                
                //Excepcion para: 'Especie no identificada'
                if(rows[i].id == 9999)
                {
                    //console.log('[Desde la funcion getDatagridValues] la especie es N/A');
                    //var clase = 'Mamifero';
                    var clase = rows[i].type;
                }
                else
                {
                   var clase = rows[i].type; 
                }            

                if(i==0)
                    coleccionID = rows[i].id + "|" + clase + "|" + rows[i].individuos + "|" + rows[i].machos + "|" + rows[i].hembras + "|" + rows[i].juvenil;
                else
                    coleccionID+= "," + rows[i].id + "|" + clase + "|" + rows[i].individuos + "|" + rows[i].machos + "|" + rows[i].hembras + "|" + rows[i].juvenil;   
            }
            return coleccionID;
        }

        function agregarDetalleEspecie()
        {
            var especieID = $('#especieSeleccionada').textbox('getValue');
            //SEPARAMOS EL VALOR DE LA CONSULTA PARA PODER AGREGARLO A CADA CAMPO DEL DATAGRID
            especieID = especieID.split(",");
            var id = especieID[0];
            var nombre_cientifico = especieID[1];
            var tipo = especieID[2];

            if(id == 9999)
            {
                var individuos = $('#individuos').numberspinner('getValue');;
                var machos     = 0;
                var hembras    = 0;
                var juvenil    = 0;
                //tipo = 'N/A';
                tipo = $('#clasificacion').combobox('getValue');
            }
            else
            {
                //VALORES DEL FORMULARIO
                var individuos = $('#individuos').numberspinner('getValue');
                var machos     = $('#machos').numberspinner('getValue');
                var hembras    = $('#hembras').numberspinner('getValue');
                var juvenil    = $('#juvenil').numberspinner('getValue');  
            }            

            $('#formDetalleEspecie').form('submit',{
                onSubmit: function()
                {                  
                    return $(this).form('enableValidation').form('validate');
                },
                success: function(data)
                {
                    //COMPROBAMOS QUE LA SUMA DE MACHOS, HEMBRAS, JUVENIL SEA MENOR O IGUAL AL NUMERO DE INDIVIDUOS
                    var individuosInt = parseInt(individuos);
                    var machosInt     = parseInt(machos);
                    var hembrasInt    = parseInt(hembras);
                    var juvenilInt    = parseInt(juvenil);
                    var sumaTotal  = parseInt(machosInt+hembrasInt+juvenilInt);

                    //console.log('Suma: ' + sumaTotal);

                    if(sumaTotal<=individuosInt)
                    {
                        //console.log('Ok, continuar...');   
                        $('#dgEspecies').datagrid('appendRow', {
                            id: id,
                            scientific_name: nombre_cientifico,
                            type: tipo,
                            individuos: individuos,
                            machos: machos,
                            hembras: hembras,
                            juvenil: juvenil
                        });

                        $('#btnAgregarEspecie').linkbutton('disable');
                        //FUNCIÓN PARA RECOLECTAR TODOS LOS ID DEL DATAGRID 
                        var coleccionID = getDatagridValues('dgEspecies');
                        $('#especieValues').textbox('setValue', coleccionID);
                        $('#especie').combobox('reset');
                        $('#especie').combobox({onBeforeLoad: function(param) {param.cargadosDatagrid = coleccionID;}});
                        $('#dlgDetalleEspecie').dialog('close');
                        $('#dgEspecies').datagrid('reload');                     
                    }
                    else
                    {
                        $.messager.show({
                            title:'Advertencia',
                            msg:'Los machos,hembras y juveniles no coinciden con los individuos',
                            timeout:5000,
                            height:'auto',
                            showType:'slide'
                        });
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////                    
                }
            });
        }



    	/**/
    </script>
</head>
<body>
    <table style="margin:0 auto 0 auto;">
      <tr>
        <td>
            <table id="dgVSI">
                <thead>
                    <tr>
                        <th data-options="field:'ck', align:'center', checkbox:true"></th>
                         <th data-options="field:'id_video', align:'center', width:90">ID</th>
                         <th data-options="field:'id_ct', align:'center', width:70">Camara</th>
                         <th data-options="field:'fecha',align:'center', width:120">Fecha</th>
                         <th data-options="field:'hora',align:'center', width:70">Hora</th>
                         <th data-options="field:'zona_municipio',align:'center', width:150" formatter="formatZona">Zona</th>

                         <?php if ($usuario_actual != 'invitado') {?>
                         <th data-options="field:'icono_ubicacion',align:'center', width:75">Ubicación</th>
                         <?php }?>

                         <th data-options="field:'destacado',align:'center', width:90">Destacado</th>
                    </tr>
                </thead>
            </table>
        </td>
        <td>
            <div id="panelVideo" style='display:flex; align-items:center; justify-content:center; width:100%; height:100%; text-align:center;'>                    
                    <header style="line-height: 15px; font-family: Roboto; font-weight: bold; font-size: 12px;">
                            <img align= "absmiddle" src="images/film.png">
                            <label>Vista previa</label>
                    </header>

                    <video controls controlsList="nodownload" id="vid_01" style="width:600px; height:450px; min-width: 100%; min-height: 100%; display: none;"></video>
                    <img id="imgCargando" src="images/spinner2.gif" width="600" height="450" style="display:none;" /> <!-- Cache the GIF image -->

            </div>
        </td>
      </tr>
    </table>
    <div id="myFooter" style="border:0px solid coral; width:610px; height:20px; background:#f0f0f0; margin-top:-3px; padding-top:5px; margin-left:7px; text-align:right; font-family: Roboto; font-size: 12px; color:dimgray;"></div>

    <?php
        if($usuario_actual != 'invitado' && $usuario_actual != 'comunicacion' && $usuario_actual != 'jcobos')
        {
    ?>
            <div id="dgVSIToolbar">
                <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-identificar',plain:'true'" onclick="getCheckedRows()">Identificar</a>
                <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-limpiar',plain:'true'" onclick="limpiar_selecciones()">Deshacer selección</a>
                <a style="padding-left:130px;">Definir chequeadas como: </a>
                <a href="#" class="easyui-linkbutton easyui-tooltip" title="Destacado" data-options="iconCls:'icon-destacada',plain:'true'" onclick="destacar('SI')"></a>
                <a href="#" class="easyui-linkbutton easyui-tooltip" title="No destacado" data-options="iconCls:'icon-nodestacada',plain:'true'" onclick="destacar('NO')"></a>
            </div>
    <?php
        }
    ?>

    <div id="windowUbicacion">
        <table id="dgUbicacion">
        <thead>
              <th data-options="field:'id_ct', align:'center'">Camara</th>
              <th data-options="field:'clave',align:'center'">Clave</th>
              <th data-options="field:'nombre_predio',align:'center'">Predio</th>
              <th data-options="field:'zona',align:'center'">Zona</th>
              <th data-options="field:'utmx',align:'center'">X</th>
              <th data-options="field:'utmy',align:'center'">Y</th>             
              <th data-options="field:'municipio',align:'center'">Estado</th>
              <th data-options="field:'estado',align:'center'">Municipio</th>
              <th data-options="field:'fecha_inicio',align:'center'">Fecha de instalación</th>
              <th data-options="field:'fecha_fin',align:'center'">Fecha de finalización</th>
              <th data-options="field:'observador',align:'center'">Observador</th>
            </thead>
          </table>
    </div>

    <div id="dlgRecolectadasParaDestacar">
        <form id="formRecolectadasParaDestacar" method="post">
            <div style="text-align: center; padding-top:10px;">
                <label style="width:420px;">¿Esta seguro de aplicar los cambios a los videos con los siguientes IDs?</label>
                <input name="textboxRecolectadasParaDestacar" id="textboxRecolectadasParaDestacar">
                <input name="textboxOperacion" id="textboxOperacion">
            </div>
        </form>
    </div>   

    <div id="dlgRecolectadasParaIdentificar">
        <form id="formRecolectadasParaIdentificar" method="post">
            <div style="text-align: center; padding-top: 10px;">
                <label style="width: 160px;">Videos seleccionados:</label>
                <input id="cuantosVideos" name="cuantosVideos">
                <input id="idsRecolectados" name="idsRecolectados">

                <label style="width:125px; padding-left:20px;">Ubicaciones validas:</label>
                <input id="cuantasUbicaciones" name="cuantasUbicaciones">
                <input id="ubicacionesRecolectadas" name="ubicacionesRecolectadas">

                <label style="width:70px; padding-left:30px;">Resultado:</label>
                <input id="resultado" name="resultado">
                <input id="complementoResultado" name="complementoResultado">
            </div>

            <div style="padding-left: 14px; padding-top: 10px;">                
                <table id="dgEspecies"></table>
                <div id="toolbarEspecies">
                    <input id="especie">
                    <a id="btnAgregarEspecie"></a>
                    <a id="btnQuitarEspecie"></a>
                    <!--<a id="btnEspecieNoIdentificada"></a>-->
                    <a style="padding-left:50px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                    <input id="especieResult">
                </div>
                <input id="especieValues" name="especieValues">




            </div>
        </form>
    </div>

    <div id="dlgDetalleEspecie">
        <form id="formDetalleEspecie" method="post">
            <div style="text-align: center; padding-top: 10px;">
                <label id="lblEspecieSeleccionada" style="width: 200px;">Especie seleccionada:</label>
                <input id="especieSeleccionada" name="especieSeleccionada">
            </div>

            <div style="text-align: center; padding-top: 10px;">
                <label style="width: 200px;">Numero de individuos:</label>
                <input id="individuos" name="individuos">
            </div>

            <!--Unicamente disponible cuando se elige especie 'No identificada'-->
            <div style="text-align: center; padding-top: 10px;">
                <label style="width: 200px;">Clasificación:</label>
                <input id="clasificacion" name="clasificacion">
            </div>
            <!--/////////////////////////////////////////////////////////////////-->

            <div style="text-align: center; padding-top: 10px;">
                <label style="width: 200px;">Machos:</label>
                <input id="machos" name="machos">
            </div>

            <div style="text-align: center; padding-top: 10px;">
                <label style="width: 200px;">Hembras:</label>
                <input id="hembras" name="hembras">
            </div>

            <div style="text-align: center; padding-top: 10px;">
                <label style="width: 200px;">Juvenil:</label>
                <input id="juvenil" name="juvenil">
            </div>
        </form>
    </div>



  <script>
   $(function()
   { 
        //DISBLE RIGHT CLICK IN VIDEO 
        $("#vid_01").bind("contextmenu",function(){
            return false;
        });      


        $('#dgVSI').datagrid({
          iconCls: 'icon-tabla',
          title:'Videos sin identificar',
          width: 610,
          height:550,
          url:'get_identificacion_videos.php',
          toolbar: '#dgVSIToolbar',
          idField:'id_video',
          autoRowHeight:false,
          pageSize: 19,
          view: scrollview,
          remoteFilter:true,
          remoteSort:true,
          rownumbers:true,            
          nowrap:true,
          fitColumns:true,
          striped:true,
          singleSelect: true,
          selectOnCheck: false,
          checkOnSelect: false,
          onLoadSuccess:function()
          {
            //DISABLE CHECKBOX [HEADER]
            $(this).datagrid('getPanel').find('div.datagrid-header input[type=checkbox]').attr('disabled','disabled');

            $('#myFooter').fadeOut(300, function() {
                $(this).html('<span style="margin:0 0 0 0;">Numero de elementos: <b style="color:crimson;">' + $("#dgVSI").datagrid('getData').total + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span>').fadeIn(300);
            });


          },
          onClickRow:function(index,row)
          {
            document.getElementById("vid_01").style.display = "none";
            document.getElementById("imgCargando").style.display = "inline";

            $.post('get_video.php', {idvideo: row.id_video}, function(result){
                $('#vid_01')[0].src = result;
                document.getElementById("imgCargando").style.display = "none";
                document.getElementById("vid_01").style.display = "inline";
          	});

            $('#dgUbicacion').propertygrid({url:'get_tabla_ubicacion.php?idubicacion=' + row.idubicacion_ct});
          }
        }).datagrid('enableFilter',[

            {
                field:'id_video',
                type:'textbox',
                options:
                {
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).textbox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgVSI').datagrid('removeFilterRule','id_video').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).textbox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgVSI').datagrid('addFilterRule', {
                                field: 'id_video',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }
            },
            {
                field:'id_ct',
                type:'textbox',
                options:{
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).textbox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgVSI').datagrid('removeFilterRule','id_ct').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {                   
                        if(value != '')
                        {
                            $(this).textbox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgVSI').datagrid('addFilterRule', {
                                field: 'id_ct',
                                op: 'contains',
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }                
            },
            {
                field:'fecha',
                type:'combobox',
                options:{

                    editable:false,
                    multiple:true,
                    panelWidth:'auto',
                    panelHeight:'auto',
                    panelMinWidth:120,
                    //...
                    url:'getFechasFilterVideos.php',
                    valueField:'fecha_cortada',
                    textField:'fecha_cortada',
                    //...
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).combobox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgVSI').datagrid('removeFilterRule','fecha').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {
                        if(value != '')
                        {
                            //console.log(value);
                            var valueConcat = "";
                            for(var i=0;i<value.length;i++)
                            {
                                if(i===0)
                                    valueConcat+= value[i];
                                else
                                    valueConcat+= "," + value[i];   
                            }
                            //console.log(valueConcat);
                            $(this).combobox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgVSI').datagrid('addFilterRule', {
                                field: 'fecha',
                                op: 'contains',
                                value: valueConcat
                            }).datagrid('doFilter');
                        }
                        else
                        {
                            //console.log('empty');
                            $(this).combobox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                            $('#dgVSI').datagrid('removeFilterRule','fecha').datagrid('doFilter');
                        }                        
                    }





                }
            },
            {
                field:'hora',
                type:'label'
            },
            {
                field:'zona_municipio',
                type:'combobox',
                options:{
                    editable:false,
                    multiple:true,
                    panelWidth:'auto',
                    panelHeight:'auto',
                    panelMinWidth:120,
                    url:'getZonasFilter.php',
                    valueField:'zona',
                    textField:'zona',
                    icons:
                    [
                        {
                            iconCls:'icon-clear',
                            handler:function(e)
                            {
                                $(e.data.target).combobox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                                $('#dgVSI').datagrid('removeFilterRule','zona_municipio').datagrid('doFilter');
                            }
                        }
                    ],
                    onChange:function(value)
                    {
                        if(value != '')
                        {
                            //console.log(value);
                            var valueConcat = "";
                            for(var i=0;i<value.length;i++)
                            {
                                if(i===0)
                                    valueConcat+= value[i];
                                else
                                    valueConcat+= "," + value[i];   
                            }
                            //console.log(valueConcat);
                            $(this).combobox('textbox').css({backgroundColor: '#BAD7C9', color: '#515D57'});
                            $('#dgVSI').datagrid('addFilterRule', {
                                field: 'zona_municipio',
                                op: 'equal',
                                value: valueConcat
                            }).datagrid('doFilter');
                        }
                        else
                        {
                            //console.log('empty');
                            $(this).combobox('textbox').css({backgroundColor: '#FFFFFF', color: 'crimson'});
                            $('#dgVSI').datagrid('removeFilterRule','zona_municipio').datagrid('doFilter');
                        }                        
                    }





                }
            },
            {
                field:'icono_ubicacion',
                type:'label'
            },
            {
                field:'destacado',
                type:'label'
            }


        ]);






        $('#panelVideo').panel({          
          width: 610,
          height: 550,     
          title: 'Vista previa', 
          iconCls: 'icon-videos'
        }); 


        $('#windowUbicacion').window({
            title:'Ubicación',
            iconCls:'icon-ubicaciones',
            width:300,
            height:338,
            closed:true,
            modal:true,
            collapsible:false,
            minimizable:false,
            maximizable:false,
            draggable:false,
            resizable:false
        });

        $('#dgUbicacion').propertygrid({
           scrollbarSize: 0,
           striped:true,
           width:'auto',
           columns:
           [[
                {field:'name',title:'Columna',width:100, resizable:false},
                {field:'value',title:'Valor', width:150,  resizable:false}
           ]]
        });

        $('#dlgRecolectadasParaDestacar').dialog({
            width:510,
            height:160,
            closed:true,
            modal:true,
            draggable:false, 
            collapsible:false, 
            resizable:false,
            buttons:
            [
                {
                    text:'Aceptar',
                    iconCls:'icon-save',
                    handler:function(e)
                    {
                        destacarenBD();
                    }
                },
                {
                    text:'Cancelar',
                    iconCls:'icon-cancel',
                    handler:function(e) 
                    {
                        $('#dlgRecolectadasParaDestacar').dialog('close');
                    }     
                }
            ],
            onOpen: function()
            {
                $('#formRecolectadasParaDestacar').form('clear').form('disableValidation').form('validate'); 
                $('#textboxOperacion').textbox('hide');
            }
        });

        $('#formRecolectadasParaDestacar').form({
            novalidate:true
        });

        $('#formRecolectadasParaIdentificar').form({
            novalidate:true
        });

        $('#textboxRecolectadasParaDestacar').textbox({
            width:450,
            height:50,
            editable:false,
            required:true,
            multiline:true
        });

        $('#textboxOperacion').textbox({
            width:250,
            editable:false,
            required:true
        });

        $('#dlgRecolectadasParaIdentificar').dialog({
            width:655,
            height:360,
            title:'Identificar',
            iconCls:'icon-identificar',
            closed:true,
            modal:true,
            draggable:false, 
            collapsible:false, 
            resizable:false,
            buttons:
            [
                {
                    text:'Guardar en la base de datos',
                    iconCls:'icon-save',
                    handler:function()
                    {
                        identificar();
                    }
                },
                {
                    text:'Cancelar',
                    iconCls:'icon-cancel',
                    handler:function()
                    {
                        $('#dlgRecolectadasParaIdentificar').dialog('close');
                    }
                }
            ],
            onOpen:function()
            {
                $('#formRecolectadasParaIdentificar').form('clear').form('disableValidation').form('validate'); 
                
                var numElementosDatagridEspecies = $("#dgEspecies").datagrid('getData').total;
                //console.log(numElementosDatagridEspecies);

                if(numElementosDatagridEspecies > 0)
                {
                    for(var c=0; c<numElementosDatagridEspecies; c++)
                    {
                        $('#dgEspecies').datagrid('deleteRow',0);
                    }
                    $('#especie').combobox({onBeforeLoad: function(param){param.cargadosDatagrid = '';}});
                    $('#especieResult').textbox('textbox').css({backgroundColor: '#505050', color: '#FFFFFF'});
                }

                $('#btnEspecieNoIdentificada').linkbutton('unselect');
                $('#especieValues').textbox('reset');
                $('#especie').combobox('enable');
                $('#especie').combobox('reset');
                $('#especie').combobox({onBeforeLoad: function(param){param.cargadosDatagrid = '';}});
                //Hidden
                $('#especieValues').textbox('hide');

            }

        });

        //INPUTS IDENTIFICAR
        $('#cuantosVideos').textbox({
            width:50,
            required:true,
            readonly:true
        });

        $('#idsRecolectados').textbox({
            width:375,
            required:true,
            readonly:true
        });

        $('#cuantasUbicaciones').textbox({
            width:50,
            required:true,
            readonly:true
        });

        $('#ubicacionesRecolectadas').textbox({
            width:375,
            required:true,
            readonly:true
        });

        $('#resultado').textbox({
            width:70,
            required:true,
            readonly:true,
            onChange:function()
            {
                $('#idsRecolectados').textbox('hide');
                $('#ubicacionesRecolectadas').textbox('hide');
                $('#complementoResultado').textbox('hide');
            }

        });

        $('#complementoResultado').textbox({
            width:50,
            required:true,
            readonly:true
        });

        $('#dgEspecies').datagrid({
            title:'Buscar especie(s)',
            rownumbers:true,
            width:610,
            height:200,
            fitColumns:true,
            autoRowHeight:true,
            striped:true,
            singleSelect:true,
            columns:
            [[
                {field:'scientific_name', title:'Nombre cientifico', width:200},
                {field:'type', title:'Tipo', width:100},
                {field:'individuos', title:'Individuos'},
                {field:'machos', title:'Machos'},
                {field:'hembras', title:'Hembras'},
                {field:'juvenil', title:'Juvenil'}

            ]],
            toolbar: '#toolbarEspecies',
            onSelect: function(index,row)
            {
                $('#btnQuitarEspecie').linkbutton('enable');
            }
        });

        $('#especie').combobox({
            width:320,
            required:false,
            hasDownArrow:false,
            valueField: 'id',
            textField:'nombre_cientifico',
            url:'getEspecies.php',
            queryParams:
            {
                cargadosDatagrid: ''
            },
            onLoadSuccess: function()
            {
                $(this).combobox('textbox').bind('keydown', function(e)
                {
                    if (e.keyCode)
                        $('#btnAgregarEspecie').linkbutton('disable');
                });
            },
            onSelect: function(record)
            {
                $('#btnAgregarEspecie').linkbutton('enable');
            }
        });

        $('#btnAgregarEspecie').linkbutton({
            text:'Agregar',
            height:20,
            iconCls:'icon-add',
            disabled:true,
            onClick: function()
            {
                //VARIABLES
                var especieID   = $('#especie').combobox('getValue');
                //ABRIR VENTANA DE INDIVIDUOS, MACHOS Y HEMBRAS
                $('#dlgDetalleEspecie').dialog('open');
                $('#especieSeleccionada').textbox('setValue', especieID);

                var especieID = especieID.split(",");
                var id = especieID[0];
                if( id == 9999 )
                {
                    $('#clasificacion').combobox('enable');
                    $('#machos').numberspinner('disable');
                    $('#hembras').numberspinner('disable');
                    $('#juvenil').numberspinner('disable');
                }
                else
                {
                    $('#clasificacion').combobox('disable');
                    $('#machos').numberspinner('enable');
                    $('#hembras').numberspinner('enable');
                    $('#juvenil').numberspinner('enable');
                }




                /*
                ***
                ***MOVE TO NEW FUNCTION
                ***
                //VARIABLES
                var especieID   = $('#especie').combobox('getValue');
                var especieText = $('#especie').combobox('getText');
                //SEPARAMOS EL VALOR DE LA CONSULTA PARA PODER AGREGARLO A CADA CAMPO DEL DATAGRID
                especieID = especieID.split(",");
                var id = especieID[0];
                var nombre_cientifico = especieID[1];
                var tipo = especieID[2];
                $('#dgEspecies').datagrid('appendRow',{id: id, scientific_name: nombre_cientifico, type: tipo});
                $(this).linkbutton('disable');
                //FUNCIÓN PARA RECOLECTAR TODOS LOS ID DEL DATAGRID 
                var coleccionID = getDatagridValues('dgEspecies');
                $('#especieValues').textbox('setValue',coleccionID);
                $('#especie').combobox('reset');   
                $('#especie').combobox({onBeforeLoad: function(param){param.cargadosDatagrid = coleccionID;}}); */
            }
        });

        $('#btnQuitarEspecie').linkbutton({
            text:'Quitar',
            height:20,
            iconCls:'icon-delete',
            disabled:true,
            onClick: function()
            {
                //VARIABLES
                var rowSelected = $('#dgEspecies').datagrid('getSelected');
                var rowIndex = $('#dgEspecies').datagrid('getRowIndex', rowSelected);
                //METODO PARA BORRAR EL ELEMENTO DEL DATAGRID Y DESHABILITAR NUEVAMENTE EL BOTON DE QUITAR
                $('#dgEspecies').datagrid('deleteRow',rowIndex);                
                $(this).linkbutton('disable');
                //FUNCIÓN PARA RECOLECTAR TODOS LOS ID DEL DATAGRID    
                var coleccionID = getDatagridValues('dgEspecies');
                $('#especieValues').textbox('setValue',coleccionID);
                $('#especie').combobox({onBeforeLoad: function(param){param.cargadosDatagrid = coleccionID;}});
            }
        });

        $('#especieResult').textbox({           
            width:70,
            prompt:'Pendiente',
            readonly:true
        }).textbox('textbox').css({backgroundColor: '#505050', color: '#FFFFFF'});

        $('#especieValues').textbox({
            width:610,
            required:true,
            readonly:true,
            onChange:function(value)
            {
                if(  $(this).textbox('getValue') !='' )
                    $('#especieResult').textbox('setValue','Válido').textbox('textbox').css({backgroundColor: '#447F48', color: '#FFFFFF'});
                else
                    $('#especieResult').textbox('setValue','Pendiente').textbox('textbox').css({backgroundColor: '#505050', color: '#FFFFFF'});
            }
        });

        //[DIALOG] DETALLE DE ESPECIE
        $('#dlgDetalleEspecie').dialog({
            title:'Especificar detalles de la especie',
            width:300,
            height:340,
            modal:true,
            closed:true,
            draggable:false, 
            collapsible:false, 
            resizable:false,
            queryParams:
            {
                especie: ''
            },
            buttons:
            [
                {
                    text:'Aceptar',
                    iconCls:'icon-aceptar',
                    handler:function()
                    {
                        agregarDetalleEspecie();

                    }
                },
                {
                    text:'Cancelar',
                    iconCls:'icon-cancel',
                    handler:function()
                    {
                        $('#dlgDetalleEspecie').dialog('close');
                    }
                }
            ],
            onOpen:function()
            {
                /*$('#individuos').numberspinner('clear');
                $('#machos').numberspinner('clear');
                $('#hembras').numberspinner('clear');
                $('#juvenil').numberspinner('clear');*/
                $('#formDetalleEspecie').form('clear').form('disableValidation').form('validate');

                $('#lblEspecieSeleccionada').hide();
                $('#especieSeleccionada').textbox('hide');

            }
        });

        $('#formDetalleEspecie').form({
            novalidate:true
        });

        $('#especieSeleccionada').textbox({
            width:250,
            required:true,
            editable:false
        });

        $('#individuos').numberspinner({
            width:250,
            required:true,
            editable:false,
            min:1,
            max:50
        });

        $('#clasificacion').combobox({
            width:250,
            panelHeight: 'auto',
            panelMaxHeight: 200,
            required:true,
            editable:false,
            valueField: 'value',
            textField: 'value',
            data: 
            [
                {value:'Ave'},
                {value:'Indeterminado'},
                {value:'Mamifero'}
            ]
        });

        $('#machos').numberspinner({
            width:250,
            required:true,
            editable:false,
            min:0,
            max:50
        });

        $('#hembras').numberspinner({
            width:250,
            required:true,
            editable:false,
            min:0,
            max:50
        });

        $('#juvenil').numberspinner({
            width:250,
            required:true,
            editable:false,
            min:0,
            max:50
        });

        $('#resultDetalleEspecie').textbox({
            width:250,
            required:true,
            editable:false
        });





    });
  </script>
  <?php
        }
    }
    else
    {
        echo "
        <html>
            <head><style>@font-face {font-family: 'Roboto';src: url('fonts/Roboto-Medium.ttf') format('truetype');}</style></head>
            <body><div style='width:100%; text-align:center; margin-top:250px;'><p style='color:crimson; font-family:Roboto;'>¡ERROR!</p><p style='color:dimgray; font-family:Roboto;'>Necesita iniciar sesión para utilizar este sistema</p></div></body>
        </html>";
    }
  ?>
</body>
</html>
