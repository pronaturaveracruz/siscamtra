<?php
	error_reporting(0);
    date_default_timezone_set("Mexico/General");
    session_start();
    $usuario_actual= $_SESSION['usuario'];
    include ("../includes/conexion.php");
    $linkMySQL = ConectarseMySQLMegaBD();
    
    $page  = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows  = isset($_POST['rows']) ? intval($_POST['rows']) : 19;
	$sort  = isset($_POST['sort']) ? strval($_POST['sort']) : 'fecha';
	$order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
	$offset = ($page-1)*$rows;

	#REQUEST VALUES
	$filterArray = isset($_REQUEST['filterRules']) ? json_decode($_REQUEST['filterRules'],true) : '';
	$num_filter  = count($filterArray);
	//print_r($filterArray);
	//print_r($num_filter);

	if($num_filter === 0)
	{
		$where = "";
	}
	else if($filterArray != '' && $num_filter > 0)
	{
		//echo "filtro activo\n";
		$where = "";
		for($i=0; $i<$num_filter; $i++)
		{
			$filterField = $filterArray[$i]['field'];
			$filterOperator = $filterArray[$i]['op'];
			$filterValue = $filterArray[$i]['value'];

			$mValues = explode(",",$filterValue);
			$countmValues = count($mValues);		
			for($j=0; $j<$countmValues; $j++)
			{
				$filterValue = $mValues[$j];
				if ($countmValues > 1)
				{
					if($j === 0)
						$where .= " AND( lower($filterField) LIKE lower('%$filterValue%')";
					else if($j !==0 && $j!==($countmValues-1))
						$where .= " OR lower($filterField) LIKE lower('%$filterValue%')";
					else if($j===($countmValues-1))
						$where .= " OR lower($filterField) LIKE lower('%$filterValue%')  )";
				}
				else
				{
					switch($filterOperator)
					{
						case 'contains':
							$where .= " AND ( lower($filterField) LIKE lower('%$filterValue%')  )";
							break;
						case 'equal';
							$where .= " AND ".$filterField." = '".$filterValue."'";
							break;
						case 'notequal';
							$where .= " AND ".$filterField . " <> '" . $filterValue ."'";
							break;
						case 'beginwith';
							$where .= " AND ".$filterField . " LIKE '" . $filterValue ."%'";
							break;
						case 'endwith';
							$where .= " AND ".$filterField . " LIKE '%" . $filterValue ."'";
							break;
						case 'less';
							$where .= " AND ".$filterField . " < '". $filterValue."'";
							break;
						case 'lessorequal';
							$where .= " AND ".$filterField . " <= '". $filterValue."'";
							break;
						case 'greater';
							$where .= " AND ".$filterField . " > '". $filterValue."'";
							break;
						case 'greaterorequal';
							$where .= " AND ".$filterField . " >= '". $filterValue."'";
							break;
					}
				}
			}		
		}
	}

	$select = "SELECT * ";
	$selectCount = "SELECT count(*) ";

	$consultaBase = "FROM
	(
		SELECT id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, destacada, ruta
		FROM concentrado_fotos
		UNION ALL
		SELECT id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, destacado AS destacada, ruta
		FROM concentrado_videos
	) q1
	WHERE (destacada = '' OR destacada IS NULL)".$where." ORDER BY fecha DESC";


    $rs = mysqli_query($linkMySQL,$selectCount.$consultaBase);
    $row = mysqli_fetch_row($rs);
    $result["total"] = $row[0];

    $rs = mysqli_query($linkMySQL, $select.$consultaBase);

    $items = array();
    while($row = mysqli_fetch_object($rs))
    {			
		array_push($items, $row);
    }

    $result["rows"] = $items;

    mysqli_free_result($rs);
    mysqli_close($linkMySQL);
	echo json_encode($result);