<?php
	error_reporting(0);
	date_default_timezone_set("Mexico/General");
	session_start();
	$usuario_actual= $_SESSION['usuario'];
	$fecha_actual = date('Y-m-d');
	include ("../includes/conexion.php");
	$linkMySQL = ConectarseMySQLMegaBD();
	$aux=0;
	$contador=0;
	
	//RECUPERAMOS VARIABLES
	$recolectadosParaDestacar = $_POST['recolectadosParaDestacar'];
	$opcion = $_POST['opcion'];

	if($recolectadosParaDestacar && $opcion)
	{
		//AJUSTAMOS LOS ARRAY
		$arreglo_recolectados_a_destacar = explode(",", $recolectadosParaDestacar);
		$cuantas = count($arreglo_recolectados_a_destacar);

		for($i=0;$i<$cuantas;$i++)
		{
			if($opcion=='SI')
				$sql_actualizar_concentrado_fotos= mysqli_query($linkMySQL, "UPDATE concentrado_fotos SET destacada = 'Si', quien_destaco= '$usuario_actual' WHERE concentrado_fotos.id_foto = $arreglo_recolectados_a_destacar[$i]");
			else if($opcion=='NO')
				$sql_actualizar_concentrado_fotos= mysqli_query($linkMySQL, "UPDATE concentrado_fotos SET destacada = 'No', quien_destaco= '$usuario_actual' WHERE concentrado_fotos.id_foto = $arreglo_recolectados_a_destacar[$i]");

			$mar_actualiza= mysqli_affected_rows($linkMySQL);
			if($mar_actualiza>0)
				$aux++;
		}



		if($aux>0)
		{
			if($opcion == 'SI')
			{
				$status = "Destacada";
				if($aux == 1)					
					$okMsg = "Se definio ".$aux." fotografia como 'Destacada'";
				else
					$okMsg = "Se definieron ".$aux." fotografias como 'Destacada'";
			}
			else if($opcion == 'NO')
			{
				$status = "No destacada";
				if($aux == 1)					
					$okMsg = "Se definio ".$aux." fotografia como 'No destacada'";
				else
					$okMsg = "Se definieron ".$aux." fotografias como 'No destacada'";
			}

			echo json_encode(array('okMsg' => $okMsg ));
		}
		else if($aux == 0)
		{
			$errorMsg = "No se actualizo nada";
			echo json_encode(array('errorMsg'=> $errorMsg ));
		}
	}
	else
	{
		$errorMsg = "Error en las variables";
		echo json_encode(array('errorMsg'=> $errorMsg));
	}