<?php
	error_reporting(0);
    date_default_timezone_set("Mexico/General");
    session_start();
    $usuario_actual= $_SESSION['usuario'];
    include ("../includes/conexion.php");
    $linkMySQL = ConectarseMySQLMegaBD();
    
    $page  = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows  = isset($_POST['rows']) ? intval($_POST['rows']) : 19;
	$sort  = isset($_POST['sort']) ? strval($_POST['sort']) : 'ruta';
	$order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
	$offset = ($page-1)*$rows;

	#REQUEST VALUES
	$filterArray = isset($_REQUEST['filterRules']) ? json_decode($_REQUEST['filterRules'],true) : '';
	$num_filter  = count($filterArray);
	//print_r($filterArray);
	//print_r($num_filter);

	if($num_filter === 0)
	{
		$where = "";
	}
	else if($filterArray != '' && $num_filter > 0)
	{
		//echo "filtro activo\n";
		$where = "";
		for($i=0; $i<$num_filter; $i++)
		{
			$filterField = $filterArray[$i]['field'];
			$filterOperator = $filterArray[$i]['op'];
			$filterValue = $filterArray[$i]['value'];

			#EXCEPCION PARA LAS ABREVIATURAS DE ZONA
			if($filterField == 'zona')
			{
				if($filterValue == 'Tlacotalpan')
					$filterValue = 'TLACO';
				else if($filterValue == 'Costa de la palma')
					$filterValue = 'CPALMA';
			}
			########################################

			$mValues = explode(",",$filterValue);
			$countmValues = count($mValues);		
			for($j=0; $j<$countmValues; $j++)
			{
				$filterValue = $mValues[$j];
				if ($countmValues > 1)
				{
					if($j === 0)
						$where .= " AND( lower(q2.$filterField) LIKE lower('%$filterValue%')";
					else if($j !==0 && $j!==($countmValues-1))
						$where .= " OR lower(q2.$filterField) LIKE lower('%$filterValue%')";
					else if($j===($countmValues-1))
						$where .= " OR lower(q2.$filterField) LIKE lower('%$filterValue%')  )";
				}
				else
				{
					switch($filterOperator)
					{
						case 'contains':
							$where .= " AND ( lower(q2.$filterField) LIKE lower('%$filterValue%')  )";
							break;
						case 'equal';
							$where .= " AND q2.".$filterField." = '".$filterValue."'";
							break;
						case 'notequal';
							$where .= " AND q2.".$filterField . " <> '" . $filterValue ."'";
							break;
						case 'beginwith';
							$where .= " AND q2.".$filterField . " LIKE '" . $filterValue ."%'";
							break;
						case 'endwith';
							$where .= " AND q2.".$filterField . " LIKE '%" . $filterValue ."'";
							break;
						case 'less';
							$where .= " AND q2.".$filterField . " < '". $filterValue."'";
							break;
						case 'lessorequal';
							$where .= " AND q2.".$filterField . " <= '". $filterValue."'";
							break;
						case 'greater';
							$where .= " AND q2.".$filterField . " > '". $filterValue."'";
							break;
						case 'greaterorequal';
							$where .= " AND q2.".$filterField . " >= '". $filterValue."'";
							break;
						case 'range':
							#TODO: Separar el filterValue en 2 variables
							$arrayFilterValue = explode(":", $filterValue);
							$f1 = $arrayFilterValue[0];
							$f2 = $arrayFilterValue[1];
							$where .= " AND (q2.fecha BETWEEN '$f1' AND '$f2')";
							break;
					}
				}
			}		
		}
	}

	$select = "SELECT id, archivo, id_ct, fecha, hora, zona, predio, ubicacion, id_clase, id_especie, nombre_cientifico, total_individuos, destacada, ruta, status, nom059, cites, redlist2013 "; 
	$selectCount = "SELECT count(*) "; // Numero de elementos: 9,426 [12,185 con id_especie = 0 y ubicaciones = 0]
	$consultaBase = "FROM
	(
		SELECT id, archivo, id_ct, fecha, hora, q1.zona, predio, ubicacion, id_clase, id_especie, nombre_cientifico, total_individuos, destacada, ruta, status, nom059, cites, redlist2013
		FROM
		(
			SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, total_individuos, destacada, ruta, status, NOM059_2010 AS nom059, CITES AS cites, RedList2013 AS redlist2013
			FROM concentrado_fotos, registro_fototrampas, aves_ecoforestal
			WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6')
				UNION ALL
			SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, total_individuos, destacada, ruta, status, estadoconserv_mexico AS nom059, cites AS cites, uicn AS redlist2013
			FROM concentrado_fotos, registro_fototrampas, mamiferos
			WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9')

			UNION ALL

			SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, total_individuos, destacado AS destacada, ruta, status, NOM059_2010 AS nom059, CITES AS cites, RedList2013 AS redlist2013
			FROM concentrado_videos, registro_videotrampas, aves_ecoforestal
			WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6')
				UNION ALL
			SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, total_individuos, destacado AS destacada, ruta, status, estadoconserv_mexico AS nom059, cites AS cites, uicn AS redlist2013
			FROM concentrado_videos, registro_videotrampas, mamiferos
			WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9')
		) AS q1, registro_ubicaciones, clasificacion
		WHERE (q1.ubicacion = registro_ubicaciones.idubicacion_ct AND q1.id_clase = clasificacion.idclasificacion)
	) AS q2 WHERE id> 0".$where." ORDER BY $sort $order LIMIT $offset,$rows";

	//print_r($select.$consultaBase);


	$result = array();
	$rs = mysqli_query($linkMySQL,$selectCount.$consultaBase);
    $row = mysqli_fetch_row($rs);
    $result["total"] = $row[0];

    $rs = mysqli_query($linkMySQL, $select.$consultaBase);

    $items = array();
    while($row = mysqli_fetch_object($rs))
    {			
		array_push($items, $row);
	}

	$result["rows"] = $items;

	mysqli_free_result($rs);
	mysqli_close($linkMySQL);
	echo json_encode($result);