<?php
	include ("../includes/conexion.php");
    $link = ConectarsePostgreSQL();
    $linkMySQL = ConectarseMySQLMegaBD();

    #RECUPERAMOS VARIABLES
    $idPredio = $_POST['idPredio']; //2
    $idEstado = $_POST['idEstado']; //30
    $municipio = $_POST['municipio']; //Tlacotalpan

    //6 30 Alvarado


    /*echo $idPredio."\n";
    echo $idEstado."\n";
    echo $municipio."\n";*/

    #OBTENEMOS EL NOMBRE DEL PREDIO EN BASE A SU ID DESDE MYSQL
    $sqlPredio = mysqli_query($linkMySQL, "SELECT nombre_predio FROM predio WHERE idpredio = '$idPredio'");
    while($row = mysqli_fetch_row($sqlPredio))
    {
        $nombrePredio = $row[0];
    }

    #1) OBTENEMOS LA GEOMETRIA DEL PREDIO EN CCL
    /*$sqlGeomPredio = pg_query($link,"SELECT ST_Multi(ST_Union(geom)) AS geom FROM
    (
        SELECT ST_AsText(geom) AS geom FROM shape_poligonos_ccl_merge_final WHERE nombre_apc ILIKE '%$nombrePredio%'
    ) as superUnion");
    while($row = pg_fetch_assoc($sqlGeomPredio))
    {
    	$geomPredio = $row['geom'];
    }*/

    #2) OBTENEMOS LA GEOMETRIA DEL MUNICIPIO EN CCL
    /*$sqlGeomMunicipio = pg_query($link,"SELECT ST_AsText(geom) FROM shape_municipios_ccl WHERE cve_ent='$idEstado' AND nom_mun = '$municipio'");
    while($row = pg_fetch_assoc($sqlGeomMunicipio))
    {
        $geomMunicipio = $row['geom'];
    }*/

    #1) DETERMINAMOS SI EL PREDIO ESTA DENTRO DEL POLIGONO DEL MUNICIPIO (ST_INTERSECTS)
    $sqlIntersects = "WITH buffer AS 
    (
        SELECT geom FROM shape_municipios_ccl WHERE cve_ent='$idEstado' AND nom_mun = '$municipio'
    )
    SELECT COUNT(*)
    FROM shape_poligonos_ccl_merge_final,buffer
    WHERE ST_Intersects (shape_poligonos_ccl_merge_final.geom,buffer.geom) AND nombre_apc ILIKE '%$nombrePredio%'"; 
    $sqlIntersectsQuery = pg_query($link, $sqlIntersects);
    while($row = pg_fetch_row($sqlIntersectsQuery))
    {
        $countResult = $row[0];
    }

    if($countResult > 0)
    {
        #OBTENEMOS LA GEOMETRIA MERGEADA DEL PREDIO EN CCL
        $sqlGeomPredio = pg_query($link,"SELECT ST_AsText(ST_Multi(ST_Union(geom))) AS geom FROM
        (
            SELECT ST_AsText(geom) AS geom FROM shape_poligonos_ccl_merge_final WHERE nombre_apc ILIKE '%$nombrePredio%'
        ) as superUnion");
        while($row = pg_fetch_assoc($sqlGeomPredio))
        {
            $myGeom = $row['geom'];
        }
        echo "OK|".$myGeom."|".$nombrePredio;
    }
    else
    {
        echo "EL PREDIO NO ESTA DENTRO DEL MUNICIPIO SELECCIONADO";
    }
?>