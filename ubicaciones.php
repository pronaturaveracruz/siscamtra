<?php
    error_reporting(0);
    date_default_timezone_set("Mexico/General");
    session_start();
    $usuario_actual= $_SESSION['usuario'];
    include ("../includes/conexion.php");
    $link = ConectarsePostgreSQL();
    $linkMySQL = ConectarseMySQLMegaBD();

    #VERIFICAMOS SI EXISTE ALGUN USUARIO LOGEADO
    if($usuario_actual)
    {
        #VERIFICAMOS SI EL USUARIO DE LA SESIÓN ESTA AUTORIZADO PARA ENTRAR A ESTA PLATAFORMA
        $sqlUsuarioValido = mysqli_query($linkMySQL,"SELECT COUNT(*)
        FROM sis_control_acceso
        LEFT JOIN plataforma ON sis_control_acceso.id_plataforma=plataforma.id_plataforma
        LEFT JOIN usuario ON sis_control_acceso.id_usuario=usuario.id_usuario
        WHERE sis_control_acceso.id_plataforma='5' AND usuario.nombre_usuario='$usuario_actual'");
        while($row = mysqli_fetch_row($sqlUsuarioValido))
        {
            $cuantos = $row[0];
        }
        if($cuantos == 0) #USUARIO NO AUTORIZADO
        {
            session_destroy();
            echo "<script>parent.window.location.reload();</script>";
            exit;
        }
        else if($cuantos > 0)
        {
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/myIcons.css">
    <link rel="stylesheet" type="text/css" href="css/myForm.css">
	<link rel="stylesheet" type="text/css" href="includes/jquery-easyui-1.5.2/themes/material/easyui.css">
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/jquery.min.js"></script>
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/jquery.easyui.min.js"></script>
  	<script type="text/javascript" src="includes/jquery-easyui-1.5.2/locale/easyui-lang-es.js"></script>
    <script type="text/javascript" src="includes/datagrid-filter/datagrid-filter.js"></script>
    <script type="text/javascript" src="includes/datagrid-view/datagrid-scrollview.js"></script>
    <!--OpenLayers-->
    <link rel="stylesheet" href="includes/Openlayers_3.19.1/ol.css" type="text/css">
    <script src="includes/Openlayers_3.19.1/ol-debug.js"></script>
    <!--Proj4-->
    <script src="includes/proj4js-2.3.15/dist/proj4.js"></script>
    <style>
        body
        {
            font-family:Roboto;
            font-size:12px;
            padding: 20px 20px 0 20px;
            margin:0;
        }
        /*SI SE DEFINIERON POR HTML*/
        input.easyui-textbox{text-align:center;}
        input.easyui-combobox{text-align:center;}
        input.easyui-numberbox{text-align:center;}
        input.easyui-datebox{text-align:center;}
        select.easyui-combobox{text-align:center;}
        /*SI SE DEFINIERON POR JS (APLICAMOS PARA TODOS: TEXT, COMBO, NUMBER,ETC)*/
        .textbox-text{text-align:center;color:crimson;font-weight:bold;}
        .textbox-readonly .textbox-text{background:#EEEEEE;text-align:center;color:dimgray;font-weight:bold;}
    </style>
  	<script>
        /*OVERRIDES*/
        //FUNCION PARA LIMITAR UNICAMENTE LA SELECCION A LOS ELEMENTOS DE LA LISTA (COMBOBOX)
		$.extend($.fn.validatebox.defaults.rules,{
			inList:{
				validator:function(value,param){
					var c = $(param[0]);
					var opts = c.combobox('options');
					var data = c.combobox('getData');
					var exists = false;
					for(var i=0; i<data.length; i++){
						if (value == data[i][opts.textField]){
							exists = true;
							break;
						}
					}
					return exists;
				},
				message:'Debe seleccionar un elemento de la lista.'
			}
		});
        //ONLY NUMBERS IN TEXTBOX
        $.extend($.fn.validatebox.defaults.rules,{
            onlyNumbers:
            {
                validator: function(value,param)
                {
                    if(  /^\d+$/.test(value)  )
                        return value;
                },
                message: 'Solo se aceptan numeros'
            }
        });

        //FUNCION PARA FORMATEAR LOS CAMPOS DATEBOX (GUIONES, DIAGONALES, ETC.)
        $.fn.datebox.defaults.formatter = function(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            return y + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
        };
        $.fn.datebox.defaults.parser = function(s)
        {
            if (s)
            {
                var a = s.split('-');
                var d = new Number(a[2]);
                var m = new Number(a[1]);
                var y = new Number(a[0]);
                var dd = new Date(y, m-1, d);
                return dd;
            }
            else
            {
                return new Date();
            }
        };

        //FUNCION PARA MOSTRAR U OCULTAR LOS TEXBOX
        $.extend($.fn.textbox.methods, {
            show: function(jq){
                return jq.each(function(){
                    $(this).next().show();
                })
            },
            hide: function(jq){
                return jq.each(function(){
                    $(this).next().hide();
                })
            }
        });

        //FUNCION PARA PODER OBLIGAR A INTRODUCIR X NUMERO DE CARACTERES (USADO PARA COORDENADAS)
        $.extend($.fn.validatebox.defaults.rules, {
    		minLength: {
        		validator: function(value, param) {
            		return value.length >= param[0];
        		},
        		message: 'Ingrese al menos {0} caracteres'
    		},
    		decimalValue: {
        		validator: function(value, param) {
            		if (!/^([0-9])*[.]?[0-9]*$/.test(value)) {
                		//Invalido
            		} else {
                		//Valido
                		return value;
            		}
        		},
        		message: 'Por favor ingrese un numero decimal valido'
    		},
    		decimalLengthX: {
        		validator: function(value, param) {
            		var coordenada = value.split(".");
            		if (coordenada.length > 1) {
                		var enteros = coordenada[0];
                		var decimales = coordenada[1];
                		if (enteros.length == 6 && decimales.length == 2) {
                    		return value;
                		}
            		} else {
                		//console.warn('Es un entero');
            		}
        		},
        		message: 'El formato correcto de la coordenada "X" son 6 enteros y 2 decimales'
    		},
    		decimalLengthY: {
        		validator: function(value, param) {
            		var coordenada = value.split(".");
            		if (coordenada.length > 1) {
                		var enteros = coordenada[0];
                		var decimales = coordenada[1];
                		if (enteros.length == 7 && decimales.length == 2) {
                    		return value;
                		}
            		} else {
                		//console.warn('Es un entero');
            		}
        		},
        		message: 'El formato correcto de la coordenada "Y" son 7 enteros y 2 decimales'
    		}
		});
  	</script>
    <script>
    	/*FUNCIONES*/
    	function resetFromEstado(estado)
    	{
    		source.clear();
    		sourcePredio.clear();
    		sourcePunto.clear();
    		$('#municipio').combobox('clear');  
    		$('#predio').combobox('clear');  		
    		var url = 'getMunicipios.php?estado='+ estado;
    		$('#municipio').combobox('reload',url);	    		
    	}

    	function resetFromMunicipio(municipio)
    	{
    		//RESTABLECER A SUS VALORES INICIALES
    		$('#x').numberbox('clear').numberbox('disable');
      		$('#y').numberbox('clear').numberbox('disable');      			
            $('#coordsValidator').textbox('clear');
            $('#coordsIntersectsStatusMessage').textbox('setValue','Pendiente').textbox('textbox').css({backgroundColor: '#505050', color: '#FFFFFF'});

			if(municipio == 'Alvarado' || municipio == 'Tlacotalpan')
            {
            	$('#predio').combobox('readonly', false);
               	$('#predio').combobox('clear');
               	$('#predio').combobox('reload', 'getPredios.php?municipio=' + municipio);
            }
            else
            {              		
               	$('#predio').combobox('clear');               		 
               	$('#predio').combobox('setValue', '8');
               	$('#predio').combobox('setText', 'Otro');
               	$('#predio').combobox('readonly', true);              		              		
            }
    	}

    	function drawMunicipio(idEstado, municipio)
    	{
    		$.post('getGeomMunicipio.php', {idEstado: idEstado, municipio:municipio}, function(result){
               //SEPARAMOS EL RESULTADO PARA OBTENER LA GEOMETRIA Y ZONA
               var arregloPHP = result.split("|");
               var geom = arregloPHP[0];
               var zonaUTM = arregloPHP[1];
               $('#zonaUTM').textbox('setValue', zonaUTM);           

               //MOSTRAMOS EL POLIGONO DEL MUNICIPIO
               source.clear();
               sourcePredio.clear();
               sourcePunto.clear();
               var format = new ol.format.WKT();
               var feature = format.readFeature(geom);
               feature.getGeometry().transform('EPSG:48402', 'EPSG:3857');
               //DEFINIR UN TITULO PARA EL FEATURE (NOMBRE DE MUNICIPIO)
               feature.set('description', municipio);
               feature.setId('myMunicipio');
               feature.setStyle(styleFunction);
               function styleFunction()
               {
                    return [
                        new ol.style.Style({
                            fill: new ol.style.Fill({
                                color: 'rgba(255,255,255,0.4)'
                            }),
                            stroke: new ol.style.Stroke({
                                color: '#3399CC',
                                width: 1.25
                            }),
                            text: new ol.style.Text({
                                font: '22px Roboto',
                                fill: new ol.style.Fill({ color: '#000' }),
                                stroke: new ol.style.Stroke({
                                    color: '#fff', width: 2
                                }),
                                // get the text from the feature - `this` is ol.Feature and show only under certain resolution
                                text: map.getView().getZoom() > 5 ? this.get('description') : ''
                            })
                        })
                    ];
                }
               vector.getSource().addFeature(feature);
            });		
    	}

        function drawPredio(idPredio, idEstado, municipio)
        {
			//VALIDAMOS SI ESTA DENTRO DEL MUNICIPIO
        	$.post('getGeomPredio.php', {idPredio: idPredio, idEstado: idEstado, municipio:municipio}, function(result){
	        	var arregloPHP = result.split("|");
               	var status = arregloPHP[0];
               	var geom = arregloPHP[1];
               	var nombrePredio = arregloPHP[2];

               	if(status === 'EL PREDIO NO ESTA DENTRO DEL MUNICIPIO SELECCIONADO')
               	{
	            	$('#predio').combobox('clear');
               		$.messager.show({
	                   	title: 'Error',
                       	msg: 'El predio no esta dentro del municipio seleccionado'
                   	}); 
               	}
               	else if(status === 'OK')
               	{
	            	//DIBUJAMOS EL POLIGONO DEL PREDIO EN EL MAPA
	        		sourcePredio.clear();
	        		sourcePunto.clear();
               		var format = new ol.format.WKT();
               		var feature = format.readFeature(geom);
               		feature.getGeometry().transform('EPSG:48402', 'EPSG:3857');
               		vectorPredio.getSource().addFeature(feature);
               		//DEFINIR UN TITULO PARA EL FEATURE (NOMBRE DEL PREDIO)
               		feature.set('description', nombrePredio);
               		feature.setId('myPredio');
               		feature.setStyle(styleFunctionPredio);
               		function styleFunctionPredio()
               		{
		        	    return [
                      		new ol.style.Style({
		                    	fill: new ol.style.Fill({
                                	color: 'rgba(255,255,255,0.2)'
                            	}),
                            	stroke: new ol.style.Stroke({
		                        	color: '#ffcc33',
                                	width: 1
                            	}),
                            	text: new ol.style.Text({
		                        	font: '16px Roboto',
                                	fill: new ol.style.Fill({ color: '#ffcc33' }),
                                	stroke: new ol.style.Stroke({
		                            	color: '#000', width: 2
                                	}),
                                	text: map.getView().getZoom() > 5 ? this.get('description') : ''
                            	})
                        	})
                    	];
                	}
               		var extent = feature.getGeometry().getExtent();
               		var center = ol.extent.getCenter(extent);
               		view.un('change:center', constrainPan);
               		map.getView().setCenter(center);
               		//AJUSTAR AL EXTENT EL POLIGONO
               		map.getView().fit(extent, map.getSize());
               		view.on('change:center', constrainPan);
               	}        		
        	});        	      	        	          
        }

        function validateCoords(zonaUTM,x,y,featureIntersects)
        {
        	//CONVERTIMOS LAS COORDENADAS UTM CAPTURADAS A CCL Y VALIDAMOS SI ESTAN DENTRO DEL MUNICIPIO/PREDIO CON LA TECNICA DE LA GALLETA
        	console.warn(zonaUTM);
        	console.warn(x);
        	console.warn(y);
        	console.warn(featureIntersects);
        	$('#coordsIntersectsStatusMessage').textbox('setValue','Intersectando...').textbox('textbox').css({backgroundColor: '#CFB41C', color: '#FFFFFF'});
        	console.warn("Intersectando...");
        	
        	if(zonaUTM=='14')
	        	var lonlat= ol.proj.transform([x, y], 'EPSG:32614', 'EPSG:3857');
          	else if(zonaUTM=='15')
	        	var lonlat= ol.proj.transform([x, y], 'EPSG:32615', 'EPSG:3857');

	        //1) DIBUJAMOS EL PUNTO EN EL MAPA
	        var point_coords = new ol.Feature({
            	geometry: new ol.geom.Point(lonlat)
          	});
          	vectorPunto.getSource().addFeature(point_coords);
          	//DEFINIR UN TITULO PARA EL FEATURE (NOMBRE DEL PREDIO)
            point_coords.set('description', "Mi coordenada");
            point_coords.setId('point_coords');
            point_coords.setStyle(styleFunctionMiCoordenada);
            function styleFunctionMiCoordenada()
            {
		    	return [
            		new ol.style.Style({

            			image: new ol.style.Icon({
            				anchor: [0.5, 1],
                 			anchorXUnits: 'fraction',
                 			anchorYUnits: 'fraction',
                 			opacity: 1,
                      src: "images/location_pin_red.png"
            			}),
                        text: new ol.style.Text({
		                	font: '11px Roboto',
                            fill: new ol.style.Fill({ color: '#B72C2C' }),
                            stroke: new ol.style.Stroke({
		                    	color: '#FFFFFF', width: 2
                        	}),
                        	offsetX: 0,
                        	offsetY: 10,
                        	text: map.getView().getZoom() > 5 ? this.get('description') : ''
                    	})
                    })
                ];
            }
          	var extent = point_coords.getGeometry().getExtent();
            var center = ol.extent.getCenter(extent);
            view.un('change:center', constrainPan);
            map.getView().setCenter(center);
            map.getView().fit(extent, map.getSize());
            view.on('change:center', constrainPan);

	        // 2) OBTENEMOS LA GEOMETRIA DESDE LOS VECTORES Y LA REPROYECTAMOS A CCL PARA PASARLO AL POST REQUEST
	        var wkt_options = {};
          	var wkt = new ol.format.WKT(wkt_options);
          	var out = wkt.writeGeometry( vectorPunto.getSource().getFeatureById('point_coords').getGeometry() );
          	out = wkt.readGeometry(out, {dataProjection:'EPSG:3857', featureProjection:'EPSG:48402'});
          	out = wkt.writeGeometry(out);
          	//console.log("GEOM Punto: " + out);

          	
          	var wkt_options2, wkt2, out2;
          	if(featureIntersects == 'municipio')
          	{
          		wkt_options2 = {};
          		wkt2 = new ol.format.WKT(wkt_options2);
          		out2 = wkt2.writeGeometry( vector.getSource().getFeatureById('myMunicipio').getGeometry() );
          		out2 = wkt2.readGeometry(out2, {dataProjection:'EPSG:3857', featureProjection:'EPSG:48402'});
          		out2 = wkt2.writeGeometry(out2);
          		//console.log("GEOM Municipio: " + out2);
          	}
          	else if(featureIntersects == 'predio')
          	{
          		wkt_options2 = {};
          		wkt2 = new ol.format.WKT(wkt_options2);
          		out2 = wkt2.writeGeometry( vectorPredio.getSource().getFeatureById('myPredio').getGeometry() );
          		out2 = wkt2.readGeometry(out2, {dataProjection:'EPSG:3857', featureProjection:'EPSG:48402'});
          		out2 = wkt2.writeGeometry(out2);
          		//console.log("GEOM Predio: " + out2);
          	}

          	// 3) MEDIANTE UN REQUEST POST, HACEMOS UN INTERSECTS EN POSTGIS Y REGRESAMOS EL RESULTADO A LA FUNCIÓN PARA PASARLO AL TEXTBOX DEL SEGUNDO RESULT
	        $.post('getResultIntersects.php', {coordsPoint_WKTccl: out, coordsIntersects_WKTccl:out2}, function(result){

	        	if(result == 'OK')
	        	{
	        		$('#coordsIntersectsStatusMessage').textbox('setValue','Coordenada válida').textbox('textbox').css({backgroundColor: '#447F48', color: '#FFFFFF'});
	        		$('#coordsIntersectsResult').textbox('setValue','OK');
	        		console.log('Coordenada válida');
	        	}
	        	else if(result == 'EL PUNTO NO ESTA DENTRO DEL MUNICIPIO_PREDIO SELECCIONADO')
	        	{
	        		$('#coordsIntersectsStatusMessage').textbox('setValue','Coordenada fuera del ' + featureIntersects).textbox('textbox').css({backgroundColor: '#C52538', color: '#FFFFFF'});
	        		$('#coordsIntersectsResult').textbox('setValue',' ');
	        		$('#coordsIntersectsResult').textbox('clear');
	        		console.log('Coordenada fuera del ' + featureIntersects);
	        	}
	        	
	        });        	
        }


        function addNewLocation()
        {
            console.log('addNewLocation');
            $('#formAddNew').form('submit',{
                url: 'saveNewLocation.php',
                onSubmit: function()
                {
                    return $(this).form('enableValidation').form('validate');
                },
                success: function(result)
                {
                    var result = eval('('+result+')');
                    if(result.errorMsg)
                    {
                        $.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    }
                    else
                    {
                        if(result.okMsg)
                        {
                            $.messager.show({
                                title: 'Aviso',
                                msg: result.okMsg
                            });
                            $('#dlgAddNew').dialog('close');
                            resetForm();
                        }
                    }
                }
            });
        }

        function resetForm()
        {
        	console.log('Reseting form...');
            vector.getSource().clear();
            vectorPunto.getSource().clear();
            vectorPredio.getSource().clear();
            $('#formAddNew').form('clear').form('disableValidation').form('validate');
            $('#coordsIntersectsStatusMessage').textbox('setValue','Pendiente').textbox('textbox').css({backgroundColor: '#505050', color: '#FFFFFF'});
			map.getView().setZoom(5);
        }
    </script>
</head>
<body>
    <table style="margin:0 auto 0 auto;"><tr><td>
    <div id="panelUbicaciones" style="padding:10px 0px;">
        <form id="formAddNew" method="post">
            <div class="fitem">
                <label style="width:150px;">Camara:</label>
                <input id="camara" name="camara">
            </div>
            <div class="fitem">
                <label style="width:150px;">Clave:</label>
                <input id="clave" name="clave">
            </div>            
            <div class="fitem">
                <label style="width:150px;">Estado:</label>
                <input id="estado" name="estado">
            </div>
            <div class="fitem">
                <label style="width:150px;">Municipio:</label>
                <input id="municipio" name="municipio">
            </div>
            <div class="fitem">
                <label style="width:150px;">Predio:</label>
                <input id="predio" name="predio">
            </div>
            <div class="fitem">
                <label style="width:150px; text-decoration:underline; font-weight:bold;">Coordenadas UTM:</label>
            </div>
            <div class="fitem">
                <label style="width:150px;">Zona UTM:</label>
                <input id="zonaUTM" name="zonaUTM">
            </div>
            <div class="fitem">
                <label style="width:150px;">Coordenada X:</label>
                <input id="x" name="x">
                <input id="xResult" name="xResult">
            </div>
            <div class="fitem">
                <label style="width:150px;">Coordenada Y:</label>
                <input id="y" name="y">
                <input id="yResult" name="yResult">
            </div>
            <div class="fitem">
            	<label style="width:150px;">Resultado:</label>
            	<input id="coordsValidator" name="coordsValidator">
                <input id="coordsIntersectsStatusMessage" name="coordsIntersectsStatusMessage">
                <input id="coordsIntersectsResult" name="coordsIntersectsResult">
            </div>
            <div class="fitem">
                <label style="width:150px;">Fecha de instalación:</label>
                <input id="fechaInstalacion" name="fechaInstalacion">
            </div>
            <div class="fitem">
                <label style="width:150px;">Fecha de finalización:</label>
                <input id="fechaFinalizacion" name="fechaFinalizacion">
            </div>
            <div class="fitem">
                <label style="width:150px;">Observador:</label>
                <input id="observador" name="observador">
            </div>
        </form>

       
        <div id="myFooter" style="padding:5px; text-align:right;">
        <?php
          if($usuario_actual != 'invitado' && $usuario_actual != 'comunicacion' && $usuario_actual != 'jcobos')
          {
        ?>
                <a id="btnAgregarUbicacion"></a>
                <a id="btnLimpiarCampos"></a>
        <?php
          }
        ?>
        </div>
        


    </div></td><td>
    <div id="panelMapa"></div>
    </td></tr></table>


  <script>
   $(function()
   {
        //VARIABLES GLOBALES
        //extentMunicipio = 0;


        $('#panelMapa').panel({
            title:'Mapa',
            iconCls:'icon-mapa',
            width:700,
            height:500
        });
        /*PROYECCIONES*/
        proj4.defs("EPSG:48402","+proj=lcc +lat_1=17.5 +lat_2=29.5 +lat_0=12 +lon_0=-102 +x_0=2500000 +y_0=0 +ellps=WGS84 +units=m +no_defs ");
        proj4.defs("EPSG:32614","+proj=utm +zone=14 +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
        proj4.defs("EPSG:32615","+proj=utm +zone=15 +ellps=WGS84 +datum=WGS84 +units=m +no_defs");

        esri = new ol.layer.Tile({
            source: new ol.source.XYZ({
                url: 'https://server.arcgisonline.com/ArcGIS/rest/services/' + 'World_Imagery/MapServer/tile/{z}/{y}/{x}'
            })
        });
        source = new ol.source.Vector({ wrapX: false });
        sourcePredio = new ol.source.Vector({ wrapX: false });
        sourcePunto = new ol.source.Vector({ wrapX: false });
        vector = new ol.layer.Vector({source: source,style: new ol.style.Style({fill: new ol.style.Fill({color: 'rgba(255, 255, 255, 0.2)'}),stroke: new ol.style.Stroke({color: '#ffcc33',width: 2}), text:new ol.style.Text({font: '12px Calibri,sans-serif',fill: new ol.style.Fill({color: '#000'}),stroke: new ol.style.Stroke({color: '#fff', width: 2}) }),image: new ol.style.Circle({radius: 4,fill: new ol.style.Fill({color: '#ffcc33'})})})});
        vectorPredio = new ol.layer.Vector({source: sourcePredio,style: new ol.style.Style({fill: new ol.style.Fill({color: 'rgba(255, 255, 255, 0.2)'}),stroke: new ol.style.Stroke({color: '#ffcc33',width: 2}), text:new ol.style.Text({font: '12px Calibri,sans-serif',fill: new ol.style.Fill({color: '#000'}),stroke: new ol.style.Stroke({color: '#fff', width: 2}) }),image: new ol.style.Circle({radius: 4,fill: new ol.style.Fill({color: '#ffcc33'})})})});
        vectorPunto = new ol.layer.Vector({source: sourcePunto,style: new ol.style.Style({fill: new ol.style.Fill({color: 'rgba(255, 255, 255, 0.2)'}),stroke: new ol.style.Stroke({color: '#ffcc33',width: 2}), text:new ol.style.Text({font: '12px Calibri,sans-serif',fill: new ol.style.Fill({color: '#000'}),stroke: new ol.style.Stroke({color: '#fff', width: 2}) }),image: new ol.style.Circle({radius: 4,fill: new ol.style.Fill({color: '#ffcc33'})})})});
        view = new ol.View({
            projection: 'EPSG:3857',
            center: ol.proj.transform([-102.65, 23.88], 'EPSG:4326', 'EPSG:3857'), //DURANGO
            zoom: 5,
            minZoom:5,
            maxZoom:17
        });
        map = new ol.Map({
            pixelRatio:1,
            layers: [esri, vector, vectorPredio, vectorPunto],
            target: 'panelMapa',
            units:'m',
            view: view,
            logo: false
        });
        var extent = [-118,33,-87.3,14.15]; //ZOOM 5
        extent = ol.extent.applyTransform(extent, ol.proj.getTransform("EPSG:4326", "EPSG:3857"));
        constrainPan = function()
        {
            var visible = view.calculateExtent(map.getSize());
            var centre = view.getCenter();
            var delta;
            var adjust = false;
            if ((delta = extent[0] - visible[0]) > 0)
            {
                adjust = true;
                centre[0] += delta;
            }
            else if ((delta = extent[2] - visible[2]) < 0)
            {
                adjust = true;
                centre[0] += delta;
            }
            if ((delta = extent[1] - visible[1]) > 0)
            {
                adjust = true;
                centre[1] += delta;
            }
            else if ((delta = extent[3] - visible[3]) < 0)
            {
                adjust = true;
                centre[1] += delta;
            }
            if (adjust)
            {
                view.setCenter(centre);
            }
        };
        view.on('change:resolution', constrainPan);
        view.on('change:center', constrainPan);
        map.updateSize();

        //METODOS A REALIZAR CADA QUE CAMBIE EL VECTOR (SOURCE) DE MUNICIPIO
		source.on('change',function(e)
		{
    		if(source.getState() === 'ready') 
    		{
        		if(vector.getSource().getFeatures().length > 0) 
        		{
        			//var extent = vector.getSource().getExtent();
        			//map.getView().fit(extent, map.getSize());
        			extentMunicipio = vector.getSource().getExtent();
        			map.getView().fit(extentMunicipio, map.getSize());
        		}
    		}
		});



        //FORMULARIO
        $('#formAddNew').form({
            novalidate:true
        });
        //ELEMENTOS DEL FORMULARIO
        $('#camara').combobox({
            width:250,
            required:true,
            editable:false,
            valueField:'idcamaras',
            textField:'completa',
            url:'getCamaras.php'
        });
        $('#clave').textbox({
            width:250
        });        
        $('#estado').combobox({
            width:250,
            required:true,
            editable:false,
            valueField:'id_estado',
            textField:'nom_estado',
            url:'getEstados.php',
            onSelect: function(rec)
            {
                var estado = rec.nom_estado;
                resetFromEstado(estado);
            }
        });
        $('#municipio').combobox({
            width:250,
            panelHeight: 'auto',
            panelMaxHeight: 200,
            required:true,
            editable:true,
            hasDownArrow: false,
            valueField:'nom_municipio',
            textField:'nom_municipio',
            validType:'inList[\'#municipio\']',
            onSelect:function(rec)
            {
      			var idEstado = $('#estado').combobox('getValue');
                var municipio = rec.nom_municipio;               	
               	resetFromMunicipio(municipio);               	
               	drawMunicipio(idEstado, municipio);
            }
        });
        $('#predio').combobox({
            width:250,
            panelHeight: 'auto',
            panelMaxHeight: 200,
            required:true,
            editable:false,
            readonly:false,
            valueField:'idpredio',
            textField:'nombre_predio',
            onChange:function(value)
            {
            	if(value != '')
            	{            		
            		$('#x').numberbox('enable');
      				$('#y').numberbox('enable');
      				//LO OBLIGAMOS A DISPARAR SU EVENTO 'onChange' PARA QUE VUELVA A VALIDAR
      				var coordsValidator = $('#coordsValidator').textbox('getValue');
      				$('#coordsValidator').textbox('setValue', '...');
      				$('#coordsValidator').textbox('setValue',coordsValidator);

            	}
            },
            onSelect: function(rec)
            {
            	var idPredio = rec.idpredio;
	            var idEstado =  $('#estado').combobox('getValue');
            	var municipio = $('#municipio').combobox('getValue');

            	if(idPredio == '8')
            	{
            		sourcePredio.clear();
            		var extentMunicipio = vector.getSource().getExtent();
        			map.getView().fit(extentMunicipio, map.getSize());
            	}
            	else
            	{
            		drawPredio(idPredio, idEstado, municipio);
            	}            	
            }            
        });
        $('#zonaUTM').textbox({
            width:250,
            required:true,
            readonly: true
        });




		$('#x').numberbox({
            required:true,
            precision:2,
            validType: [ 'decimalValue', 'decimalLengthX' ],
            width:250,
            onChange: function(value)
            {
            	var valueX = value;
            	var cuantosCaracteresX = value.length;
            	console.warn("[X] Value: " + valueX + "Chars: " + cuantosCaracteresX);
            	switch(cuantosCaracteresX)
            	{
            		case 0:            			
            			$('#x').numberbox('textbox').css({backgroundColor: '#FFFFFF', color: '#DE2247'});
            			$('#xResult').textbox('setValue',' ');
            			$('#xResult').textbox('clear');
            			break;
            		case 9:
            			$('#x').numberbox('textbox').css({backgroundColor: '#447F48', color: '#FFFFFF'});
            			$('#xResult').textbox('setValue',' ');
            			$('#xResult').textbox('clear');
            			$('#xResult').textbox('setValue','OK');
            			break;
            		default:
            			$('#x').numberbox('textbox').css({backgroundColor: '#C52538', color: '#FFFFFF'});
            			$('#xResult').textbox('setValue',' ');
            			$('#xResult').textbox('clear');
            	}
            }
        });

        $('#xResult').textbox({
        	width:50,
        	required:true,
        	readonly:true,
        	onChange: function(value)
        	{
        		vectorPunto.getSource().clear();

        		var resultX = value;
        		var resultY = $('#yResult').textbox('getValue');

        		if(resultX == 'OK' && resultY == 'OK')
        		{
        			console.log('[resultX] Ambos correctos');
        			$('#coordsValidator').textbox('setValue',' ');
        			$('#coordsValidator').textbox('clear');
        			$('#coordsValidator').textbox('setValue','OK');
        		}
        		else
        		{
        			$('#coordsValidator').textbox('setValue',' ');
        			$('#coordsValidator').textbox('clear');
        		}
        	}        	
        });

        $('#y').numberbox({
            required:true,
            precision:2,
            validType: [ 'decimalValue', 'decimalLengthY' ],
            width:250,
            onChange: function(value)
            {
            	var valueY = value;
            	var cuantosCaracteresY = value.length;
            	console.warn("[Y] Value: " + valueY + "Chars: " + cuantosCaracteresY);
            	switch(cuantosCaracteresY)
            	{
            		case 0:            			
            			$('#y').numberbox('textbox').css({backgroundColor: '#FFFFFF', color: '#DE2247'});
            			$('#yResult').textbox('setValue',' ');
            			$('#yResult').textbox('clear');
            			break;
            		case 10:
            			$('#y').numberbox('textbox').css({backgroundColor: '#447F48', color: '#FFFFFF'});
            			$('#yResult').textbox('setValue',' ');
            			$('#yResult').textbox('clear');
            			$('#yResult').textbox('setValue','OK');
            			break;
            		default:
            			$('#y').numberbox('textbox').css({backgroundColor: '#C52538', color: '#FFFFFF'});
            			$('#yResult').textbox('setValue',' ');
            			$('#yResult').textbox('clear');
            	}
            }
        });

        $('#yResult').textbox({
        	width:50,
        	required:true,
        	readonly:true,
        	onChange: function(value)
        	{
        		vectorPunto.getSource().clear();

        		var resultX = $('#xResult').textbox('getValue');
        		var resultY = value;

        		if(resultX == 'OK' && resultY == 'OK')
        		{
        			console.log('[resultY] Ambos correctos');
        			$('#coordsValidator').textbox('setValue',' ');
        			$('#coordsValidator').textbox('clear');
        			$('#coordsValidator').textbox('setValue','OK');
        		}
        		else
        		{
        			$('#coordsValidator').textbox('setValue',' ');
        			$('#coordsValidator').textbox('clear');
        		}
        	}      	
        });


        $('#coordsValidator').textbox({
        	width:50,
            required:true,
            readonly:true,
            onChange:function(value)
            {           	
            	var validatorValue = value;
            	if(validatorValue == 'OK')
            	{
            		var zonaUTM = $('#zonaUTM').numberbox('getValue');
            		var x = $('#x').numberbox('getValue');
            		var y = $('#y').numberbox('getValue');
            		if( $('#predio').combobox('getValue') === '8' )
            			var featureIntersects = 'municipio';
            		else if( parseInt($('#predio').combobox('getValue')) > 0 )
            			var featureIntersects = 'predio';
            		//VALIDAR SI CAE DENTRO DEL MUNICIPIO/POLIGONO
            		console.log('Validating with PostGIS Intersects with ' + featureIntersects);
            		validateCoords(zonaUTM,x,y,featureIntersects);
            	}
            	else
            	{
            		$('#coordsIntersectsStatusMessage').textbox('setValue','Pendiente').textbox('textbox').css({backgroundColor: '#505050', color: '#FFFFFF'});
            		//console.log('Clearing the last result...');
            		$('#coordsIntersectsResult').textbox('setValue',' ');
            		$('#coordsIntersectsResult').textbox('clear');
            	}
            }	
        });
        $('#coordsIntersectsStatusMessage').textbox({
            width:250,
            required:true,
            readonly:true
        });

        $('#coordsIntersectsResult').textbox({
            width:45,
            required:true,
            readonly:true
        });

        $('#fechaInstalacion').datebox({
            width:250,
            required:true,
            editable:false
        }).datebox('calendar').calendar({
            validator: function(date)
            {
                var now = new Date();
                var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
                return date<=d1;
            }
        });
        $('#fechaFinalizacion').datebox({
            width:250,
            required:true,
            editable:false
        }).datebox('calendar').calendar({
            validator: function(date)
            {
                var now = new Date();
                var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
                return date<=d1;
            }
        });
        $('#observador').combobox({
            width:250,
            panelHeight: 'auto',
            panelMaxHeight: 200,
            required:true,
            editable:false,
            valueField: 'value',
			textField: 'value',
            data: 
            [
            	{value:'Benito Martínez Rivera'},
              {value:'César Raziel Lucio Palacio'},
            	{value:'Fernanda Salazar Chamorro'},
              {value:'Gabriel Aguilar Márquez'},
              {value:'Rafael Rodríguez Mesa'},
            	{value:'Nicholas García Knight'}
            ]
        });
        $('#btnAgregarUbicacion').linkbutton({
            iconCls:'icon-save',
            text:'Agregar ubicación',
            onClick: function()
            {
               addNewLocation();
            }
        });
        $('#btnLimpiarCampos').linkbutton({
            iconCls: 'icon-limpiar',
            text:'Limpiar campos',
            onClick: function()
            {
                resetForm();                
            }
        });
        //PANELES
        $('#panelUbicaciones').panel({
            width:500,
            height:500,
            title:'Registro de ubicaciones de camaras trampa',
            iconCls:'icon-ubicaciones',
            footer: '#myFooter',
            onOpen:function()
            {
                console.log('Panel opened');
                $('#formAddNew').form('clear').form('disableValidation').form('validate');
                $('#x').numberbox('disable');
            	$('#y').numberbox('disable');
                $('#coordsIntersectsStatusMessage').textbox('setValue','Pendiente').textbox('textbox').css({backgroundColor: '#505050', color: '#FFFFFF'});

                //Hide auxiliar textboxes
                $('#xResult').textbox('hide');
        		$('#yResult').textbox('hide');
        		$('#coordsValidator').textbox('hide');
        		$('#coordsIntersectsResult').textbox('hide');
            }
        });






    });
  </script>
  <?php
        }
    }
    else
    {
        echo "
        <html>
            <head><style>@font-face {font-family: 'Roboto';src: url('fonts/Roboto-Medium.ttf') format('truetype');}</style></head>
            <body><div style='width:100%; text-align:center; margin-top:250px;'><p style='color:crimson; font-family:Roboto;'>¡ERROR!</p><p style='color:dimgray; font-family:Roboto;'>Necesita iniciar sesión para utilizar este sistema</p></div></body>
        </html>";
    }
  ?>
</body>
</html>
