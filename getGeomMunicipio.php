<?php
	include ("../includes/conexion.php");
    $link = ConectarsePostgreSQL();
    $linkMySQL = ConectarseMySQLMegaBD();

    #RECUPERAMOS VARIABLES
    $idEstado = $_POST['idEstado'];
    $municipio = $_POST['municipio'];

    #OBTENEMOS LA GEOMETRIA DEL MUNICIPIO
    $sqlGeom = pg_query($link,"SELECT ST_AsText(geom) AS geom FROM shape_municipios_ccl WHERE cve_ent='$idEstado' AND nom_mun = '$municipio'"); //
    while($row = pg_fetch_assoc($sqlGeom))
    {
    	$myGeom = $row['geom'];
    }

    #OBTENEMOS LA ZONA UTM DEL MUNICIPIO
    $sqlZonaUTM = pg_query($link, "SELECT
    FLOOR 
    (
        (
            ST_X 
            (
                ST_Centroid 
                (
                    ST_Transform(geom, 4326)
                )
            ) + 180
        ) / 6
    ) + 1 AS zona
    FROM
        shape_municipios_ccl
    WHERE
        nom_mun = '$municipio' AND cve_ent = '$idEstado'");
    while($row = pg_fetch_assoc($sqlZonaUTM))
    {
        $zonaUTM = $row['zona'];    
    }
	
	echo $myGeom."|".$zonaUTM;
?>