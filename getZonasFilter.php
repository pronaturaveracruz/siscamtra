<?php
	include ("../includes/conexion.php");
	$linkMySQL = ConectarseMySQLMegaBD();
	
	$sql = mysqli_query($linkMySQL, "SELECT q1.zona 
	FROM
	(
		SELECT zona, ubicacion, id_clase
		FROM concentrado_fotos, registro_fototrampas, aves_ecoforestal
		WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6')
		UNION ALL
		SELECT zona, ubicacion, id_clase
		FROM concentrado_fotos, registro_fototrampas, mamiferos
		WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9')

		UNION ALL

		SELECT zona, ubicacion, id_clase
		FROM concentrado_videos, registro_videotrampas, aves_ecoforestal
		WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6')
		UNION ALL
		SELECT zona, ubicacion, id_clase
		FROM concentrado_videos, registro_videotrampas, mamiferos
		WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9')
	) AS q1, registro_ubicaciones, clasificacion
	WHERE (q1.ubicacion = registro_ubicaciones.idubicacion_ct AND q1.id_clase = clasificacion.idclasificacion)
	GROUP BY q1.zona
	ORDER BY q1.zona ASC");

	$items = array();	
	while($row = mysqli_fetch_object($sql))
	{

		$zona = $row -> zona ;

		if($zona == 'NORTE')
			$row -> zona = "Norte";
		else if($zona == 'TLACO')
			$row -> zona = "Tlacotalpan";
		else if($zona == 'CPALMA')
			$row -> zona = "Costa de la palma";

		array_push($items, $row);
	}
	echo json_encode($items);