<?php
include "../includes/conexion.php";
$linkMySQL = ConectarseMySQLMegaBD();

$sql = mysqli_query($linkMySQL, "SELECT fecha_cortada
    FROM
    (
        SELECT
        SUBSTRING(fecha,1,7) AS fecha_cortada
        FROM concentrado_videos
        WHERE status = 'PENDIENTE' AND(zona='SMO' OR zona='Jalacingo') OR (zona ='TLACO' AND fecha < '2017-11-01' AND status = 'PENDIENTE')
        ORDER BY fecha DESC
    ) AS q1
    GROUP BY fecha_cortada
    ORDER BY fecha_cortada DESC");

$items = array();
while ($row = mysqli_fetch_object($sql)) {

    $fecha_cortada = $row->fecha_cortada;
    $fecha_cortada = explode("-", $fecha_cortada);
    $anyo          = $fecha_cortada[0];
    $mes           = $fecha_cortada[1];

    switch ($mes) {
        case '01':
            $mes = "Enero";
            break;
        case '02':
            $mes = "Febrero";
            break;
        case '03':
            $mes = "Marzo";
            break;
        case '04':
            $mes = "Abril";
            break;
        case '05':
            $mes = "Mayo";
            break;
        case '06':
            $mes = "Junio";
            break;
        case '07':
            $mes = "Julio";
            break;
        case '08':
            $mes = "Agosto";
            break;
        case '09':
            $mes = "Septiembre";
            break;
        case '10':
            $mes = "Octubre";
            break;
        case '11':
            $mes = "Noviembre";
            break;
        case '12':
            $mes = "Diciembre";
            break;
        default:
            break;
    }

    $row->fecha_cortada = $mes . " " . $anyo;
    array_push($items, $row);

}
echo json_encode($items);
