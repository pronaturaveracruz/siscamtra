<?php
	//error_reporting(0);
	date_default_timezone_set("Mexico/General");
	$fecha_actual = date("Ymd_His");
	include ("../includes/conexion.php");
	$linkMySQL = ConectarseMySQLMegaBD();
	$linkPG    = ConectarsePostgreSQL();
	##FUNCIÓN PARA CONVERTIR UN ARRAY TIPO OBJETO A UN ARRAY CONVENCIONAL
	function objectToArray($d) 
	{
		if (is_object($d)) 
		{
			$d = get_object_vars($d); #Gets the properties of the given object with get_object_vars function
		} 
		if (is_array($d)) 
		{
            return array_map(__FUNCTION__, $d); #Return array converted to object, Using __FUNCTION__ (Magic constant), for recursive call
        }
        else 
        {
        	return $d; #Return array
        }
    }
	##FUNCIÓN PARA CREAR UN ARCHIVO ZIP
	function create_zip($files = array(), $destination = '', $overwrite = false)
	{
	    if (file_exists($destination) && !$overwrite) {return false;}
	    $valid_files = array();
	    if (is_array($files)) 
	    {
        	foreach ($files as $file) 
        	{
            	if (file_exists($file)) 
                	$valid_files[] = $file;
        	}
    	}
    	if (count($valid_files)) 
    	{
        	$zip = new ZipArchive();
        	if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) 
            	return false;
        	foreach ($valid_files as $file) 
        	{
            	$file_wo_path = str_replace("exportToShapefile/", "", $file);
            	$zip->addFile($file, $file_wo_path);
        	}
        	$zip->close();
        	return file_exists($destination);
    	} 
    	else 
    	{
        	return false;
    	}
	}

	#VARIABLES POST
	$id = $_POST['id'];
	$id_ct = $_POST['id_ct'];
	$fecha = $_POST['fecha'];
	$hora = $_POST['hora'];
	$zona = $_POST['zona'];
	$predio = $_POST['predio'];
	$id_clase = $_POST['id_clase'];
	$nombre_cientifico = $_POST['nombre_cientifico'];
	$destacada = $_POST['destacada'];
	$proyeccion = $_POST['proyeccion'];
	
	#INICIALIZAMOS VARIABLES LOCALES
	$filterArray = array();
	$filtroActivo = 0;
	$where = "";
	

	$pgsql2shp = "pgsql2shp";
	$ogr2ogr   = "ogr2ogr";
	$host      = HOST_PG;
	$user      = USER_PG;
	$frase     = PASS_PG;
	$encoding  = "export PGCLIENTENCODING=LATIN1;";
	##1) DEFINIMOS LOS NOMBRES DE LOS ARCHIVOS
	$nombre_archivo    = "shape_siscamtra_" . $fecha_actual ."_".$proyeccion;
	##2) DESTINO DE TODOS LOS ARCHIVOS SHAPEFILE (.shp, .shx, .prj, etc.)
	$destino_shapefile = PATH_FOR_SHAPEFILES_FOR_SISCAMTRA . $nombre_archivo;
	

	#VALIDAR SI LAS VARIABLES POST TIENEN ALGO PARA FILTRAR
	if($id !== '')
	{
		$id = explode("|", $id);
		array_push($filterArray, $id);
		$filtroActivo++;
	}
	if($id_ct !== '')
	{
		$id_ct = explode("|", $id_ct);
		array_push($filterArray, $id_ct);
		$filtroActivo++;
	}
	if($fecha !== '')
	{
		$fecha = explode("|", $fecha);
		array_push($filterArray, $fecha);
		$filtroActivo++;
	}
	if($hora !== '')
	{
		$hora = explode("|", $hora);
		array_push($filterArray, $hora);
		$filtroActivo++;
	}
	if($zona !== '')
	{
		$zona = explode("|", $zona);
		array_push($filterArray, $zona);
		$filtroActivo++;
	}
	if($predio !== '')
	{
		$predio = explode("|", $predio);
		array_push($filterArray, $predio);
		$filtroActivo++;
	}
	if($id_clase !== '')
	{
		$id_clase = explode("|", $id_clase);
		array_push($filterArray, $id_clase);
		$filtroActivo++;
	}
	if($nombre_cientifico !== '')
	{
		$nombre_cientifico = explode("|", $nombre_cientifico);
		array_push($filterArray, $nombre_cientifico);
		$filtroActivo++;
	}
	if($destacada !== '')
	{
		$destacada = explode("|", $destacada);
		array_push($filterArray, $destacada);
		$filtroActivo++;
	}

	if($filtroActivo > 0)
	{
		#CAMBIAMOS LOS VALORES DE LAS LLAVES DE NUMERICOS A TEXTO (PARA QUE QUEDEN IGUAL QUE EN 'get_apc.php')
		$filterArray = array_map(function($tag) {
	    	return array(
	        	'field' => $tag[0],
        		'op' 	=> $tag[1],
        		'value' => $tag[2]
    		);
		}, $filterArray);

		$num_filter  = count($filterArray);
		for($i=0; $i<$num_filter; $i++)
		{
			$filterField = $filterArray[$i]['field'];
			$filterOperator = $filterArray[$i]['op'];
			$filterValue = $filterArray[$i]['value'];

			#EXCEPCION PARA LAS ABREVIATURAS DE ZONA
			if($filterField == 'zona')
			{
				if($filterValue == 'Tlacotalpan')
					$filterValue = 'TLACO';
				else if($filterValue == 'Costa de la palma')
					$filterValue = 'CPALMA';
			}
			########################################

			$mValues = explode(",",$filterValue);
			$countmValues = count($mValues);		
			for($j=0; $j<$countmValues; $j++)
			{
				$filterValue = $mValues[$j];
				if ($countmValues > 1)
				{
					if($j === 0)
						$where .= " AND( lower(q2.$filterField) LIKE lower('%$filterValue%')";
					else if($j !==0 && $j!==($countmValues-1))
						$where .= " OR lower(q2.$filterField) LIKE lower('%$filterValue%')";
					else if($j===($countmValues-1))
						$where .= " OR lower(q2.$filterField) LIKE lower('%$filterValue%')  )";
				}
				else
				{
					switch($filterOperator)
					{
						case 'contains':
							$where .= " AND ( lower(q2.$filterField) LIKE lower('%$filterValue%')  )";
							break;
						case 'equal';
							$where .= " AND q2.".$filterField." = '".$filterValue."'";
							break;
						case 'notequal';
							$where .= " AND q2.".$filterField . " <> '" . $filterValue ."'";
							break;
						case 'beginwith';
							$where .= " AND q2.".$filterField . " LIKE '" . $filterValue ."%'";
							break;
						case 'endwith';
							$where .= " AND q2.".$filterField . " LIKE '%" . $filterValue ."'";
							break;
						case 'less';
							$where .= " AND q2.".$filterField . " < '". $filterValue."'";
							break;
						case 'lessorequal';
							$where .= " AND q2.".$filterField . " <= '". $filterValue."'";
							break;
						case 'greater';
							$where .= " AND q2.".$filterField . " > '". $filterValue."'";
							break;
						case 'greaterorequal';
							$where .= " AND q2.".$filterField . " >= '". $filterValue."'";
							break;
						case 'range':
							#TODO: Separar el filterValue en 2 variables
							$arrayFilterValue = explode(":", $filterValue);
							$f1 = $arrayFilterValue[0];
							$f2 = $arrayFilterValue[1];
							$where .= " AND (q2.fecha BETWEEN '$f1' AND '$f2')";
							break;
					}
				}
			}
		}
	}

	##INSERTAR AQUI LAS CONSULTAS
	#1) EJECUTAR CONSULTA COMUN CON COUNT(*) PARA SABER SI EL RESULTADO ES 0 Y NO EJECUTAR PARA EVITAR GENERAR UN SHAPE DAÑADO
	$countElements = pg_query($linkPG,"SELECT COUNT(*) FROM( SELECT id, id_ct, fecha, hora, zona, predio AS nom_predio, nombre_predio, ubicacion, id_clase, notas AS clase, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador, ST_SetSRID( ST_MakePoint(CAST(utmx AS decimal) , CAST(utmy AS decimal)), 32614) AS geom FROM ( SELECT id, archivo, id_ct, fecha, hora, q1.zona, predio, nombre_predio, ubicacion, id_clase, notas, id_especie, nombre_cientifico, destacada, ruta, status, clave, registro_ubicaciones.zona AS zona_utm, utmx, utmy, registro_ubicaciones.municipio, estado, fecha_inicio, fecha_fin, observador FROM ( SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, aves_ecoforestal WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, mamiferos WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, aves_ecoforestal WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, mamiferos WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9')) AS q1, registro_ubicaciones, clasificacion, predio WHERE (q1.ubicacion = registro_ubicaciones.idubicacion_ct AND q1.id_clase = clasificacion.idclasificacion AND registro_ubicaciones.predio = predio.idpredio) ) AS q14 WHERE q14.zona_utm = '14' UNION ALL SELECT id, id_ct, fecha, hora, zona, predio, nombre_predio, ubicacion, id_clase, notas, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador, ST_SetSRID( ST_MakePoint(CAST(utmx AS decimal) , CAST(utmy AS decimal)), 32615) AS geom FROM ( SELECT id, archivo, id_ct, fecha, hora, q1.zona, predio, nombre_predio, ubicacion, id_clase, notas, id_especie, nombre_cientifico, destacada, ruta, status, clave, registro_ubicaciones.zona AS zona_utm, utmx, utmy, registro_ubicaciones.municipio, estado, fecha_inicio, fecha_fin, observador FROM ( SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, aves_ecoforestal WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, mamiferos WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, aves_ecoforestal WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, mamiferos WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') ) AS q1, registro_ubicaciones, clasificacion, predio WHERE (q1.ubicacion = registro_ubicaciones.idubicacion_ct AND q1.id_clase = clasificacion.idclasificacion AND registro_ubicaciones.predio = predio.idpredio) ) AS q14 WHERE q14.zona_utm = '15' ) AS q2 WHERE id > CAST(0 AS char)".$where );

	$auxContinuar = 0;
	$cuantos_elementos = 0;

	while($row = pg_fetch_row($countElements))
	{
		$cuantos_elementos = $row[0];
		if($cuantos_elementos !== '0')
			$auxContinuar+=1;
	}

	if ($cuantos_elementos > 0)
		$tipogeom = 'elementos';
	else if($cuantos_elementos == 0)
		$tipogeom = 'ceros';


	#CCL
	if($proyeccion === 'ccl')
    	$consulta = "SELECT id, id_ct, fecha, hora, zona, nombre_predio AS nom_predio, notas AS clase, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador, ST_Transform(geom, 48402) AS geom FROM( SELECT id, id_ct, fecha, hora, zona, predio, nombre_predio, ubicacion, id_clase, notas, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador, ST_SetSRID( ST_MakePoint(CAST(utmx AS decimal) , CAST(utmy AS decimal)), 32614) AS geom FROM ( SELECT id, archivo, id_ct, fecha, hora, q1.zona, predio, nombre_predio, ubicacion, id_clase, notas, id_especie, nombre_cientifico, destacada, ruta, status, clave, registro_ubicaciones.zona AS zona_utm, utmx, utmy, registro_ubicaciones.municipio, estado, fecha_inicio, fecha_fin, observador FROM ( SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, aves_ecoforestal WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, mamiferos WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, aves_ecoforestal WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, mamiferos WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9')) AS q1, registro_ubicaciones, clasificacion, predio WHERE (q1.ubicacion = registro_ubicaciones.idubicacion_ct AND q1.id_clase = clasificacion.idclasificacion AND registro_ubicaciones.predio = predio.idpredio) ) AS q14 WHERE q14.zona_utm = '14' UNION ALL SELECT id, id_ct, fecha, hora, zona, predio, nombre_predio, ubicacion, id_clase, notas, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador, ST_SetSRID( ST_MakePoint(CAST(utmx AS decimal) , CAST(utmy AS decimal)), 32615) AS geom FROM ( SELECT id, archivo, id_ct, fecha, hora, q1.zona, predio, nombre_predio, ubicacion, id_clase, notas, id_especie, nombre_cientifico, destacada, ruta, status, clave, registro_ubicaciones.zona AS zona_utm, utmx, utmy, registro_ubicaciones.municipio, estado, fecha_inicio, fecha_fin, observador FROM ( SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, aves_ecoforestal WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, mamiferos WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, aves_ecoforestal WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, mamiferos WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') ) AS q1, registro_ubicaciones, clasificacion, predio WHERE (q1.ubicacion = registro_ubicaciones.idubicacion_ct AND q1.id_clase = clasificacion.idclasificacion AND registro_ubicaciones.predio = predio.idpredio) ) AS q14 WHERE q14.zona_utm = '15' ) AS q2 WHERE id > CAST(0 AS char) ".$where;
	#UTM14
	if($proyeccion === 'utm14')
    	$consulta = "SELECT id, id_ct, fecha, hora, zona, nombre_predio AS nom_predio, notas AS clase, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador, ST_Transform(geom, 32614) AS geom FROM( SELECT id, id_ct, fecha, hora, zona, predio, nombre_predio, ubicacion, id_clase, notas, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador, ST_SetSRID( ST_MakePoint(CAST(utmx AS decimal) , CAST(utmy AS decimal)), 32614) AS geom FROM ( SELECT id, archivo, id_ct, fecha, hora, q1.zona, predio, nombre_predio, ubicacion, id_clase, notas, id_especie, nombre_cientifico, destacada, ruta, status, clave, registro_ubicaciones.zona AS zona_utm, utmx, utmy, registro_ubicaciones.municipio, estado, fecha_inicio, fecha_fin, observador FROM ( SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, aves_ecoforestal WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, mamiferos WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, aves_ecoforestal WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, mamiferos WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9')) AS q1, registro_ubicaciones, clasificacion, predio WHERE (q1.ubicacion = registro_ubicaciones.idubicacion_ct AND q1.id_clase = clasificacion.idclasificacion AND registro_ubicaciones.predio = predio.idpredio) ) AS q14 WHERE q14.zona_utm = '14' UNION ALL SELECT id, id_ct, fecha, hora, zona, predio, nombre_predio, ubicacion, id_clase, notas, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador, ST_SetSRID( ST_MakePoint(CAST(utmx AS decimal) , CAST(utmy AS decimal)), 32615) AS geom FROM ( SELECT id, archivo, id_ct, fecha, hora, q1.zona, predio, nombre_predio, ubicacion, id_clase, notas, id_especie, nombre_cientifico, destacada, ruta, status, clave, registro_ubicaciones.zona AS zona_utm, utmx, utmy, registro_ubicaciones.municipio, estado, fecha_inicio, fecha_fin, observador FROM ( SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, aves_ecoforestal WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, mamiferos WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, aves_ecoforestal WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, mamiferos WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') ) AS q1, registro_ubicaciones, clasificacion, predio WHERE (q1.ubicacion = registro_ubicaciones.idubicacion_ct AND q1.id_clase = clasificacion.idclasificacion AND registro_ubicaciones.predio = predio.idpredio) ) AS q14 WHERE q14.zona_utm = '15' ) AS q2 WHERE id > CAST(0 AS char) ".$where;
	#UTM15
	if($proyeccion === 'utm15')
    	$consulta = "SELECT id, id_ct, fecha, hora, zona, nombre_predio AS nom_predio, notas AS clase, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador, ST_Transform(geom, 32615) AS geom FROM( SELECT id, id_ct, fecha, hora, zona, predio, nombre_predio, ubicacion, id_clase, notas, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador, ST_SetSRID( ST_MakePoint(CAST(utmx AS decimal) , CAST(utmy AS decimal)), 32614) AS geom FROM ( SELECT id, archivo, id_ct, fecha, hora, q1.zona, predio, nombre_predio, ubicacion, id_clase, notas, id_especie, nombre_cientifico, destacada, ruta, status, clave, registro_ubicaciones.zona AS zona_utm, utmx, utmy, registro_ubicaciones.municipio, estado, fecha_inicio, fecha_fin, observador FROM ( SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, aves_ecoforestal WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, mamiferos WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, aves_ecoforestal WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, mamiferos WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9')) AS q1, registro_ubicaciones, clasificacion, predio WHERE (q1.ubicacion = registro_ubicaciones.idubicacion_ct AND q1.id_clase = clasificacion.idclasificacion AND registro_ubicaciones.predio = predio.idpredio) ) AS q14 WHERE q14.zona_utm = '14' UNION ALL SELECT id, id_ct, fecha, hora, zona, predio, nombre_predio, ubicacion, id_clase, notas, nombre_cientifico, destacada, clave, zona_utm, utmx, utmy, municipio, estado, fecha_inicio, fecha_fin, observador, ST_SetSRID( ST_MakePoint(CAST(utmx AS decimal) , CAST(utmy AS decimal)), 32615) AS geom FROM ( SELECT id, archivo, id_ct, fecha, hora, q1.zona, predio, nombre_predio, ubicacion, id_clase, notas, id_especie, nombre_cientifico, destacada, ruta, status, clave, registro_ubicaciones.zona AS zona_utm, utmx, utmy, registro_ubicaciones.municipio, estado, fecha_inicio, fecha_fin, observador FROM ( SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, aves_ecoforestal WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_fotos.id_foto AS id, foto AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacada, ruta, status FROM concentrado_fotos, registro_fototrampas, mamiferos WHERE (concentrado_fotos.id_foto = registro_fototrampas.id_foto) AND (registro_fototrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, aves_ecoforestal WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=aves_ecoforestal.id_aves_ecoforestal) AND (id_clase = '6') UNION ALL SELECT concentrado_videos.id_video AS id, video AS archivo, id_ct, fecha, hora, zona, ubicacion, id_clase, id_especie, nombre_cientifico, destacado AS destacada, ruta, status FROM concentrado_videos, registro_videotrampas, mamiferos WHERE (concentrado_videos.id_video = registro_videotrampas.id_video) AND (registro_videotrampas.id_especie=mamiferos.idMamiferos) AND (id_clase = '9') ) AS q1, registro_ubicaciones, clasificacion, predio WHERE (q1.ubicacion = registro_ubicaciones.idubicacion_ct AND q1.id_clase = clasificacion.idclasificacion AND registro_ubicaciones.predio = predio.idpredio) ) AS q14 WHERE q14.zona_utm = '15' ) AS q2 WHERE id > CAST(0 AS char) ".$where;


	#DEBUG
	//echo $consulta;


	#ELIMINAR TODOS LOS ARCHIVOS DEL DIRECTORIO 'exportToShapefile/'
	$files = glob('exportToShapefile/*');
	foreach ($files as $file) {
		if (is_file($file))
        	unlink($file);
	}

	#GENERAMOS EL SHAPEFILE CON LAS HERRAMIENTAS ESPACIALES DE POSTGIS Y OGR
	if($tipogeom == 'elementos')
	{
		//shell_exec($encoding.' "' . $pgsql2shp . '" -f "' . $destino_shapefile . '" -h ' . $host . ' -u ' . $user . ' -P ' . $frase . ' ecoforestal_geo "' . $consulta . '" ');
		shell_exec($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile . '.shp PG:"host=' . $host . ' dbname=ecoforestal_geo user=' . $user . ' password=' . $frase . '" -sql "' . $consulta . '"');
    	shell_exec($ogr2ogr.' -f KML '.$destino_shapefile.'.kml PG:"host='.$host.' dbname=ecoforestal_geo user='.$user.' password='.$frase.'" -sql "'.$consulta .'"');
	}

	#CREAR EL ARCHIVO ZIP
	if($tipogeom != 'ceros')
	{
		#1) AGREGAMOS A UN ARREGLO LA LISTA DE ARCHIVOS
		$arreglo_archivos = array();
		foreach (glob('exportToShapefile/' . $nombre_archivo . ".*") as $nombre_fichero) {
			array_push($arreglo_archivos, $nombre_fichero);
		}

		#2) DEFINIMOS NOMBRE DEL ARCHIVO Y CREAMOS EL ZIP CON LA FUNCIÓN 'create_zip'
		$nombre_archivo_zip = "exportToShapefile/shapeSISCAMTRA_" . $fecha_actual ."_".$proyeccion. ".zip";
		$result             = create_zip($arreglo_archivos, $nombre_archivo_zip);

		if($result)
		{
			if (!$nombre_archivo_zip) 
			{
				echo "error";
			}
			else
			{
				if($tipogeom == 'elementos')
            		echo "ok,".$tipogeom.",".$cuantos_elementos.",".$nombre_archivo_zip;
			}
		}
		#FIN
	}