<?php
	include "../includes/conexion.php";
	$linkMySQL = ConectarseMySQLMegaBD();

	#RECUPERAMOS VARIABLES
	$camara            = $_POST['camara'];
	$clave             = $_POST['clave'];
	$idEstado          = $_POST['estado'];
	$municipio         = $_POST['municipio'];
	$predio            = $_POST['predio'];
	$zonaUTM           = $_POST['zonaUTM'];
	$x                 = $_POST['x'];
	$y                 = $_POST['y'];
	$fechaInstalacion  = $_POST['fechaInstalacion'];
	$fechaFinalizacion = $_POST['fechaFinalizacion'];
	$observador        = $_POST['observador'];

	$sqlGetNombreEstado = mysqli_query($linkMySQL, "SELECT nom_estado FROM ubicacion WHERE id_estado = '$idEstado'");
	while($estado = mysqli_fetch_assoc($sqlGetNombreEstado))
	{
		$nombreEstado = $estado['nom_estado'];
	}

	/*echo "Camara: ".$camara;
	echo "Clave: ".$clave;
	echo "Predio: ".$predio;
	echo "zonaUTM: ".$zonaUTM;
	echo "x: ".$x;
	echo "y: ".$y;
	echo "Municipio: ".$municipio;
	echo "Estado: ".$nombreEstado;	
	echo "fechaInstalacion: ".$fechaInstalacion;
	echo "fechaFinalizacion: ".$fechaFinalizacion;
	echo "observador: ".$observador;*/

	#VERIFICAMOS SI LA UBICACION YA ESTA EN LA BASE DE DATOS
	$sqlVerificaUbicacion = mysqli_query($linkMySQL, "SELECT COUNT(*) FROM registro_ubicaciones WHERE id_camara='$camara' AND predio ='$predio' AND zona='$zonaUTM' AND utmx='$x' AND utmy='$y' AND municipio='$municipio' AND estado='$nombreEstado' AND fecha_inicio='$fechaInstalacion' AND fecha_fin='$fechaFinalizacion' AND observador='$observador' ");
	while($ubicacionesRepetidas = mysqli_fetch_row($sqlVerificaUbicacion))
	{
		$totalUbicacionesRepetidas = $ubicacionesRepetidas[0];
	}

	if($totalUbicacionesRepetidas == 0)
	{
		$sqlInsert = mysqli_query($linkMySQL, "INSERT INTO registro_ubicaciones(id_camara,clave,predio,zona,utmx,utmy,municipio,estado,fecha_inicio,fecha_fin,observador) VALUES ('$camara','$clave','$predio','$zonaUTM','$x','$y','$municipio','$nombreEstado','$fechaInstalacion','$fechaFinalizacion','$observador')");
		$sqlInsertAffectedRows = mysqli_affected_rows($linkMySQL);
		if($sqlInsertAffectedRows > 0)
		{
			$result = "OK";
		}
		else
		{
			$result = "ERROR AL INSERTAR";
		}
	}
	else if($totalUbicacionesRepetidas > 0)
	{
		$result = "YA ESTA EN LA BD";
	}

	#PROCESAR LA VARIABLE $result Y PASAR UN JSON DE REGRESO A JS
	if($result == "OK")
	{
		$mensaje = "Se insertó correctamente la ubicación en la base de datos.";
		echo json_encode(
			array('okMsg' => $mensaje)
		);
	}
	else if($result == "ERROR AL INSERTAR")
	{
		$mensaje = "No se pudo insertar la ubicación en la base de datos.";
		echo json_encode(
			array('errorMsg' => $mensaje)
		);
	}
	else if($result == "YA ESTA EN LA BD")
	{
		$mensaje = "Estos datos ya los habías capturado.";
		echo json_encode(
			array('errorMsg' => $mensaje)
		);
	}
