<?php

	/*
		SELECT * FROM concentrado_fotos WHERE id_foto = '10318';
		SELECT * FROM concentrado_videos WHERE id_video = '1261';
	*/


	//error_reporting(0);
	date_default_timezone_set("Mexico/General");
	session_start();
	$usuario_actual= $_SESSION['usuario'];
	$fecha_actual = date('Y-m-d');
	include ("../includes/conexion.php");
	$linkMySQL = ConectarseMySQLMegaBD();
	$auxFotos=0;
	$auxVideos=0;

	$fotosMsg = '';
	$videosMsg = ''; 

	$contador=0;
	
	#RECUPERAMOS VARIABLES
	$paraDestacarValues = $_POST['paraDestacarValues'];
	$opcion = $_POST['opcion'];

	if($paraDestacarValues && $opcion)
	{
		#AJUSTAMOS LOS ARRAY
		$arreglo_recolectados_a_destacar = explode(",", $paraDestacarValues);
		$cuantas = count($arreglo_recolectados_a_destacar);

		#AJUSTAR PARA FOTOS Y VIDEOS
		for($i=0;$i<$cuantas;$i++)
		{
			$thisID = $arreglo_recolectados_a_destacar[$i];
			$thisIDArray = explode("|", $thisID);
			$id = $thisIDArray[0];
			$tipo = $thisIDArray[1];

			if($opcion == 'SI')
			{
				if($tipo == 'jpg')
				{
					$sqlUpdate = mysqli_query($linkMySQL, "UPDATE concentrado_fotos SET destacada = 'Si', quien_destaco= '$usuario_actual' WHERE concentrado_fotos.id_foto = $id");
					$marUpdate = mysqli_affected_rows($linkMySQL);
					if($marUpdate > 0)
						$auxFotos++;
				}
				else if($tipo == 'mp4')
				{
					$sqlUpdate = mysqli_query($linkMySQL, "UPDATE concentrado_videos SET destacado = 'Si', quien_destaco= '$usuario_actual' WHERE concentrado_videos.id_video = $id");
					$marUpdate = mysqli_affected_rows($linkMySQL);
					if($marUpdate > 0)
						$auxVideos++;
				}

				
			}
			else if($opcion == 'NO')
			{
				if($tipo == 'jpg')
				{
					$sqlUpdate = mysqli_query($linkMySQL, "UPDATE concentrado_fotos SET destacada = 'No', quien_destaco= '$usuario_actual' WHERE concentrado_fotos.id_foto = $id");
					$marUpdate = mysqli_affected_rows($linkMySQL);
					if($marUpdate > 0)
						$auxFotos++;
				}
				else if($tipo == 'mp4')
				{
					$sqlUpdate = mysqli_query($linkMySQL, "UPDATE concentrado_videos SET destacado = 'No', quien_destaco= '$usuario_actual' WHERE concentrado_videos.id_video = $id");
					$marUpdate = mysqli_affected_rows($linkMySQL);
					if($marUpdate > 0)
						$auxVideos++;
				}

				
			}
			
		}
		###########################################################################

		#GENERAR MENSAJE
		if($auxFotos > 0)
		{
			
			//echo "auxFotos es mayor a cero";

			if($auxFotos == 1)
				$fotosMsg = "1 fotografía.";
			else
				$fotosMsg = $auxFotos." fotografías.";
		}

		if($auxVideos > 0)
		{	
			//echo "auxVideos es mayor a cero";

			if($auxVideos == 1)
				$videosMsg = "1 video.";
			else
				$videosMsg = $auxVideos." videos.";
		}

		if($auxFotos > 0 || $auxVideos > 0)
		{
			//echo "AMBOS mayores a cero";
			$okMsg = "Se actualizó:<br><br>".$fotosMsg."<br>".$videosMsg;
			echo json_encode(array('okMsg'=> $okMsg ));
		}
		else
		{
			$errorMsg = "No se actualizó nada.";
			echo json_encode(array('errorMsg'=> $errorMsg ));
		}
		#############################################################################



	}
	else
	{
		$errorMsg = "Error en las variables";
		echo json_encode(array('errorMsg'=> $errorMsg));
	}