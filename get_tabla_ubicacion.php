<?php
	$result = array();
	include ("../includes/conexion.php");
	$linkMySQL = ConectarseMySQLMegaBD();
	
	$idubicacion=$_GET['idubicacion'];
	if($idubicacion!='')
	{
		$sql = mysqli_query($linkMySQL, "SELECT idubicacion_ct, id_ct, clave, nombre_predio, zona, utmx, utmy, registro_ubicaciones.municipio, estado, fecha_inicio, fecha_fin, observador 
			FROM registro_ubicaciones 
			INNER JOIN camaras ON registro_ubicaciones.id_camara=camaras.idcamaras
			INNER JOIN predio ON registro_ubicaciones.predio=predio.idpredio
			WHERE idubicacion_ct=$idubicacion");

		$items = array();	
		while($row = mysqli_fetch_assoc($sql))
		{	
			$items[] = array("name" => "Camara","value" => $row['id_ct']);
			$items[] = array("name" => "Clave","value" => $row['clave']);
			$items[] = array("name" => "APC","value" => $row['nombre_predio']);
			$items[] = array("name" => "Zona","value" => $row['zona']);
			$items[] = array("name" => "X","value" => $row['utmx']);
			$items[] = array("name" => "Y","value" => $row['utmy']);
			$items[] = array("name" => "Estado","value" => $row['estado']);
			$items[] = array("name" => "Municipio","value" => $row['municipio']);
			$items[] = array("name" => "Fecha de inicio","value" => $row['fecha_inicio']);
			$items[] = array("name" => "Fecha de fin","value" => $row['fecha_fin']);
			$items[] = array("name" => "Observador","value" => $row['observador']);
		}


		echo json_encode($items);
	}
?>
